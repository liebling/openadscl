#include <stdio.h>
#include <stdlib.h>
#ifdef SDF
#include "bbhutil.h"
#endif
#include <string.h>
#include <math.h>
#include "parm.h"

/* fields */

#define VAR_DECLS
#include "fields.h"

/* grid */

double x_h[N];
double x_c[N];

/* Other Parameters etc. */

double dx, dt, mass0;

void grid(void);		/* set up the grid */
void body();			/* main loop */
void initfields(void);		/* initialize fields */
void copy(double *to, const double *const from, const int n);
double L2Norm(const double *const f, const int n);
void intfieldRev(double *field, const double *const deriv, const double dx, int n);
void calcmetrica(double *a, const double *const phi, const double *const pi, const double *const x, int n);
void calcmetricanoRB(double *a, double *dmdr, const double *const phi, const double *const pi, const double *const x, int n);
void calcmetricanoRBnip(double *a, const double *const phi, const double *const pi, double *work, const double *const x, int n);
void calcmetricanoRBbw(double *a, const double *const phi, const double *const pi, const double *const x, int n);
void calcdelta(double *delta, const double *const phi, const double *const pi, const double *const x, int n);
void calcdeltanoRB(double *delta, const double *const phi, const double *const pi, const double *const x, int n);
void calcdeltanoRBnotrescaled(double *delta, double *work, const double *const phi, const double *const pi, const double *const x, int n);
void calcdeltanoRBnotrescaledRK4(double *delta, double *work, const double *const phi, const double *const pi, const double *const x, int n);
void calcmetricanoRBnotrescaled(double *a, double *dmdr, const double *const phi, const double *const pi, const double *const x, int n);
double interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4);
double ej(int q, double x, int deriv);
void deriv(double *f_dx, double *f, const double *const x, int a, int num);
void decompose(double *work0, double *work1, double *work2, double *qq, double *pp, double *a, double *delta, const double *const x, double time, int n);





int main(int argc, char **argv)
{				/* Main Code */

    int l, m;

    if (N % 2) {
	// N is odd:
	dx = (Xmax - Xmin) / (N - 1);	/* radial grid increment */
    } else {
	// N is even:
	dx = (Xmax - Xmin) / N;	/* radial grid increment */
    }
    dt = CFL * dx;

    printf("main: Parameters: \n");
    printf("main: Parameters: RKORDER=%d\n", RKORDER);
    printf("main: Parameters: N=%d T=%f INITDATA=%d \n", N, T, INITDATA);
    printf("main: Parameters: dx=%f dt=%f CFL=%f\n", dx, dt, CFL);
    printf("main: Parameters: epsilon=%f sigma=%f \n", Epsilon, Sigma);
    printf("main: Parameters: INTAFREQ=%d  \n", INTAFREQ);
    printf("main: Parameters: MINABHTHRESH=%f  \n", MINABHTHRESH);
    printf("main: Parameters: pureads=%d  \n", PUREADS);
    printf("main: Parameters: num steps=%d \n", (int) (T / dt));

    if (PUREADS == 2) {
	printf("main: Running with metric fixed at initial form\n");
    }
    if (PUREADS == 1) {
	printf("main: Running strictly with A=1 and delta=0 \n");
    }
    if (PUREADS == 0) {
	printf("main: Running with dynamic metric. \n");
    }

    grid();			/* setting up the grid */

    initfields();		/* initialize physical fields */

    body();			/* rest of everything         */

#ifdef SDF
    printf("main: Closing SDF files\n");
    gft_close_all();		// close all sdf output files
#endif

}

/******************************************************
  ******************************************************/
void initfields(void)
{
    int l, m, i, k;
    double source;
    int nummodes = 20;
    double amp[20];
    double myx, phihat, qqhat, deriv_phihat;
    double dx;


    /*                                              */
    /*  Set the scalar field quantities:            */
    /*                                              */
    if (INITDATA == 0) {
	/* Gaussian initial data as per original BR 2011 paper: */
	dx = x_c[1] - x_c[0];
	for (l = 0; l < N; l++)	/* initial data assignment */
	    for (m = 0; m < M; m++) {
		pp[idx(m, l)] = (2.0 * Epsilon / Pi) * exp(-4.0 * pow(tan(x_c[l]) / Pi / Sigma, 2));
		ph[idx(m, l)] = 1.0;
		qq[idx(m, l)] = 0.0;
		metrica[idx(m, l)] = 1.0;
		delta[idx(m, l)] = 0.0;
		dmdr[idx(m, l)] = 0.0;
	    }
    } else if (INITDATA > 0) {
	if (INITDATA == 3) {
	    amp[0] = -5.2425460892619531461285118478491379955288259455312e-10;
	    amp[1] = 1.3117099583503019375086743191077785994683567963015e-6;
	    amp[2] = -0.00014964721915943007724746958105128924169290842030729;
	    amp[3] = 0.0043466966389511848063819943690919963624391409800953;
	    amp[4] = -0.042591965460603435402172673338944904349563745880071;
	    amp[5] = 0.124777017304233306416236252494411987615654575524252;
	    amp[6] = 0.0415923391014111021387454174981373292052181918414173;
	    amp[7] = 0.009277850489017819729613774817929064826600877579445;
	    amp[8] = 0.0017726517088183857953670017487178389356848416766082;
	    amp[9] = 0.0003139695203162652972532962887634783878777794685334;
	    amp[10] = 0.000053352648237415414239095114647136034936543878826383;
	    amp[11] = 8.8499238671345204807036546175413270447005269440922e-6;
	    amp[12] = 1.4466948827159328086173182944946255811353720196937e-6;
	    amp[13] = 2.343650192238783437675204483273195430377749816263e-7;
	    amp[14] = 3.775470946071690093316696073412180076147542000183e-8;
	    amp[15] = 6.0610829853960879239396969467194430944770076017905e-9;
	    amp[16] = 9.710412455457528748127920439514269933959843056549e-10;
	    amp[17] = 1.5539385562582101225379244222543615269329395341479e-10;
	    amp[18] = 2.48545360160558764031870951671123913767007226545e-11;
	    amp[19] = 3.9749552050590093862284485180018791483994873803168e-12;
	} else if (INITDATA == 2) {
	    // approximately quasi-periodic initial data
	    for (k = 0; k < nummodes; k++) {
		// amp[k] = (exp(-ALPHA * sqrt(pow((double) k - KDOMINANT, 2)))) / (2.0 * (double) k + 3.0);
		amp[k] = (exp(-ALPHA * sqrt(pow((double) k - KDOMINANT, 2))));
	    }
	    printf("initfields: ALPHA=%e KDOMINANT=%e \n", ALPHA, KDOMINANT);
	} else if (INITDATA == 1) {
	    // two-mode, equal energy ID:
	    amp[0] = 0.333333333333333;
	    amp[1] = 0.2;
	    for (k = 2; k < nummodes; k++) {
		amp[k] = 0.0;
	    }
	    /* Initial data constructed in terms of modes:          */
	    for (k = 0; k < nummodes; k++) {
		printf("initfields: k=%d, amp[k]=%e\n", k, amp[k]);
	    }
	    /* Initial data constructed in terms of modes:          */
	}
	for (k = 0; k < nummodes; k++) {
	    printf("initfields: k=%d, amp[k]=%e\n", k, amp[k]);
	}
	for (l = 0; l < N; l++)
	    for (m = 0; m < M; m++) {
		pp[idx(m, l)] = 0.0;
		qq[idx(m, l)] = 0.0;
		ph[idx(m, l)] = 0.0;
		metrica[idx(m, l)] = 1.0;
		delta[idx(m, l)] = 0.0;
		dmdr[idx(m, l)] = 0.0;
		for (k = 0; k < 20; k++) {
		    myx = x_c[l];
		    phihat = ej(k, myx, 0);
		    deriv_phihat = ej(k, myx, 1);
		    qqhat = deriv_phihat;
		    qq[idx(m, l)] = qq[idx(m, l)] + Epsilon * amp[k] * qqhat;
		}
	    }
	//Regularity at the origin:
	qq[idx(0, 0)] = 0.0;
	qq[idx(0, N - 1)] = 0.0;
    }


    /*                                              */
    /*  Solve the constraints and other quantities: */
    /*                                              */
    if (PUREADS != 1) {
	// Solve for phi by integrating qq:
	intfieldRev(ph, qq, dx, N);
	// Solve for metrica:
	calcmetricanoRBnotrescaled(metrica, dmdr, qq, pp, x_c, N);
	// Solve for delta:
	calcdeltanoRBnotrescaledRK4(delta, aux_pi, qq, pp, x_c, N);
    }
    // Compute dmdr:
    dmdr[0] = 0.0;
    hamc[0] = 0.0;
    for (i = 1; i < N - 1; i++) {
	source = pow(qq[i], 2) + pow(pp[i], 2);
	dmdr[i] = pow(tan(x_c[i]), 2) * metrica[i] * source;
	// See the last term in parenthesis in Eq. 18 of 1308.1235 (http://arxiv.org/pdf/1308.1235.pdf ):
	aux_pi[i] = metrica[i] * exp(-delta[i]) * qq[i] / sin(x_c[i]) / cos(x_c[i]);
	hamc[i] = sin(x_c[i]) * cos(x_c[i]) * (metrica[i + 1] - metrica[i - 1]) / (x_c[i + 1] - x_c[i - 1])
	    - (1.0 + 2.0 * pow(sin(x_c[i]), 2)) * (1.0 - metrica[i])
	    + pow(sin(x_c[i]) * cos(x_c[i]), 2) * metrica[i] * source;
    }
    // quadratic fit:
    aux_pi[0] = 1.5 * aux_pi[1] - 0.6 * aux_pi[2] + 0.1 * aux_pi[3];
    aux_pi[N - 1] = 0.0;
    hamc[N - 1] = 0.0;

    mass0 = 0.0;
    for (i = 1; i < N; i++) {
	mass0 = mass0 + 0.5 * (dmdr[i] + dmdr[i - 1]) * dx;
    }

    copy(delta_np1, delta, N);
    copy(metrica_np1, metrica, N);
    copy(qq_np1, qq, N);
    copy(pp_np1, pp, N);
    copy(ph_np1, ph, N);

    printf("initfields: ||dmdr||_2    = %f \n", L2Norm(dmdr, N));
    printf("initfields: ||metrica||_2 = %f \n", L2Norm(metrica, N));
    printf("initfields: ||delta||_2   = %f \n", L2Norm(delta, N));
    printf("initfields: ||qq||_2      = %f \n", L2Norm(qq, N));
    printf("initfields: ||pp||_2      = %f \n", L2Norm(pp, N));
    printf("initfields: ||ph||_2      = %f \n", L2Norm(ph, N));
    printf("initfields: ||aux_pi||_2  = %f \n", L2Norm(aux_pi, N));
    printf("initfields: ||hamc||_2    = %f \n", L2Norm(hamc, N));
    printf("initfields: mass0         = %f \n", mass0);

    // using diss_* arrays simply for storage:
    printf("initfields: Decomposing initial data \n");
    decompose(diss_pp, diss_qq, diss_ph, qq, pp, metrica, delta, x_c, 0.0, N);
    printf("initfields: Done.                    \n");
}

/******************************************************
  ******************************************************/
void decompose(double *work0, double *work1, double *work2, double *qq, double *pp, double *a, double *delta, const double *const x, double time, int n)
{
    // TTF conserved quantities (approximately conserved in evolution):
    double ttfE, ttfN, ttfT;
    double xip1, xi, dx;
    double integrandPi, integrandPhi;
    int i, j;
    int nj = 60;

    ttfE = 0.0;
    ttfN = 0.0;
    ttfT = 0.0;
    // Loop over modes j
    for (j = 0; j < nj; j++) {
	work0[j] = 0.0;
	work1[j] = 0.0;
	for (i = 0; i < N; i++) {
	    xip1 = x[i + 1];
	    xi = x[i];
	    dx = xip1 - xi;
	    integrandPi = 0.5 * sqrt(a[i + 1]) * pow(tan(x[i + 1]), 2) * pp[i + 1] * ej(j, x[i + 1], 0)
		+ 0.5 * sqrt(a[i]) * pow(tan(x[i]), 2) * pp[i] * ej(j, x[i], 0);
	    integrandPhi = 0.5 * sqrt(a[i + 1]) * pow(tan(x[i + 1]), 2) * qq[i + 1] * ej(j, x[i + 1], 1)
		+ 0.5 * sqrt(a[i]) * pow(tan(x[i]), 2) * qq[i] * ej(j, x[i], 1);
	    work0[j] = work0[j] + integrandPi * dx;
	    work1[j] = work1[j] + integrandPhi * dx;
	}
	// Energy in this mode:
	work2[j] = pow(work0[j], 2) + pow(work1[j] / (2.0 * j + 3.0), 2);
	// Particle number in this mode:
	work1[j] = work2[j] / (2.0 * j + 3.0);
	// Phase (how much of energy is in Pi versus total energy):
	work0[j] = pow(work0[j], 2) / work2[j];
	//
	ttfE = ttfE + work2[j];
	ttfN = ttfN + work1[j];
    }

    ttfT = ttfE / ttfN;

    if (1) {
	printf("decompose: at t=%e E=%e N=%e T=%e \n", time, ttfE, ttfN, ttfT);
	for (j = 0; j < 10; j++) {
	    printf("decompose: Ej[%d]=%e phase[%d]=%e \n", j, work2[j], j, work0[j]);
	}
    }
}

/******************************************************
  ******************************************************/
void grid(void)
{

    int l, m;

    for (l = 0; l < N; l++) {	/* Setting up the grid */

	x_c[l] = Xmin + (double) l *dx;
	x_h[l] = Xmin + ((double) l - 0.5) * dx;

    }

}
