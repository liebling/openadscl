#include <stdio.h>
#include <string.h>
#ifdef SDF
#include "bbhutil.h"
#endif
#include <stdlib.h>
#include <math.h>
#ifdef MAC
#include <OpenCL/opencl.h>
#else 
#include <CL/cl.h>
#endif 
#include "parm.h"

void copy(double *to, const double *const from, const int n);
double L2Norm(const double *const f, const int n);
double interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4);
void calcdeltanoRBnotrescaledRK4(double *delta, double *work, const double *const phi, const double *const pi, const double *const x, int n);
void calcdeltanoRBnotrescaledRK4pq(double *delta, const double *const pqsourceint, const double *const pqsourcecop, const double *const x, int n);
void calcmetricanoRBnotrescaled(double *a, double *dmdr, const double *const phi, const double *const pi, const double *const x, int n);
void decompose(double *work0, double *work1, double *work2, double *qq, double *pp, double *a, double *delta, const double *const x, double time, int n);



#define shrCheckError(ErrNum, cl_success)\
{\
    if (ErrNum != cl_success)\
	{\
		printf ("file: body.c line: %d - error: %d\n", __LINE__, ErrNum);\
		if (ErrNum == CL_INVALID_PLATFORM) {printf("not a valid platform\n");} \
      else if (ErrNum == CL_INVALID_DEVICE_TYPE) {printf("not a valid device type\n");}\
      else if (ErrNum == CL_INVALID_VALUE)       {printf("not a valid value      \n");}\
      else if (ErrNum == CL_DEVICE_NOT_FOUND)    {printf("device not found       \n");}\
      else if (ErrNum == CL_OUT_OF_RESOURCES)    {printf("out of resources       \n");}\
      else if (ErrNum == CL_OUT_OF_HOST_MEMORY)  {printf("out of host memory     \n");}\
		exit (1);\
	} \
}

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

const int shrTRUE = 1;

/* simple function to read kernel cl file */
char *rw_readbuf(const char *);

/* -------------------------------------------------- */


/* fields from main.c */

#include "fields.h"

/* grid from main.c */

extern double x_h[N];
extern double x_c[N];

/* parameters from main.c */

extern double dx, dt, mass0;


void body()
{				/* body routine starts here */


    // Turn-on extra output in order to debug (set to 1 to turn-on):
    int ltrace = 0;
    int ltrace2 = 0;
    int ltrace3 = 0;
    int ltrace4 = 0;

    /* file stuff for i/o */

    FILE *fpout, *fpout2, *fpout3, *fpout4;
    double tmpmopi, logmopi_max;
    double mopi_max = 0.0;
    double mopi_time = 0.0;
    double mopi_nexttime = MOPI_DELTAT;
    double qq_buff[M];
    double rhs[N], jj[N];

    // (global) minimum of metrica:
    double res, metricax, metricah, jacobian, mina;
    double omina = 1.0;
    double gmina = 1.0;
    int minai, gminai;

    /* opencl global/local stuff */

    size_t globalWorkSize[] = { M, N };
    size_t globalWorkSize_boundary[] = { N };
    size_t globalWorkSize_boundary_m[] = { M };

    // Good for scaling (assumes N evenly divisible by 64):
    size_t localWorkSize[] = { 1, 64 };
    size_t localWorkSize_boundary[] = { 64 };
    size_t localWorkSize_boundary_m[] = { 1 };


    /* start the whole opencl dance */

    cl_int err = 0;
    size_t returned_size = 0;
    cl_platform_id cpPlatform;
    err = clGetPlatformIDs(1, &cpPlatform, 0);
    shrCheckError(err, CL_SUCCESS);

    //Get the devices
    cl_device_id cdDevices[2];
    // Use the GPU (discrete):
    err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 2, cdDevices, NULL);
    // Use the CPU cores:
    // err = clGetDeviceIDs (cpPlatform, CL_DEVICE_TYPE_CPU, 1, cdDevices, NULL);
    shrCheckError(err, CL_SUCCESS);

    // Use the CPU or GPU (depending on above lines):
    cl_device_id cdDevice = cdDevices[0];

    // Get some information about the returned device
    cl_char vendor_name[1024] = { 0 };
    cl_char device_name[1024] = { 0 };
    err = clGetDeviceInfo(cdDevice, CL_DEVICE_VENDOR, sizeof(vendor_name), vendor_name, &returned_size);
    err |= clGetDeviceInfo(cdDevice, CL_DEVICE_NAME, sizeof(device_name), device_name, &returned_size);
    shrCheckError(err, CL_SUCCESS);
    printf("Connecting to %s %s...\n", vendor_name, device_name);


    //Create the context
    cl_context gpu_context = clCreateContext(0, 1, &cdDevice, NULL, NULL, &err);

    // create a command-queue
    cl_command_queue cmd_queue = clCreateCommandQueue(gpu_context, cdDevice, 0, &err);
    shrCheckError(err, CL_SUCCESS);

    // create buffers on device


// -----------------------------
// Allocate memory on device (hence the "d_", not derivative):
//     (SLL: Also copies data from Host->Device)

    cl_mem d_dx = clCreateBuffer(gpu_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_double), &dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_dt = clCreateBuffer(gpu_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_double), &dt, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_qq = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_delta = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), delta, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_dmdr = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), dmdr, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_hamc = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), hamc, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_momc = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), momc, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_qq_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_delta_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), delta_dx, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_qq_np1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_np1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_np1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_np1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_np1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_np1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_np1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_np1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_delta_np1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), delta_np1, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_qq_k1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_k1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_k1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_k1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_k1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_k1, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_k1 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_k1, &err);
    shrCheckError(err, CL_SUCCESS);
    //
    cl_mem d_qq_k2 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_k2, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_k2 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_k2, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_k2 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_k2, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_k2 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_k2, &err);
    shrCheckError(err, CL_SUCCESS);
    //
    cl_mem d_qq_k3 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_k3, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_k3 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_k3, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_k3 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_k3, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_k3 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_k3, &err);
    shrCheckError(err, CL_SUCCESS);
    //
    cl_mem d_qq_k4 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), qq_k4, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pp_k4 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pp_k4, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_ph_k4 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), ph_k4, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_metrica_k4 = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), metrica_k4, &err);
    shrCheckError(err, CL_SUCCESS);
    //

    cl_mem d_aux_pi = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), aux_pi, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_aux_pi_dx = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), aux_pi_dx, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pqsourceint = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pqsourceint, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_pqsourcecop = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), pqsourcecop, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_rhs_qq = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), rhs_qq, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_rhs_pp = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), rhs_pp, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_rhs_ph = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), rhs_ph, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_rhs_metrica = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), rhs_metrica, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_diss_qq = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), diss_qq, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_diss_pp = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), diss_pp, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_diss_ph = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), diss_ph, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_diss_metrica = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), diss_metrica, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_diss_aux_pi = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * N * sizeof(cl_double), diss_aux_pi, &err);
    shrCheckError(err, CL_SUCCESS);

    cl_mem d_qq_buff = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, M * sizeof(cl_double), qq_buff, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_x_h = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_double), x_h, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_x_c = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_double), x_c, &err);
    shrCheckError(err, CL_SUCCESS);

    // to do integration on host
    cl_mem d_rhs = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_double), rhs, &err);
    shrCheckError(err, CL_SUCCESS);
    cl_mem d_jj = clCreateBuffer(gpu_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_double), jj, &err);
    shrCheckError(err, CL_SUCCESS);


// -----------------------------

    // read kernel file 
    char *source = rw_readbuf("kern2.cl");

    // create the program
    cl_program program = clCreateProgramWithSource(gpu_context, 1, (const char **) &source, NULL, &err);
    shrCheckError(err, CL_SUCCESS);

    // build the program
    //const char options[] = "-cl-fast-relaxed-math -cl-mad-enable";
    const char options[] = "";
    err = clBuildProgram(program, 1, &cdDevice, options, NULL, NULL);

    // Print out the error log if we fail to compile 
    if (err != CL_SUCCESS) {
	size_t len;
	char buffer[1024];

	err = clGetProgramBuildInfo(program, cdDevice, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
	err = clGetProgramBuildInfo(program, cdDevice, CL_PROGRAM_BUILD_LOG, len, buffer, NULL);

	fprintf(stderr, "Error Log -- %s\n", buffer);
	free(buffer);

    }
    // set kernel arguments

    cl_kernel kernel_derivs1 = clCreateKernel(program, "kernel_derivs1", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_derivs1, 0, sizeof(cl_mem), (void *) &d_qq_dx);
    err |= clSetKernelArg(kernel_derivs1, 1, sizeof(cl_mem), (void *) &d_pp_dx);
    err |= clSetKernelArg(kernel_derivs1, 2, sizeof(cl_mem), (void *) &d_ph_dx);
    err |= clSetKernelArg(kernel_derivs1, 3, sizeof(cl_mem), (void *) &d_metrica_dx);
    err |= clSetKernelArg(kernel_derivs1, 4, sizeof(cl_mem), (void *) &d_delta_dx);
    err |= clSetKernelArg(kernel_derivs1, 5, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_derivs1, 6, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_derivs1, 7, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_derivs1, 8, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_derivs1, 9, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_derivs1, 10, sizeof(cl_mem), (void *) &d_dx);
    err |= clSetKernelArg(kernel_derivs1, 11, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_derivs1, 12, sizeof(cl_mem), (void *) &d_aux_pi_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_derivs2 = clCreateKernel(program, "kernel_derivs2", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_derivs2, 0, sizeof(cl_mem), (void *) &d_qq_dx);
    err |= clSetKernelArg(kernel_derivs2, 1, sizeof(cl_mem), (void *) &d_pp_dx);
    err |= clSetKernelArg(kernel_derivs2, 2, sizeof(cl_mem), (void *) &d_ph_dx);
    err |= clSetKernelArg(kernel_derivs2, 3, sizeof(cl_mem), (void *) &d_metrica_dx);
    err |= clSetKernelArg(kernel_derivs2, 4, sizeof(cl_mem), (void *) &d_delta_dx);
    err |= clSetKernelArg(kernel_derivs2, 5, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_derivs2, 6, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_derivs2, 7, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_derivs2, 8, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_derivs2, 9, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_derivs2, 10, sizeof(cl_mem), (void *) &d_dx);
    err |= clSetKernelArg(kernel_derivs2, 11, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_derivs2, 12, sizeof(cl_mem), (void *) &d_aux_pi_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_calcdiss = clCreateKernel(program, "kernel_calcdiss", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_calcdiss, 0, sizeof(cl_mem), (void *) &d_diss_qq);
    err |= clSetKernelArg(kernel_calcdiss, 1, sizeof(cl_mem), (void *) &d_diss_pp);
    err |= clSetKernelArg(kernel_calcdiss, 2, sizeof(cl_mem), (void *) &d_diss_ph);
    err |= clSetKernelArg(kernel_calcdiss, 3, sizeof(cl_mem), (void *) &d_diss_metrica);
    err |= clSetKernelArg(kernel_calcdiss, 4, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_calcdiss, 5, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_calcdiss, 6, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_calcdiss, 7, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_calcdiss, 8, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_calcdiss, 9, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_calcdiss, 10, sizeof(cl_mem), (void *) &d_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_calcdissn = clCreateKernel(program, "kernel_calcdissn", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_calcdissn, 0, sizeof(cl_mem), (void *) &d_diss_qq);
    err |= clSetKernelArg(kernel_calcdissn, 1, sizeof(cl_mem), (void *) &d_diss_pp);
    err |= clSetKernelArg(kernel_calcdissn, 2, sizeof(cl_mem), (void *) &d_diss_ph);
    err |= clSetKernelArg(kernel_calcdissn, 3, sizeof(cl_mem), (void *) &d_diss_metrica);
    err |= clSetKernelArg(kernel_calcdissn, 4, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_calcdissn, 5, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_calcdissn, 6, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_calcdissn, 7, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_calcdissn, 8, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_calcdissn, 9, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_calcdissn, 10, sizeof(cl_mem), (void *) &d_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_rhs1 = clCreateKernel(program, "kernel_rhs1", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_rhs1, 0, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_rhs1, 1, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_rhs1, 2, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_rhs1, 3, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_rhs1, 4, sizeof(cl_mem), (void *) &d_diss_qq);
    err |= clSetKernelArg(kernel_rhs1, 5, sizeof(cl_mem), (void *) &d_diss_pp);
    err |= clSetKernelArg(kernel_rhs1, 6, sizeof(cl_mem), (void *) &d_diss_ph);
    err |= clSetKernelArg(kernel_rhs1, 7, sizeof(cl_mem), (void *) &d_diss_metrica);
    err |= clSetKernelArg(kernel_rhs1, 8, sizeof(cl_mem), (void *) &d_qq_dx);
    err |= clSetKernelArg(kernel_rhs1, 9, sizeof(cl_mem), (void *) &d_pp_dx);
    err |= clSetKernelArg(kernel_rhs1, 10, sizeof(cl_mem), (void *) &d_ph_dx);
    err |= clSetKernelArg(kernel_rhs1, 11, sizeof(cl_mem), (void *) &d_metrica_dx);
    err |= clSetKernelArg(kernel_rhs1, 12, sizeof(cl_mem), (void *) &d_delta_dx);
    err |= clSetKernelArg(kernel_rhs1, 13, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_rhs1, 14, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_rhs1, 15, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_rhs1, 16, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_rhs1, 17, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_rhs1, 18, sizeof(cl_mem), (void *) &d_dx);
    err |= clSetKernelArg(kernel_rhs1, 19, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_rhs1, 20, sizeof(cl_mem), (void *) &d_aux_pi_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_rhs2 = clCreateKernel(program, "kernel_rhs2", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_rhs2, 0, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_rhs2, 1, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_rhs2, 2, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_rhs2, 3, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_rhs2, 4, sizeof(cl_mem), (void *) &d_diss_qq);
    err |= clSetKernelArg(kernel_rhs2, 5, sizeof(cl_mem), (void *) &d_diss_pp);
    err |= clSetKernelArg(kernel_rhs2, 6, sizeof(cl_mem), (void *) &d_diss_ph);
    err |= clSetKernelArg(kernel_rhs2, 7, sizeof(cl_mem), (void *) &d_diss_metrica);
    err |= clSetKernelArg(kernel_rhs2, 8, sizeof(cl_mem), (void *) &d_qq_dx);
    err |= clSetKernelArg(kernel_rhs2, 9, sizeof(cl_mem), (void *) &d_pp_dx);
    err |= clSetKernelArg(kernel_rhs2, 10, sizeof(cl_mem), (void *) &d_ph_dx);
    err |= clSetKernelArg(kernel_rhs2, 11, sizeof(cl_mem), (void *) &d_metrica_dx);
    err |= clSetKernelArg(kernel_rhs2, 12, sizeof(cl_mem), (void *) &d_delta_dx);
    err |= clSetKernelArg(kernel_rhs2, 13, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_rhs2, 14, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_rhs2, 15, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_rhs2, 16, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_rhs2, 17, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_rhs2, 18, sizeof(cl_mem), (void *) &d_dx);
    err |= clSetKernelArg(kernel_rhs2, 19, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_rhs2, 20, sizeof(cl_mem), (void *) &d_aux_pi_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_rhs3 = clCreateKernel(program, "kernel_rhs3", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_rhs3, 0, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_rhs3, 1, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_rhs3, 2, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_rhs3, 3, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_rhs3, 4, sizeof(cl_mem), (void *) &d_diss_qq);
    err |= clSetKernelArg(kernel_rhs3, 5, sizeof(cl_mem), (void *) &d_diss_pp);
    err |= clSetKernelArg(kernel_rhs3, 6, sizeof(cl_mem), (void *) &d_diss_ph);
    err |= clSetKernelArg(kernel_rhs3, 7, sizeof(cl_mem), (void *) &d_diss_metrica);
    err |= clSetKernelArg(kernel_rhs3, 8, sizeof(cl_mem), (void *) &d_qq_dx);
    err |= clSetKernelArg(kernel_rhs3, 9, sizeof(cl_mem), (void *) &d_pp_dx);
    err |= clSetKernelArg(kernel_rhs3, 10, sizeof(cl_mem), (void *) &d_ph_dx);
    err |= clSetKernelArg(kernel_rhs3, 11, sizeof(cl_mem), (void *) &d_metrica_dx);
    err |= clSetKernelArg(kernel_rhs3, 12, sizeof(cl_mem), (void *) &d_delta_dx);
    err |= clSetKernelArg(kernel_rhs3, 13, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_rhs3, 14, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_rhs3, 15, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_rhs3, 16, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_rhs3, 17, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_rhs3, 18, sizeof(cl_mem), (void *) &d_dx);
    err |= clSetKernelArg(kernel_rhs3, 19, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_rhs3, 20, sizeof(cl_mem), (void *) &d_aux_pi_dx);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_update1 = clCreateKernel(program, "kernel_update1", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_update1, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_update1, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_update1, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_update1, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_update1, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_update1, 5, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_update1, 6, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_update1, 7, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_update1, 8, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_update1, 9, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_update1, 10, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_update1, 11, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_update1, 12, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_update1, 13, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_update1, 14, sizeof(cl_mem), (void *) &d_dt);
    err |= clSetKernelArg(kernel_update1, 15, sizeof(cl_mem), (void *) &d_qq_k1);
    err |= clSetKernelArg(kernel_update1, 16, sizeof(cl_mem), (void *) &d_pp_k1);
    err |= clSetKernelArg(kernel_update1, 17, sizeof(cl_mem), (void *) &d_ph_k1);
    err |= clSetKernelArg(kernel_update1, 18, sizeof(cl_mem), (void *) &d_metrica_k1);
    err |= clSetKernelArg(kernel_update1, 19, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_update1, 20, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_update1, 21, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_update2 = clCreateKernel(program, "kernel_update2", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_update2, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_update2, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_update2, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_update2, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_update2, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_update2, 5, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_update2, 6, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_update2, 7, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_update2, 8, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_update2, 9, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_update2, 10, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_update2, 11, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_update2, 12, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_update2, 13, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_update2, 14, sizeof(cl_mem), (void *) &d_dt);
    err |= clSetKernelArg(kernel_update2, 15, sizeof(cl_mem), (void *) &d_qq_k2);
    err |= clSetKernelArg(kernel_update2, 16, sizeof(cl_mem), (void *) &d_pp_k2);
    err |= clSetKernelArg(kernel_update2, 17, sizeof(cl_mem), (void *) &d_ph_k2);
    err |= clSetKernelArg(kernel_update2, 18, sizeof(cl_mem), (void *) &d_metrica_k2);
    err |= clSetKernelArg(kernel_update2, 19, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_update2, 20, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_update2, 21, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_update3 = clCreateKernel(program, "kernel_update3", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_update3, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_update3, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_update3, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_update3, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_update3, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_update3, 5, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_update3, 6, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_update3, 7, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_update3, 8, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_update3, 9, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_update3, 10, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_update3, 11, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_update3, 12, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_update3, 13, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_update3, 14, sizeof(cl_mem), (void *) &d_dt);
    err |= clSetKernelArg(kernel_update3, 15, sizeof(cl_mem), (void *) &d_qq_k3);
    err |= clSetKernelArg(kernel_update3, 16, sizeof(cl_mem), (void *) &d_pp_k3);
    err |= clSetKernelArg(kernel_update3, 17, sizeof(cl_mem), (void *) &d_ph_k3);
    err |= clSetKernelArg(kernel_update3, 18, sizeof(cl_mem), (void *) &d_metrica_k3);
    err |= clSetKernelArg(kernel_update3, 19, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_update3, 20, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_update3, 21, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_update4 = clCreateKernel(program, "kernel_update4", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_update4, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_update4, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_update4, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_update4, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_update4, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_update4, 5, sizeof(cl_mem), (void *) &d_rhs_qq);
    err |= clSetKernelArg(kernel_update4, 6, sizeof(cl_mem), (void *) &d_rhs_pp);
    err |= clSetKernelArg(kernel_update4, 7, sizeof(cl_mem), (void *) &d_rhs_ph);
    err |= clSetKernelArg(kernel_update4, 8, sizeof(cl_mem), (void *) &d_rhs_metrica);
    err |= clSetKernelArg(kernel_update4, 9, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_update4, 10, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_update4, 11, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_update4, 12, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_update4, 13, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_update4, 14, sizeof(cl_mem), (void *) &d_dt);
    err |= clSetKernelArg(kernel_update4, 15, sizeof(cl_mem), (void *) &d_qq_k4);
    err |= clSetKernelArg(kernel_update4, 16, sizeof(cl_mem), (void *) &d_pp_k4);
    err |= clSetKernelArg(kernel_update4, 17, sizeof(cl_mem), (void *) &d_ph_k4);
    err |= clSetKernelArg(kernel_update4, 18, sizeof(cl_mem), (void *) &d_metrica_k4);
    err |= clSetKernelArg(kernel_update4, 19, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_update4, 20, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_update4, 21, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_boundary = clCreateKernel(program, "kernel_boundary", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_boundary, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_boundary, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_boundary, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_boundary, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_boundary, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_boundary, 5, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_boundary, 6, sizeof(cl_mem), (void *) &d_qq_buff);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_rkfinal = clCreateKernel(program, "kernel_rkfinal", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_rkfinal, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_rkfinal, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_rkfinal, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_rkfinal, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_rkfinal, 4, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_rkfinal, 5, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_rkfinal, 6, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_rkfinal, 7, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_rkfinal, 8, sizeof(cl_mem), (void *) &d_qq_k1);
    err |= clSetKernelArg(kernel_rkfinal, 9, sizeof(cl_mem), (void *) &d_pp_k1);
    err |= clSetKernelArg(kernel_rkfinal, 10, sizeof(cl_mem), (void *) &d_ph_k1);
    err |= clSetKernelArg(kernel_rkfinal, 11, sizeof(cl_mem), (void *) &d_metrica_k1);
    err |= clSetKernelArg(kernel_rkfinal, 12, sizeof(cl_mem), (void *) &d_qq_k2);
    err |= clSetKernelArg(kernel_rkfinal, 13, sizeof(cl_mem), (void *) &d_pp_k2);
    err |= clSetKernelArg(kernel_rkfinal, 14, sizeof(cl_mem), (void *) &d_ph_k2);
    err |= clSetKernelArg(kernel_rkfinal, 15, sizeof(cl_mem), (void *) &d_metrica_k2);
    err |= clSetKernelArg(kernel_rkfinal, 16, sizeof(cl_mem), (void *) &d_qq_k3);
    err |= clSetKernelArg(kernel_rkfinal, 17, sizeof(cl_mem), (void *) &d_pp_k3);
    err |= clSetKernelArg(kernel_rkfinal, 18, sizeof(cl_mem), (void *) &d_ph_k3);
    err |= clSetKernelArg(kernel_rkfinal, 19, sizeof(cl_mem), (void *) &d_metrica_k3);
    err |= clSetKernelArg(kernel_rkfinal, 20, sizeof(cl_mem), (void *) &d_qq_k4);
    err |= clSetKernelArg(kernel_rkfinal, 21, sizeof(cl_mem), (void *) &d_pp_k4);
    err |= clSetKernelArg(kernel_rkfinal, 22, sizeof(cl_mem), (void *) &d_ph_k4);
    err |= clSetKernelArg(kernel_rkfinal, 23, sizeof(cl_mem), (void *) &d_metrica_k4);
    err |= clSetKernelArg(kernel_rkfinal, 24, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_rkfinal, 25, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_rkfinal, 26, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_rkfinal, 27, sizeof(cl_mem), (void *) &d_diss_aux_pi);
    err |= clSetKernelArg(kernel_rkfinal, 28, sizeof(cl_mem), (void *) &d_dt);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_swapnn1 = clCreateKernel(program, "kernel_swapnn1", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_swapnn1, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_swapnn1, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_swapnn1, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_swapnn1, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_swapnn1, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_swapnn1, 5, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_swapnn1, 6, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_swapnn1, 7, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_swapnn1, 8, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_swapnn1, 9, sizeof(cl_mem), (void *) &d_delta);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_compauxpi = clCreateKernel(program, "kernel_compauxpi", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_compauxpi, 0, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_compauxpi, 1, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_compauxpi, 2, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_compauxpi, 3, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_compauxpi, 4, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_compauxpi2 = clCreateKernel(program, "kernel_compauxpi2", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_compauxpi2, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_compauxpi2, 1, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_compauxpi2, 2, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_compauxpi2, 3, sizeof(cl_mem), (void *) &d_aux_pi);
    err |= clSetKernelArg(kernel_compauxpi2, 4, sizeof(cl_mem), (void *) &d_x_c);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_compaux = clCreateKernel(program, "kernel_compaux", &err);
    shrCheckError(err, CL_SUCCESS);

    err |= clSetKernelArg(kernel_compaux, 0, sizeof(cl_mem), (void *) &d_dmdr);
    err |= clSetKernelArg(kernel_compaux, 1, sizeof(cl_mem), (void *) &d_hamc);
    err |= clSetKernelArg(kernel_compaux, 2, sizeof(cl_mem), (void *) &d_momc);
    err |= clSetKernelArg(kernel_compaux, 3, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_compaux, 4, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_compaux, 5, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_compaux, 6, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_compaux, 7, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_compaux, 8, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_compaux, 9, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_compaux, 10, sizeof(cl_mem), (void *) &d_dt);
    shrCheckError(err, CL_SUCCESS);

    cl_kernel kernel_relax2 = clCreateKernel(program, "kernel_relax2", &err);
    shrCheckError(err, CL_SUCCESS);

    err = clSetKernelArg(kernel_relax2, 0, sizeof(cl_mem), (void *) &d_qq_np1);
    err |= clSetKernelArg(kernel_relax2, 1, sizeof(cl_mem), (void *) &d_pp_np1);
    err |= clSetKernelArg(kernel_relax2, 2, sizeof(cl_mem), (void *) &d_ph_np1);
    err |= clSetKernelArg(kernel_relax2, 3, sizeof(cl_mem), (void *) &d_metrica_np1);
    err |= clSetKernelArg(kernel_relax2, 4, sizeof(cl_mem), (void *) &d_delta_np1);
    err |= clSetKernelArg(kernel_relax2, 5, sizeof(cl_mem), (void *) &d_qq);
    err |= clSetKernelArg(kernel_relax2, 6, sizeof(cl_mem), (void *) &d_pp);
    err |= clSetKernelArg(kernel_relax2, 7, sizeof(cl_mem), (void *) &d_ph);
    err |= clSetKernelArg(kernel_relax2, 8, sizeof(cl_mem), (void *) &d_metrica);
    err |= clSetKernelArg(kernel_relax2, 9, sizeof(cl_mem), (void *) &d_delta);
    err |= clSetKernelArg(kernel_relax2, 10, sizeof(cl_mem), (void *) &d_x_c);
    err |= clSetKernelArg(kernel_relax2, 11, sizeof(cl_mem), (void *) &d_rhs);
    err |= clSetKernelArg(kernel_relax2, 12, sizeof(cl_mem), (void *) &d_jj);
    err |= clSetKernelArg(kernel_relax2, 13, sizeof(cl_mem), (void *) &d_pqsourceint);
    err |= clSetKernelArg(kernel_relax2, 14, sizeof(cl_mem), (void *) &d_pqsourcecop);
    shrCheckError(err, CL_SUCCESS);


    /* opencl initiation complete! */

    /* set up time stepping parameters */

    int j, i;
    unsigned long int k;
    double timer = 0.0;
    double endtime = T;
    double mass;
    unsigned long int nt = (int) ((endtime - timer) / dt);

    /* variables for integrating metrica and delta in space: */
    double xh, cosxh, deltax, phih, pih, deltadiff, tmp;
    double cosxj, sinxj, cosxjh, sinxjh, cosxjp1, sinxjp1, Sj, Sjh, Sjp1;
    double ft, k1, ftphh_yphk1, ftphh_yphk2, k3, ftph_yk3, k4, olddelta, metricadiff;
    double h, k2;
    double sinx, cosx;
    double J, K, totalres;
    double ajhat, aj;
    double ak1hat, ak2hat, ak3hat, ajp1hat, oldmetrica;


    /* create output file */

    fpout = fopen("qq_out.dat", "w");	/*  for output */
    fpout2 = fopen("mina_out.dat", "w");	/*  for output */
    fpout4 = fopen("mopi.dat", "w");	/*  for output */

    // Initialize SDF-related quantities:
    int rc;
    double bbox[2], bboxj[2];
    bbox[0] = Xmin;
    bbox[1] = Xmax;
    // for outputting mode decomposition quantities:
    bboxj[0] = 0.0;
    bboxj[1] = 60.0;
    char fname[8];
    char gft_name[64];
    int rank = 1;
    int gftShape[1], gftShapej[1];
    gftShape[0] = N;
    gftShapej[0] = 61;
#ifdef SDF
    if (ltrace)
	printf("body: Outputting sdf at t=%f rank=%d over (%f,%f) N=%d \n", timer, rank, bbox[0], bbox[1], gftShape[0]);
    if (ltrace4) {
	// Just for debugging:
	strcpy(fname, "aux_pi");
	sprintf(gft_name, "%d%s", 0, fname);
	rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, aux_pi);
	if (rc != 1) {
	    printf("body: Error w/ gft_out_bbox() aux_pi rc=%d\n", rc);
	}
	// Just for debugging:
	strcpy(fname, "auxpidx");
	sprintf(gft_name, "%d%s", 0, fname);
	rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, aux_pi_dx);
	if (rc != 1) {
	    printf("body: Error w/ gft_out_bbox() aux_pi_dx rc=%d\n", rc);
	}
	strcpy(fname, "ph");
	sprintf(gft_name, "%d%s", 0, fname);
	rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, ph);
	if (rc != 1) {
	    printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	}
	//
    }
    //
    strcpy(fname, "qq");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, qq);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() QQ rc=%d\n", rc);
    }
    //
    strcpy(fname, "pp");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, pp);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PP rc=%d\n", rc);
    }
    //
    strcpy(fname, "delta");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, delta);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
    }
    //
    strcpy(fname, "metrica");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, metrica);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
    }
    //
    strcpy(fname, "dmdr");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, dmdr);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
    }
    //
    strcpy(fname, "hamc");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, hamc);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
    }
    //
    strcpy(fname, "momc");
    sprintf(gft_name, "%d%s", 0, fname);
    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, momc);
    if (rc != 1) {
	printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
    }
    if (ltrace)
	printf("Done w/SDF output\n");
#endif

    /* finally start main time iterations loop! */

    for (k = 1; k <= nt; k++) {
	if (ltrace2)
	    printf("body: calling kernel_compauxpi\n");
	//  
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_compauxpi, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_derivs1\n");
	//  takes the derivatives of the _n fields:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_derivs1, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (EPSDIS > 1e-10) {
	    // probably not necessary at multiple iterations
	    if (ltrace2)
		printf("body: calling kernel_calcdissn\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_calcdissn, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	}

	if (ltrace2) {
	    // just for debugging:
	    err = clEnqueueReadBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), &delta_np1, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_delta_dx, CL_TRUE, 0, N * M * sizeof(double), &delta_dx, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    printf("body: ||delta_np1||_2      = %f \n", L2Norm(delta_np1, N));
	    printf("body: ||delta    ||_2      = %f \n", L2Norm(delta, N));
	    printf("body: ||delta_dx ||_2      = %f \n", L2Norm(delta_dx, N));
	}

	if (ltrace2)
	    printf("body: calling kernel_rhs1\n");
	//  Computes the time derivatives w/r/t the _n fields:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_rhs1, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_update1\n");
	//  Updates the _np1 fields based on the _n righthandsides:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_update1, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_boundary\n");
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_boundary, 1, NULL, globalWorkSize_boundary_m, localWorkSize_boundary_m, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (PUREADS == 0) {
	    if (ltrace2)
		printf("body: Pulling data from device needed to spatially integrate delta\n");
	    err = clEnqueueReadBuffer(cmd_queue, d_pp_np1, CL_TRUE, 0, N * M * sizeof(double), &pp, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_qq_np1, CL_TRUE, 0, N * M * sizeof(double), &qq, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace3) {
		printf("body: For delta solve ||qq||_2      = %f \n", L2Norm(qq, N));
		printf("body: For delta solve ||pp||_2      = %f \n", L2Norm(pp, N));
	    }
	    calcdeltanoRBnotrescaledRK4(delta_np1, diss_ph, qq, pp, x_c, N);
	    if (ltrace2)
		printf("body: Solved for ||delta_np1||=%f\n", L2Norm(delta_np1, N));
	    //
	    if (ltrace3)
		printf("body: Sending data to   device for  the spatially integrated delta\n");
	    err = clEnqueueWriteBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), delta_np1, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	}
	//  ..................................Done with iter 1 of RK

	if (ltrace2)
	    printf("body: calling kernel_compauxpi2\n");
	//  
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_compauxpi2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_derivs2\n");
	//  takes the derivatives of the _np1 fields:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_derivs2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (EPSDIS > 1e-10) {
	    // probably not necessary at multiple iterations
	    if (ltrace2)
		printf("body: calling kernel_calcdiss\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_calcdiss, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	}

	if (ltrace2) {
	    // just for debugging:
	    err = clEnqueueReadBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), &delta_np1, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_delta_dx, CL_TRUE, 0, N * M * sizeof(double), &delta_dx, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    printf("body: ||delta_np1||_2      = %f \n", L2Norm(delta_np1, N));
	    printf("body: ||delta    ||_2      = %f \n", L2Norm(delta, N));
	    printf("body: ||delta_dx ||_2      = %f \n", L2Norm(delta_dx, N));
	}

	if (ltrace2)
	    printf("body: calling kernel_rhs2\n");
	//  Computes the time derivatives w/r/t the _np1 fields:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_rhs2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_update2\n");
	//  Updates the _np1 fields based on the righthandsides:
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_update2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_boundary\n");
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_boundary, 1, NULL, globalWorkSize_boundary_m, localWorkSize_boundary_m, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (PUREADS == 0) {
	    if (ltrace2)
		printf("body: calling kernel_relax2 j=%d \n", j);
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_relax2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace2)
		printf("body: Pulling data from device needed to spatially integrate delta\n");
	    err = clEnqueueReadBuffer(cmd_queue, d_pqsourceint, CL_TRUE, 0, N * M * sizeof(double), &pqsourceint, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_pqsourcecop, CL_TRUE, 0, N * M * sizeof(double), &pqsourcecop, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace2) {
		printf("body: For delta solve ||pqsourceint||_2 = %f \n", L2Norm(pqsourceint, N));
		printf("body: For delta solve ||pqsourcecop||_2 = %f \n", L2Norm(pqsourcecop, N));
	    }
	    calcdeltanoRBnotrescaledRK4pq(delta_np1, pqsourceint, pqsourcecop, x_c, N);

	    copy(delta, delta_np1, N);
	    if (ltrace2)
		printf("body: Solved for ||delta_np1||=%f\n", L2Norm(delta_np1, N));
	    if (ltrace2)
		printf("body:            ||delta_n  ||=%f\n", L2Norm(delta, N));
	    //
	    if (ltrace3)
		printf("body: Sending data to   device for  the spatially integrated delta\n");
	    err = clEnqueueWriteBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), delta_np1, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	}
	//  ..................................Done with iter 2 of RK

	if (RKORDER > 2) {

	    if (ltrace2)
		printf("body: calling kernel_compauxpi2\n");
	    //  
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_compauxpi2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_derivs2\n");
	    //  takes the derivatives of the _np1 fields:
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_derivs2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (EPSDIS > 1e-10) {
		if (ltrace2)
		    printf("body: calling kernel_calcdiss\n");
		err = clEnqueueNDRangeKernel(cmd_queue, kernel_calcdiss, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
	    }

	    if (ltrace2) {
		// just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), &delta_np1, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta_dx, CL_TRUE, 0, N * M * sizeof(double), &delta_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		printf("body: ||delta_np1||_2      = %f \n", L2Norm(delta_np1, N));
		printf("body: ||delta    ||_2      = %f \n", L2Norm(delta, N));
		printf("body: ||delta_dx ||_2      = %f \n", L2Norm(delta_dx, N));
	    }

	    if (ltrace2)
		printf("body: calling kernel_rhs3\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_rhs3, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_update3\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_update3, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_boundary\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_boundary, 1, NULL, globalWorkSize_boundary_m, localWorkSize_boundary_m, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (PUREADS == 0) {
		if (ltrace2)
		    printf("body: calling kernel_relax2 j=%d \n", j);
		err = clEnqueueNDRangeKernel(cmd_queue, kernel_relax2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		if (ltrace2)
		    printf("body: Pulling data from device needed to spatially integrate delta\n");
		err = clEnqueueReadBuffer(cmd_queue, d_pqsourceint, CL_TRUE, 0, N * M * sizeof(double), &pqsourceint, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_pqsourcecop, CL_TRUE, 0, N * M * sizeof(double), &pqsourcecop, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		if (ltrace2) {
		    printf("body: For delta solve ||pqsourceint||_2 = %f \n", L2Norm(pqsourceint, N));
		    printf("body: For delta solve ||pqsourcecop||_2 = %f \n", L2Norm(pqsourcecop, N));
		}
		calcdeltanoRBnotrescaledRK4pq(delta_np1, pqsourceint, pqsourcecop, x_c, N);

		copy(delta, delta_np1, N);
		if (ltrace2)
		    printf("body: Solved for ||delta_np1||=%f\n", L2Norm(delta_np1, N));
		if (ltrace2)
		    printf("body:            ||delta_n  ||=%f\n", L2Norm(delta, N));
		//
		if (ltrace3)
		    printf("body: Sending data to   device for  the spatially integrated delta\n");
		err = clEnqueueWriteBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), delta_np1, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		//
	    }

	}			// if RKORDER>2 

	//  ..................................Done with iter 3 of RK

	if (RKORDER > 3) {

	    if (ltrace2)
		printf("body: calling kernel_compauxpi2\n");
	    //  
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_compauxpi2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_derivs2\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_derivs2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (EPSDIS > 1e-10) {
		if (ltrace2)
		    printf("body: calling kernel_calcdiss\n");
		err = clEnqueueNDRangeKernel(cmd_queue, kernel_calcdiss, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
	    }

	    if (ltrace2) {
		// just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), &delta_np1, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta_dx, CL_TRUE, 0, N * M * sizeof(double), &delta_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		printf("body: ||delta_np1||_2      = %f \n", L2Norm(delta_np1, N));
		printf("body: ||delta    ||_2      = %f \n", L2Norm(delta, N));
		printf("body: ||delta_dx ||_2      = %f \n", L2Norm(delta_dx, N));
	    }

	    if (ltrace2)
		printf("body: calling kernel_rhs3\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_rhs3, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_update4\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_update4, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (ltrace2)
		printf("body: calling kernel_boundary\n");
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_boundary, 1, NULL, globalWorkSize_boundary_m, localWorkSize_boundary_m, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    if (PUREADS == 0) {
		if (ltrace2)
		    printf("body: calling kernel_relax2 j=%d \n", j);
		err = clEnqueueNDRangeKernel(cmd_queue, kernel_relax2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		if (ltrace2)
		    printf("body: Pulling data from device needed to spatially integrate delta\n");
		err = clEnqueueReadBuffer(cmd_queue, d_pqsourceint, CL_TRUE, 0, N * M * sizeof(double), &pqsourceint, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_pqsourcecop, CL_TRUE, 0, N * M * sizeof(double), &pqsourcecop, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		if (ltrace2) {
		    printf("body: For delta solve ||pqsourceint||_2 = %f \n", L2Norm(pqsourceint, N));
		    printf("body: For delta solve ||pqsourcecop||_2 = %f \n", L2Norm(pqsourcecop, N));
		}
		calcdeltanoRBnotrescaledRK4pq(delta_np1, pqsourceint, pqsourcecop, x_c, N);

		copy(delta, delta_np1, N);
		if (ltrace2)
		    printf("body: Solved for ||delta_np1||=%f\n", L2Norm(delta_np1, N));
		if (ltrace2)
		    printf("body:            ||delta_n  ||=%f\n", L2Norm(delta, N));
		//
		if (ltrace3)
		    printf("body: Sending data to   device for  the spatially integrated delta\n");
		err = clEnqueueWriteBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), delta_np1, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		//
	    }
	    //  ..................................Done with all RK steps:
	    if (ltrace2) {
		// just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), &delta_np1, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		err = clEnqueueReadBuffer(cmd_queue, d_delta_dx, CL_TRUE, 0, N * M * sizeof(double), &delta_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		printf("body: ||delta_np1||_2      = %f \n", L2Norm(delta_np1, N));
		printf("body: ||delta    ||_2      = %f \n", L2Norm(delta, N));
		printf("body: ||delta_dx ||_2      = %f \n", L2Norm(delta_dx, N));
	    }

	    if (ltrace2)
		printf("body: calling kernel_rkfinal\n");
	    // For RK4, one has to add up the _k1, _k2, _k3, and _k4 terms (wouldn't need with a low-storage method)
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_rkfinal, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	}			// if RKORDER>3 

	if (PUREADS == 0) {
	    if (ltrace2)
		printf("body: calling kernel_relax2 j=%d \n", j);
	    err = clEnqueueNDRangeKernel(cmd_queue, kernel_relax2, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace2)
		printf("body: Pulling data from device needed to spatially integrate delta\n");
	    err = clEnqueueReadBuffer(cmd_queue, d_pqsourceint, CL_TRUE, 0, N * M * sizeof(double), &pqsourceint, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    err = clEnqueueReadBuffer(cmd_queue, d_pqsourcecop, CL_TRUE, 0, N * M * sizeof(double), &pqsourcecop, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace2) {
		printf("body: For delta solve ||pqsourceint||_2 = %f \n", L2Norm(pqsourceint, N));
		printf("body: For delta solve ||pqsourcecop||_2 = %f \n", L2Norm(pqsourcecop, N));
	    }
	    calcdeltanoRBnotrescaledRK4pq(delta_np1, pqsourceint, pqsourcecop, x_c, N);

	    copy(delta, delta_np1, N);
	    if (ltrace2)
		printf("body: Solved for ||delta_np1||=%f\n", L2Norm(delta_np1, N));
	    if (ltrace2)
		printf("body:            ||delta_n  ||=%f\n", L2Norm(delta, N));
	    //
	    if (ltrace3)
		printf("body: Sending data to   device for  the spatially integrated delta\n");
	    err = clEnqueueWriteBuffer(cmd_queue, d_delta_np1, CL_TRUE, 0, N * M * sizeof(double), delta_np1, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    if (!(k % (int) INTAFREQ)) {
		mina = 1.0;
		// using diss_ph as just a work array:
		//    (qq and pp should have been brought from device for delta solve already)
		calcmetricanoRBnotrescaled(metrica_np1, diss_ph, qq, pp, x_c, N);
		printf("body: Solved for ||metrica_np1||=%f\n", L2Norm(metrica_np1, N));
		// Figure out diagnostics (does not work yet):
		if (mina < gmina) {
		    gmina = mina;
		    gminai = minai;
		}
		mass = 0.0;
		for (i = 1; i < N; i++) {
		    mass = mass + 0.5 * (dmdr[i] + dmdr[i - 1]) * dx;
		}
		double dmass;
		dmass = (mass - mass0) / mass0;
		// Transfer metrica solution back to device:
		err = clEnqueueWriteBuffer(cmd_queue, d_metrica_np1, CL_TRUE, 0, N * M * sizeof(double), metrica_np1, 0, NULL, NULL);
		// err = clEnqueueWriteBuffer (cmd_queue, d_metrica, CL_TRUE, 0, N * M * sizeof (double), metrica, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Get value of "Pi" at origin:
		err = clEnqueueReadBuffer(cmd_queue, d_qq_buff, CL_TRUE, 0, M * sizeof(double), &qq_buff, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Output mopi:
		tmpmopi = pow(qq_buff[0], 2);
		if (ltrace2)
		    printf("tmpmopi=%f mopi_max=%f timer=%f mopi_nexttime=%f \n", tmpmopi, mopi_max, timer, mopi_nexttime);
		if (mopi_max < tmpmopi) {
		    mopi_max = tmpmopi;
		    mopi_time = timer;
		    if (ltrace2)
			printf("body: Found new maximum=%f\n", mopi_max);
		}
		if (timer > mopi_nexttime) {
		    if (ltrace2)
			printf("body: outputting max and starting new period %f\n", mopi_nexttime);
		    // Output data for just completed period:
		    logmopi_max = log10(mopi_max);
		    fprintf(fpout4, "%f %g %g %g\n", mopi_time, logmopi_max, gmina, dmass);
		    // Compute when next period ends:
		    mopi_nexttime = mopi_nexttime + MOPI_DELTAT;
		    // Reset max for next period:
		    mopi_max = -9999.9;
		}
		// Output min(a):
		if (omina != mina) {
		    omina = mina;
		    fprintf(fpout2, "%f %g %g %g %d %g\n", timer, mina, gmina, x_c[minai], minai, dmass);
		    if (mina < MINABHTHRESH) {
			fpout3 = fopen("BH.dat", "w");	/*  for output */
			fprintf(fpout3, "%f %g %g %g %d %g\n", timer, mina, gmina, x_c[minai], minai, dmass);
			fclose(fpout);
			fclose(fpout2);
			fclose(fpout3);
			fclose(fpout4);
			printf("body: BH formed, quitting.\n");
#ifdef SDF
			printf("body: Closing SDF files\n");
			gft_close_all();	// close all sdf output files
#endif
			exit(0);
		    }
		}
	    }			// end of INTAFREQ solve
	}			// end of final metric solve

	if (ltrace2)
	    printf("body: calling kernel_swapnn1\n");
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_swapnn1, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	if (ltrace2)
	    printf("body: calling kernel_compaux\n");
	err = clEnqueueNDRangeKernel(cmd_queue, kernel_compaux, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
	shrCheckError(err, CL_SUCCESS);
	clFinish(cmd_queue);

	/* output preparation */


	/* finish iteration */
	timer += dt;
	if (ltrace2)
	    printf("body: iter k=%d timer=%f \n", k, timer);

	if (!(k % (int) Fr)) {
	    // bring data from device 
	    if (ltrace2)
		printf("body: Bring data back to host  k=%d timer=%f \n", k, timer);
	    //
	    //Just bring back one point:
	    err = clEnqueueReadBuffer(cmd_queue, d_qq_buff, CL_TRUE, 0, M * sizeof(double), &qq_buff, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    //Bring back all fields
	    err = clEnqueueReadBuffer(cmd_queue, d_qq, CL_TRUE, 0, N * M * sizeof(double), &qq, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    err = clEnqueueReadBuffer(cmd_queue, d_pp, CL_TRUE, 0, N * M * sizeof(double), &pp, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    if (ltrace2) {
		// Just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_qq_dx, CL_TRUE, 0, N * M * sizeof(double), &qq_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_pp_dx, CL_TRUE, 0, N * M * sizeof(double), &pp_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_aux_pi, CL_TRUE, 0, N * M * sizeof(double), &aux_pi, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_aux_pi_dx, CL_TRUE, 0, N * M * sizeof(double), &aux_pi_dx, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
		// Just for debugging:
		err = clEnqueueReadBuffer(cmd_queue, d_diss_aux_pi, CL_TRUE, 0, N * M * sizeof(double), &diss_aux_pi, 0, NULL, NULL);
		shrCheckError(err, CL_SUCCESS);
		clFinish(cmd_queue);
	    }
	    err = clEnqueueReadBuffer(cmd_queue, d_metrica, CL_TRUE, 0, N * M * sizeof(double), &metrica, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);

	    err = clEnqueueReadBuffer(cmd_queue, d_delta, CL_TRUE, 0, N * M * sizeof(double), &delta, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    err = clEnqueueReadBuffer(cmd_queue, d_dmdr, CL_TRUE, 0, N * M * sizeof(double), &dmdr, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    err = clEnqueueReadBuffer(cmd_queue, d_hamc, CL_TRUE, 0, N * M * sizeof(double), &hamc, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    //
	    err = clEnqueueReadBuffer(cmd_queue, d_momc, CL_TRUE, 0, N * M * sizeof(double), &momc, 0, NULL, NULL);
	    shrCheckError(err, CL_SUCCESS);
	    clFinish(cmd_queue);
	    if (ltrace2) {
		printf("body: Done bringing data back to host  k=%d timer=%f \n", k, timer);
		printf("body: ||dmdr||_2    = %f \n", L2Norm(dmdr, N));
		printf("body: ||metrica||_2 = %f \n", L2Norm(metrica, N));
		printf("body: ||delta||_2   = %f \n", L2Norm(delta, N));
		printf("body: ||qq||_2      = %f \n", L2Norm(qq, N));
		printf("body: ||pp||_2      = %f \n", L2Norm(pp, N));
		printf("body: ||ph||_2      = %f \n", L2Norm(ph, N));
		printf("body: ||aux_pi||_2  = %f \n", L2Norm(aux_pi, N));
	    }
	    // in case minai has not been computed:
	    // if (minai< 0 ) minai = 0;
	    // if (minai>N-1) minai = N-1;
	    mina = 1.0;
	    for (i = 1; i < N; i++) {
		if (metrica[i] < mina) {
		    mina = metrica[i];
		    minai = i;
		}
	    }
	    if (gmina > mina)
		gmina = mina;
	    if (mina < MINABHTHRESH) {
		fpout3 = fopen("BH.dat", "w");	/*  for output */
		fprintf(fpout3, "%f %g %g %g %d %g\n", timer, mina, gmina, x_c[minai], minai, 0.0);
		fclose(fpout);
		fclose(fpout2);
		fclose(fpout3);
		fclose(fpout4);
		printf("body: BH formed, quitting.\n");
#ifdef SDF
		printf("body: Closing SDF files\n");
		gft_close_all();	// close all sdf output files
#endif
		exit(0);
	    }
	    /* Write output to files and screen */

	    fprintf(fpout, "t= %f %g %g %g %g %d \n", timer, qq_buff[0], mina, gmina, x_c[minai], minai);
	    printf("t= %f Pi(0,t)= %g Min|A|= %g / %g x@mina= %g i= %d \n", timer, qq_buff[0], mina, gmina, x_c[minai], minai);

#ifdef SDF
	    if (ltrace)
		printf("body: Outputting sdf at t=%f rank=%d over (%f,%f) N=%d \n", timer, rank, bbox[0], bbox[1], gftShape[0]);
	    if (!(k % (int) DECOMPOSEFREQ * Fr)) {
		    if (ltrace) printf("body: Decomposing initial data \n");
		    decompose(diss_pp, diss_qq, diss_ph, qq, pp, metrica, delta, x_c, timer, N);
		    if (ltrace) printf("body: Done decomposing.        \n");
	       //
	       strcpy(fname, "Nj");
	       sprintf(gft_name, "%d%s", 0, fname);
	       rc = gft_out_bbox(gft_name, timer, gftShapej, rank, bboxj, diss_qq);
	       if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() Nj rc=%d\n", rc);
	       }
	       //
	       strcpy(fname, "Phasej");
	       sprintf(gft_name, "%d%s", 0, fname);
	       rc = gft_out_bbox(gft_name, timer, gftShapej, rank, bboxj, diss_pp);
	       if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() Phasej rc=%d\n", rc);
	       }
	       //
	       strcpy(fname, "Ej");
	       sprintf(gft_name, "%d%s", 0, fname);
	       rc = gft_out_bbox(gft_name, timer, gftShapej, rank, bboxj, diss_ph);
	       if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() Ej rc=%d\n", rc);
	       }
	    }
	    //
	    strcpy(fname, "qq");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, qq);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() QQ rc=%d\n", rc);
	    }
	    //
	    strcpy(fname, "pp");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, pp);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PP rc=%d\n", rc);
	    }
	    if (ltrace4) {
		// Just for debugging:
		strcpy(fname, "pqint");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, pqsourceint);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() pqsourceint rc=%d\n", rc);
		}
		strcpy(fname, "pqcop");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, pqsourcecop);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() pqsourcecop rc=%d\n", rc);
		}
		strcpy(fname, "qq_dx");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, qq_dx);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() PP_dx rc=%d\n", rc);
		}
		// Just for debugging:
		strcpy(fname, "pp_dx");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, pp_dx);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() PP_dx rc=%d\n", rc);
		}
		// Just for debugging:
		strcpy(fname, "aux_pi");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, aux_pi);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() aux_pi rc=%d\n", rc);
		}
		// Just for debugging:
		strcpy(fname, "auxpidx");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, aux_pi_dx);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() aux_pi_dx rc=%d\n", rc);
		}
		// Just for debugging:
		strcpy(fname, "dauxpi");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, diss_aux_pi);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() diss_aux_pi rc=%d\n", rc);
		}

		strcpy(fname, "ph");
		sprintf(gft_name, "%d%s", 0, fname);
		rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, ph);
		if (rc != 1) {
		    printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
		}
	    }

	    strcpy(fname, "delta");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, delta);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	    }
	    //
	    strcpy(fname, "metrica");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, metrica);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	    }
	    //
	    strcpy(fname, "dmdr");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, dmdr);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	    }
	    //
	    strcpy(fname, "hamc");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, hamc);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	    }
	    //
	    strcpy(fname, "momc");
	    sprintf(gft_name, "%d%s", 0, fname);
	    rc = gft_out_bbox(gft_name, timer, gftShape, rank, bbox, momc);
	    if (rc != 1) {
		printf("body: Error w/ gft_out_bbox() PH rc=%d\n", rc);
	    }
	    if (ltrace)
		printf("Done w/SDF output\n");
#endif
	}

    }				// END: for loop over k

    if (ltrace)
	printf("body: closing files, k=%d timer=%f\n", k, timer);

    // close files 
    fclose(fpout);
    fclose(fpout2);
    fclose(fpout4);

}
