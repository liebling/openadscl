#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

double interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4);
double ej(int q, double x, int deriv);

/*-------------------------------------------------------------------------
 *
 *  copy   
 *
 *-------------------------------------------------------------------------*/
void copy(double *to, const double *const from, const int n)
{

    int i;

    for (i = 0; i < n; i++) {
	to[i] = from[i];
    }

    return;

}

/*-------------------------------------------------------------------------
 *
 *  L2norm
 *
 *-------------------------------------------------------------------------*/
double L2Norm(const double *const f, const int n)
{

    int i;
    double sum = 0.0;

    for (i = 0; i < n; ++i) {
	sum += f[i] * f[i];
    }

    return sqrt(sum / n);

}

/*-------------------------------------------------------------------------
 *
 *  intfieldRev
 *
 *-------------------------------------------------------------------------*/
void intfieldRev(double *field, const double *const deriv, const double dx, int n)
{
    int i;
    field[n - 1] = 0.0;
    for (i = n - 2; i >= 0; i--) {
	field[i] = field[i + 1] - 0.5 * dx * (deriv[i] + deriv[i - 1]);
    }
}



/*-------------------------------------------------------------------------
 *
 *  calcdeltanoRB   --- no red black ordering
 *
 *-------------------------------------------------------------------------*/
void calcdeltanoRB(double *delta, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    // const double THRESHOLD=0.000000001;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50000;
    double dx, res, rhs, xh, cosxh, deltax, jacobian, phih, pih, deltadiff, tmp;
    double totalres;
    dx = x[1] - x[0];
    // coordinate time is proper time at outer boundary:
    delta[n - 1] = 0.0;

    deltadiff = 100000.0;
    numiter = 0;
    while (deltadiff > THRESHOLD && numiter < MAXITERS) {
	deltadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	// for (i = 1; i < n-1; i++) {
	for (i = n - 1; i > 0; i--) {
	    xh = 0.5 * (x[i - 1] + x[i]);
	    if (i > 1 && i < n - 2) {
		phih = interpcubic(phi[i - 1], phi[i], phi[i + 1], phi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i - 1], pi[i], pi[i + 1], pi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i < n - 3) {
		phih = interpcubic(phi[i], phi[i + 1], phi[i + 2], phi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		pih = interpcubic(pi[i], pi[i + 1], pi[i + 2], pi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
	    } else if (i > 2) {
		phih = interpcubic(phi[i - 2], phi[i - 1], phi[i], phi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		pih = interpcubic(pi[i - 2], pi[i - 1], pi[i], pi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
	    } else {
		phih = 0.5 * (phi[i] + phi[i - 1]);
		pih = 0.5 * (pi[i] + pi[i - 1]);
	    }
	    deltax = (delta[i] - delta[i - 1]) / dx;
	    cosxh = cos(xh);
	    rhs = -sin(xh) * pow(cosxh, 5) * (phih * phih / cosxh / cosxh + pih * pih);
	    res = deltax - rhs;
	    // jacobian: d (res) / d (delta[i])
	    jacobian = -1.0 / dx;
	    delta[i - 1] = delta[i - 1] - res / jacobian;
	    // delta[i]   = max(0.0,delta[i]);
	    deltadiff = deltadiff + pow(res / jacobian, 2);

	    // tmp = delta[i-1];
	    // delta[i-1]   = delta[i] - dx * rhs;
	    // deltadiff  = deltadiff + pow(tmp-delta[i-1],2);

	    totalres = totalres + res * res;
	}
	// Quadratic fit at origin so that delta is regular:
	// tmp = delta[0] - (4.0/3.0)*delta[1]+(1.0/3.0)*delta[2];
	// delta[0] = delta[0] - tmp;
	// deltadiff  = deltadiff + pow(tmp,2);
	//
	deltadiff = sqrt(deltadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	printf("calcdeltanoRB: %d ||delta_new-delta_old||_2 = %e ||res||=%e \n", numiter, deltadiff, totalres);
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcdelta
 *
 *-------------------------------------------------------------------------*/
void calcdelta(double *delta, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.000000001;
    const int MAXITERS = 50000;
    double dx, res, rhs, xh, cosxh, deltax, jacobian, phih, pih, deltadiff, tmp;
    dx = x[1] - x[0];
    // coordinate time is proper time at outer boundary:
    delta[n - 1] = 0.0;

    deltadiff = 100000.0;
    numiter = 0;
    while (deltadiff > THRESHOLD && numiter < MAXITERS) {
	deltadiff = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = 1; i < n - 1; i = i + 2) {
	    xh = 0.5 * (x[i + 1] + x[i]);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    deltax = (delta[i + 1] - delta[i]) / dx;
	    cosxh = cos(xh);
	    rhs = -sin(xh) * pow(cosxh, 5) * (phih * phih / cosxh / cosxh + pih * pih);
	    res = deltax - rhs;
	    // jacobian: d (res) / d (delta[i])
	    jacobian = -1.0 / dx;
	    delta[i] = delta[i] - res / jacobian;
	    deltadiff = deltadiff + pow(res / jacobian, 2);
	}
	for (i = 2; i < n - 1; i = i + 2) {
	    xh = 0.5 * (x[i + 1] + x[i]);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    deltax = (delta[i + 1] - delta[i]) / dx;
	    cosxh = cos(xh);
	    rhs = -sin(xh) * pow(cosxh, 5) * (phih * phih / cosxh / cosxh + pih * pih);
	    res = deltax - rhs;
	    // jacobian: d (res) / d (delta[i])
	    jacobian = -1.0 / dx;
	    delta[i] = delta[i] - res / jacobian;
	    deltadiff = deltadiff + pow(res / jacobian, 2);
	}
	// Quadratic fit at origin so that delta is regular:
	tmp = delta[0] - (4.0 / 3.0) * delta[1] + (1.0 / 3.0) * delta[2];
	delta[0] = delta[0] - tmp;
	deltadiff = deltadiff + pow(tmp, 2);
	//
	deltadiff = sqrt(deltadiff / (n - 1));
	printf("calcdelta: %d ||delta_new-delta_old||_2 = %e \n", numiter, deltadiff);
    }

}


/*-------------------------------------------------------------------------
 *
 *  calcmetricanoRBwide  ---straight-forward approach with no Red-Black ordering
 *                          and with wide stencil
 *
 *-------------------------------------------------------------------------*/
void calcmetricanoRBwide(double *metrica, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.000000001;
    const int MAXITERS = 50000;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while (metricadiff > THRESHOLD && numiter < MAXITERS) {
	metricadiff = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = 1; i < n - 1; i = i + 1) {
	    metricax = 0.5 * (metrica[i + 1] - metrica[i - 1]) / dx;
	    sinx = sin(x[i]);
	    cosx = cos(x[i]);
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phi[i] * phi[i] / cosx / cosx + pi[i] * pi[i]);
	    res = metricax + J * (1.0 - metrica[i]) + K * metrica[i] - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = -J + K;
	    metrica[i] = metrica[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	}
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	printf("calcmetrica: %d ||metrica_new-metrica_old||_2 = %e \n", numiter, metricadiff);
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcmetricanoRBbw ---no Red-Black ordering, but tight stencil...going backwards
 *
 *-------------------------------------------------------------------------*/
void calcmetricanoRBbw(double *metrica, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50000;
    const int MINITERS = 10;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K, xh, metricah, totalres;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while ((metricadiff > THRESHOLD || numiter < MINITERS) && numiter < MAXITERS) {
	// while (metricadiff > THRESHOLD && numiter < MAXITERS ) {
	//
	totalres = 0.0;
	metricadiff = 0.0;
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = n - 2; i > 0; i--) {
	    // for (i = 1; i < n-1; i++) {
	    // center on the half grid-point:
	    metricax = (metrica[i] - metrica[i - 1]) / dx;
	    metricah = 0.5 * (metrica[i] + metrica[i - 1]);
	    xh = 0.5 * (x[i] + x[i - 1]);
	    sinx = sin(xh);
	    cosx = cos(xh);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phih * phih / cosx / cosx + pih * pih);
	    res = metricax + J * (1.0 - metricah) + K * metricah - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = 1.0 / dx - 0.5 * J + 0.5 * K;
	    metrica[i] = metrica[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	    totalres = totalres + res * res;
	}
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	printf("calcmetricanoRBbw: %d ||metrica_new-metrica_old||_2 = %e |res|=%e\n", numiter, metricadiff, totalres);
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcmetricanoRBnip ---no Red-Black ordering, but tight stencil---not in place
 *
 *-------------------------------------------------------------------------*/
void calcmetricanoRBnip(double *metrica, const double *const phi, const double *const pi, double *work, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50000;
    const int MINITERS = 10;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K, xh, metricah, totalres;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while ((metricadiff > THRESHOLD || numiter < MINITERS) && numiter < MAXITERS) {
	// copy data over:
	for (i = 0; i < n; i++) {
	    work[i] = metrica[i];
	}
	metricadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = 1; i < n - 1; i++) {
	    // center on the half grid-point:
	    metricax = (work[i] - work[i - 1]) / dx;
	    metricah = 0.5 * (work[i] + work[i - 1]);
	    xh = 0.5 * (x[i] + x[i - 1]);
	    sinx = sin(xh);
	    cosx = cos(xh);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phih * phih / cosx / cosx + pih * pih);
	    res = metricax + J * (1.0 - metricah) + K * metricah - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = 1.0 / dx - 0.5 * J + 0.5 * K;
	    metrica[i] = work[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	    totalres = totalres + res * res;
	}
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	printf("calcmetricanoRB: %d ||metrica_new-metrica_old||_2 = %e |res|=%e\n", numiter, metricadiff, totalres);
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcmetricanoRB ---no Red-Black ordering, but tight stencil
 *
 *-------------------------------------------------------------------------*/
void calcmetricanoRB(double *metrica, double *dmdr, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50000;
    const int MINITERS = 10;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K, xh, metricah, totalres, source;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while ((metricadiff > THRESHOLD || numiter < MINITERS) && numiter < MAXITERS) {
	// while (metricadiff > THRESHOLD && numiter < MAXITERS ) {
	metricadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = 1; i < n - 1; i++) {
	    // center on the half grid-point:
	    metricax = (metrica[i] - metrica[i - 1]) / dx;
	    metricah = 0.5 * (metrica[i] + metrica[i - 1]);
	    xh = 0.5 * (x[i] + x[i - 1]);
	    sinx = sin(xh);
	    cosx = cos(xh);
	    // phih     = 0.5*(phi[i+1]+ phi[i]);
	    // pih      = 0.5*(pi[i+1] + pi[i]);
	    if (i > 1 && i < n - 2) {
		phih = interpcubic(phi[i - 1], phi[i], phi[i + 1], phi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i - 1], pi[i], pi[i + 1], pi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i < n - 3) {
		phih = interpcubic(phi[i], phi[i + 1], phi[i + 2], phi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		pih = interpcubic(pi[i], pi[i + 1], pi[i + 2], pi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
	    } else if (i > 2) {
		phih = interpcubic(phi[i - 2], phi[i - 1], phi[i], phi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		pih = interpcubic(pi[i - 2], pi[i - 1], pi[i], pi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
	    } else {
		phih = 0.5 * (phi[i] + phi[i - 1]);
		pih = 0.5 * (pi[i] + pi[i - 1]);
	    }
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phih * phih / cosx / cosx + pih * pih);
	    res = metricax + J * (1.0 - metricah) + K * metricah - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = 1.0 / dx - 0.5 * J + 0.5 * K;
	    metrica[i] = metrica[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	    totalres = totalres + res * res;
	}
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	printf("calcmetricanoRB: %d ||metrica_new-metrica_old||_2 = %e |res|=%e\n", numiter, metricadiff, totalres);
	// Quadratic fit to second point (method is iterative anyway):
	//metrica[1] = (3.0*metrica[0] + metrica[2])/4.0; 
    }

    // Compute dmdr:
    dmdr[0] = 0.0;
    for (i = 1; i < n - 1; i++) {
	source = pow(phi[i] / cos(x[i]), 2) + pow(pi[i], 2);
	dmdr[i] = pow(tan(x[i]) * cos(x[i]) * cos(x[i]), 2) * metrica[i] * source;
    }
}

/*-------------------------------------------------------------------------
 *
 *  calcmetrica
 *
 *-------------------------------------------------------------------------*/
void calcmetrica(double *metrica, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.000000001;
    const int MAXITERS = 50000;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K, xh, metricah;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while (metricadiff > THRESHOLD && numiter < MAXITERS) {
	metricadiff = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	for (i = 1; i < n - 1; i = i + 2) {
	    // center on the half grid-point:
	    metricax = (metrica[i] - metrica[i - 1]) / dx;
	    metricah = 0.5 * (metrica[i] + metrica[i - 1]);
	    xh = 0.5 * (x[i] + x[i - 1]);
	    sinx = sin(xh);
	    cosx = cos(xh);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phih * phih / cosx / cosx + pih * pih);
	    res = metricax + J * (1.0 - metricah) + K * metricah - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = 1.0 / dx - 0.5 * J + 0.5 * K;
	    metrica[i] = metrica[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	}
	for (i = 2; i < n - 1; i = i + 2) {
	    // center on the half grid-point:
	    metricax = (metrica[i] - metrica[i - 1]) / dx;
	    metricah = 0.5 * (metrica[i] + metrica[i - 1]);
	    xh = 0.5 * (x[i] + x[i - 1]);
	    sinx = sin(xh);
	    cosx = cos(xh);
	    phih = 0.5 * (phi[i + 1] + phi[i]);
	    pih = 0.5 * (pi[i + 1] + pi[i]);
	    // Put eq. 2.8 in form:  A_{,x} + J (1-A) + K A
	    rhs = 0.0;
	    J = -(1.0 + 2.0 * sinx * sinx) / sinx / cosx;
	    K = sinx * pow(cosx, 5) * (phih * phih / cosx / cosx + pih * pih);
	    res = metricax + J * (1.0 - metricah) + K * metricah - rhs;
	    // jacobian: d (res) / d (metrica[i])
	    jacobian = 1.0 / dx - 0.5 * J + 0.5 * K;
	    metrica[i] = metrica[i] - res / jacobian;
	    metrica[i] = min(1.0, metrica[i]);
	    metrica[i] = max(0.0, metrica[i]);
	    metricadiff = metricadiff + pow(res / jacobian, 2);
	}
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	printf("calcmetrica: %d ||metrica_new-metrica_old||_2 = %e \n", numiter, metricadiff);
    }

}

/*-------------------------------------------------------------------------
 *
 *  interpcubic
 *
 *-------------------------------------------------------------------------*/
double interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4)
{
    double interpcubic, xx1, xx2, xx3, xx4;
    xx1 = x - x1;
    xx2 = x - x2;
    xx3 = x - x3;
    xx4 = x - x4;
    interpcubic = xx2 * xx3 * xx4 * y1 / ((x1 - x2) * (x1 - x3) * (x1 - x4))
	+ xx1 * xx3 * xx4 * y2 / ((x2 - x1) * (x2 - x3) * (x2 - x4))
	+ xx1 * xx2 * xx4 * y3 / ((x3 - x1) * (x3 - x2) * (x3 - x4))
	+ xx1 * xx2 * xx3 * y4 / ((x4 - x1) * (x4 - x2) * (x4 - x3));

    return (interpcubic);
}

/*-------------------------------------------------------------------------
 *
 *  ej---derived/translated from massads code
 *      For AdS, provide the value of the mode at a particular point:
 *           q    ---(integer) number of the mode (0....)
 *           x    ---(double) coordinate where one wants the value
 *           deriv---(integer) if ==0, then just the mode value
 *                             if ==1, then the value of the derivative of the mode
 *      These array values represent the decomposition of the modes onto a
 *      cosign basis.
 *
 *-------------------------------------------------------------------------*/
double ej(int q, double x, int deriv)
{
    int i;
    const int maxq = 60;
    double a[maxq + 1][maxq + 2];
    double ej;

    a[0][0] = 2.39365368240860;
    a[0][1] = 0.797884560802865;
    a[1][0] = 2.30329432980890;
    a[1][1] = 2.30329432980890;
    a[1][2] = 0.921317731923561;
    a[2][0] = 2.28014505554696;
    a[2][1] = 2.28014505554696;
    a[2][2] = 2.28014505554696;
    a[2][3] = 0.977205023805840;
    a[3][0] = 2.27081926981814;
    a[3][1] = 2.27081926981814;
    a[3][2] = 2.27081926981814;
    a[3][3] = 2.27081926981814;
    a[3][4] = 1.00925300880806;
    a[4][0] = 2.26614198520271;
    a[4][1] = 2.26614198520271;
    a[4][2] = 2.26614198520271;
    a[4][3] = 2.26614198520271;
    a[4][4] = 2.26614198520271;
    a[4][5] = 1.03006453872851;
    a[5][0] = 2.26346491163354;
    a[5][1] = 2.26346491163354;
    a[5][2] = 2.26346491163354;
    a[5][3] = 2.26346491163354;
    a[5][4] = 2.26346491163354;
    a[5][5] = 2.26346491163354;
    a[5][6] = 1.04467611306164;
    a[6][0] = 2.26179013159540;
    a[6][1] = 2.26179013159540;
    a[6][2] = 2.26179013159540;
    a[6][3] = 2.26179013159540;
    a[6][4] = 2.26179013159540;
    a[6][5] = 2.26179013159540;
    a[6][6] = 2.26179013159540;
    a[6][7] = 1.05550206141119;
    a[7][0] = 2.26067292227479;
    a[7][1] = 2.26067292227479;
    a[7][2] = 2.26067292227479;
    a[7][3] = 2.26067292227479;
    a[7][4] = 2.26067292227479;
    a[7][5] = 2.26067292227479;
    a[7][6] = 2.26067292227479;
    a[7][7] = 2.26067292227479;
    a[7][8] = 1.06384608107049;
    a[8][0] = 2.25989054712684;
    a[8][1] = 2.25989054712684;
    a[8][2] = 2.25989054712684;
    a[8][3] = 2.25989054712684;
    a[8][4] = 2.25989054712684;
    a[8][5] = 2.25989054712684;
    a[8][6] = 2.25989054712684;
    a[8][7] = 2.25989054712684;
    a[8][8] = 2.25989054712684;
    a[8][9] = 1.07047446969166;
    a[9][0] = 2.25932137685031;
    a[9][1] = 2.25932137685031;
    a[9][2] = 2.25932137685031;
    a[9][3] = 2.25932137685031;
    a[9][4] = 2.25932137685031;
    a[9][5] = 2.25932137685031;
    a[9][6] = 2.25932137685031;
    a[9][7] = 2.25932137685031;
    a[9][8] = 2.25932137685031;
    a[9][9] = 2.25932137685031;
    a[9][10] = 1.07586732230967;
    a[10][0] = 2.25889440502814;
    a[10][1] = 2.25889440502814;
    a[10][2] = 2.25889440502814;
    a[10][3] = 2.25889440502814;
    a[10][4] = 2.25889440502814;
    a[10][5] = 2.25889440502814;
    a[10][6] = 2.25889440502814;
    a[10][7] = 2.25889440502814;
    a[10][8] = 2.25889440502814;
    a[10][9] = 2.25889440502814;
    a[10][10] = 2.25889440502814;
    a[10][11] = 1.08034080240476;
    a[11][0] = 2.25856591023908;
    a[11][1] = 2.25856591023908;
    a[11][2] = 2.25856591023908;
    a[11][3] = 2.25856591023908;
    a[11][4] = 2.25856591023908;
    a[11][5] = 2.25856591023908;
    a[11][6] = 2.25856591023908;
    a[11][7] = 2.25856591023908;
    a[11][8] = 2.25856591023908;
    a[11][9] = 2.25856591023908;
    a[11][10] = 2.25856591023908;
    a[11][11] = 2.25856591023908;
    a[11][12] = 1.08411163691476;
    a[12][0] = 2.25830777366926;
    a[12][1] = 2.25830777366926;
    a[12][2] = 2.25830777366926;
    a[12][3] = 2.25830777366926;
    a[12][4] = 2.25830777366926;
    a[12][5] = 2.25830777366926;
    a[12][6] = 2.25830777366926;
    a[12][7] = 2.25830777366926;
    a[12][8] = 2.25830777366926;
    a[12][9] = 2.25830777366926;
    a[12][10] = 2.25830777366926;
    a[12][11] = 2.25830777366926;
    a[12][12] = 2.25830777366926;
    a[12][13] = 1.08733337250742;
    a[13][0] = 2.25810124316687;
    a[13][1] = 2.25810124316687;
    a[13][2] = 2.25810124316687;
    a[13][3] = 2.25810124316687;
    a[13][4] = 2.25810124316687;
    a[13][5] = 2.25810124316687;
    a[13][6] = 2.25810124316687;
    a[13][7] = 2.25810124316687;
    a[13][8] = 2.25810124316687;
    a[13][9] = 2.25810124316687;
    a[13][10] = 2.25810124316687;
    a[13][11] = 2.25810124316687;
    a[13][12] = 2.25810124316687;
    a[13][13] = 2.25810124316687;
    a[13][14] = 1.09011784152883;
    a[14][0] = 2.25793342322363;
    a[14][1] = 2.25793342322363;
    a[14][2] = 2.25793342322363;
    a[14][3] = 2.25793342322363;
    a[14][4] = 2.25793342322363;
    a[14][5] = 2.25793342322363;
    a[14][6] = 2.25793342322363;
    a[14][7] = 2.25793342322363;
    a[14][8] = 2.25793342322363;
    a[14][9] = 2.25793342322363;
    a[14][10] = 2.25793342322363;
    a[14][11] = 2.25793342322363;
    a[14][12] = 2.25793342322363;
    a[14][13] = 2.25793342322363;
    a[14][14] = 2.25793342322363;
    a[14][15] = 1.09254843059208;
    a[15][0] = 2.25779520919827;
    a[15][1] = 2.25779520919827;
    a[15][2] = 2.25779520919827;
    a[15][3] = 2.25779520919827;
    a[15][4] = 2.25779520919827;
    a[15][5] = 2.25779520919827;
    a[15][6] = 2.25779520919827;
    a[15][7] = 2.25779520919827;
    a[15][8] = 2.25779520919827;
    a[15][9] = 2.25779520919827;
    a[15][10] = 2.25779520919827;
    a[15][11] = 2.25779520919827;
    a[15][12] = 2.25779520919827;
    a[15][13] = 2.25779520919827;
    a[15][14] = 2.25779520919827;
    a[15][15] = 2.25779520919827;
    a[15][16] = 1.09468858627795;
    a[16][0] = 2.25768002438023;
    a[16][1] = 2.25768002438023;
    a[16][2] = 2.25768002438023;
    a[16][3] = 2.25768002438023;
    a[16][4] = 2.25768002438023;
    a[16][5] = 2.25768002438023;
    a[16][6] = 2.25768002438023;
    a[16][7] = 2.25768002438023;
    a[16][8] = 2.25768002438023;
    a[16][9] = 2.25768002438023;
    a[16][10] = 2.25768002438023;
    a[16][11] = 2.25768002438023;
    a[16][12] = 2.25768002438023;
    a[16][13] = 2.25768002438023;
    a[16][14] = 2.25768002438023;
    a[16][15] = 2.25768002438023;
    a[16][16] = 2.25768002438023;
    a[16][17] = 1.09658744041326;
    a[17][0] = 2.25758302208049;
    a[17][1] = 2.25758302208049;
    a[17][2] = 2.25758302208049;
    a[17][3] = 2.25758302208049;
    a[17][4] = 2.25758302208049;
    a[17][5] = 2.25758302208049;
    a[17][6] = 2.25758302208049;
    a[17][7] = 2.25758302208049;
    a[17][8] = 2.25758302208049;
    a[17][9] = 2.25758302208049;
    a[17][10] = 2.25758302208049;
    a[17][11] = 2.25758302208049;
    a[17][12] = 2.25758302208049;
    a[17][13] = 2.25758302208049;
    a[17][14] = 2.25758302208049;
    a[17][15] = 2.25758302208049;
    a[17][16] = 2.25758302208049;
    a[17][17] = 2.25758302208049;
    a[17][18] = 1.09828363236348;
    a[18][0] = 2.25750056684855;
    a[18][1] = 2.25750056684855;
    a[18][2] = 2.25750056684855;
    a[18][3] = 2.25750056684855;
    a[18][4] = 2.25750056684855;
    a[18][5] = 2.25750056684855;
    a[18][6] = 2.25750056684855;
    a[18][7] = 2.25750056684855;
    a[18][8] = 2.25750056684855;
    a[18][9] = 2.25750056684855;
    a[18][10] = 2.25750056684855;
    a[18][11] = 2.25750056684855;
    a[18][12] = 2.25750056684855;
    a[18][13] = 2.25750056684855;
    a[18][14] = 2.25750056684855;
    a[18][15] = 2.25750056684855;
    a[18][16] = 2.25750056684855;
    a[18][17] = 2.25750056684855;
    a[18][18] = 2.25750056684855;
    a[18][19] = 1.09980796846468;
    a[19][0] = 2.25742988853834;
    a[19][1] = 2.25742988853834;
    a[19][2] = 2.25742988853834;
    a[19][3] = 2.25742988853834;
    a[19][4] = 2.25742988853834;
    a[19][5] = 2.25742988853834;
    a[19][6] = 2.25742988853834;
    a[19][7] = 2.25742988853834;
    a[19][8] = 2.25742988853834;
    a[19][9] = 2.25742988853834;
    a[19][10] = 2.25742988853834;
    a[19][11] = 2.25742988853834;
    a[19][12] = 2.25742988853834;
    a[19][13] = 2.25742988853834;
    a[19][14] = 2.25742988853834;
    a[19][15] = 2.25742988853834;
    a[19][16] = 2.25742988853834;
    a[19][17] = 2.25742988853834;
    a[19][18] = 2.25742988853834;
    a[19][19] = 2.25742988853834;
    a[19][20] = 1.10118531148212;
    a[20][0] = 2.25736884639865;
    a[20][1] = 2.25736884639865;
    a[20][2] = 2.25736884639865;
    a[20][3] = 2.25736884639865;
    a[20][4] = 2.25736884639865;
    a[20][5] = 2.25736884639865;
    a[20][6] = 2.25736884639865;
    a[20][7] = 2.25736884639865;
    a[20][8] = 2.25736884639865;
    a[20][9] = 2.25736884639865;
    a[20][10] = 2.25736884639865;
    a[20][11] = 2.25736884639865;
    a[20][12] = 2.25736884639865;
    a[20][13] = 2.25736884639865;
    a[20][14] = 2.25736884639865;
    a[20][15] = 2.25736884639865;
    a[20][16] = 2.25736884639865;
    a[20][17] = 2.25736884639865;
    a[20][18] = 2.25736884639865;
    a[20][19] = 2.25736884639865;
    a[20][20] = 2.25736884639865;
    a[20][21] = 1.10243594824120;
    a[21][0] = 2.25731576493540;
    a[21][1] = 2.25731576493540;
    a[21][2] = 2.25731576493540;
    a[21][3] = 2.25731576493540;
    a[21][4] = 2.25731576493540;
    a[21][5] = 2.25731576493540;
    a[21][6] = 2.25731576493540;
    a[21][7] = 2.25731576493540;
    a[21][8] = 2.25731576493540;
    a[21][9] = 2.25731576493540;
    a[21][10] = 2.25731576493540;
    a[21][11] = 2.25731576493540;
    a[21][12] = 2.25731576493540;
    a[21][13] = 2.25731576493540;
    a[21][14] = 2.25731576493540;
    a[21][15] = 2.25731576493540;
    a[21][16] = 2.25731576493540;
    a[21][17] = 2.25731576493540;
    a[21][18] = 2.25731576493540;
    a[21][19] = 2.25731576493540;
    a[21][20] = 2.25731576493540;
    a[21][21] = 2.25731576493540;
    a[21][22] = 1.10357659619064;
    a[22][0] = 2.25726931763110;
    a[22][1] = 2.25726931763110;
    a[22][2] = 2.25726931763110;
    a[22][3] = 2.25726931763110;
    a[22][4] = 2.25726931763110;
    a[22][5] = 2.25726931763110;
    a[22][6] = 2.25726931763110;
    a[22][7] = 2.25726931763110;
    a[22][8] = 2.25726931763110;
    a[22][9] = 2.25726931763110;
    a[22][10] = 2.25726931763110;
    a[22][11] = 2.25726931763110;
    a[22][12] = 2.25726931763110;
    a[22][13] = 2.25726931763110;
    a[22][14] = 2.25726931763110;
    a[22][15] = 2.25726931763110;
    a[22][16] = 2.25726931763110;
    a[22][17] = 2.25726931763110;
    a[22][18] = 2.25726931763110;
    a[22][19] = 2.25726931763110;
    a[22][20] = 2.25726931763110;
    a[22][21] = 2.25726931763110;
    a[22][22] = 2.25726931763110;
    a[22][23] = 1.10462115543650;
    a[23][0] = 2.25722844321273;
    a[23][1] = 2.25722844321273;
    a[23][2] = 2.25722844321273;
    a[23][3] = 2.25722844321273;
    a[23][4] = 2.25722844321273;
    a[23][5] = 2.25722844321273;
    a[23][6] = 2.25722844321273;
    a[23][7] = 2.25722844321273;
    a[23][8] = 2.25722844321273;
    a[23][9] = 2.25722844321273;
    a[23][10] = 2.25722844321273;
    a[23][11] = 2.25722844321273;
    a[23][12] = 2.25722844321273;
    a[23][13] = 2.25722844321273;
    a[23][14] = 2.25722844321273;
    a[23][15] = 2.25722844321273;
    a[23][16] = 2.25722844321273;
    a[23][17] = 2.25722844321273;
    a[23][18] = 2.25722844321273;
    a[23][19] = 2.25722844321273;
    a[23][20] = 2.25722844321273;
    a[23][21] = 2.25722844321273;
    a[23][22] = 2.25722844321273;
    a[23][23] = 2.25722844321273;
    a[23][24] = 1.10558127830827;
    a[24][0] = 2.25719228445639;
    a[24][1] = 2.25719228445639;
    a[24][2] = 2.25719228445639;
    a[24][3] = 2.25719228445639;
    a[24][4] = 2.25719228445639;
    a[24][5] = 2.25719228445639;
    a[24][6] = 2.25719228445639;
    a[24][7] = 2.25719228445639;
    a[24][8] = 2.25719228445639;
    a[24][9] = 2.25719228445639;
    a[24][10] = 2.25719228445639;
    a[24][11] = 2.25719228445639;
    a[24][12] = 2.25719228445639;
    a[24][13] = 2.25719228445639;
    a[24][14] = 2.25719228445639;
    a[24][15] = 2.25719228445639;
    a[24][16] = 2.25719228445639;
    a[24][17] = 2.25719228445639;
    a[24][18] = 2.25719228445639;
    a[24][19] = 2.25719228445639;
    a[24][20] = 2.25719228445639;
    a[24][21] = 2.25719228445639;
    a[24][22] = 2.25719228445639;
    a[24][23] = 2.25719228445639;
    a[24][24] = 2.25719228445639;
    a[24][25] = 1.10646680610608;
    a[25][0] = 2.25716014285337;
    a[25][1] = 2.25716014285337;
    a[25][2] = 2.25716014285337;
    a[25][3] = 2.25716014285337;
    a[25][4] = 2.25716014285337;
    a[25][5] = 2.25716014285337;
    a[25][6] = 2.25716014285337;
    a[25][7] = 2.25716014285337;
    a[25][8] = 2.25716014285337;
    a[25][9] = 2.25716014285337;
    a[25][10] = 2.25716014285337;
    a[25][11] = 2.25716014285337;
    a[25][12] = 2.25716014285337;
    a[25][13] = 2.25716014285337;
    a[25][14] = 2.25716014285337;
    a[25][15] = 2.25716014285337;
    a[25][16] = 2.25716014285337;
    a[25][17] = 2.25716014285337;
    a[25][18] = 2.25716014285337;
    a[25][19] = 2.25716014285337;
    a[25][20] = 2.25716014285337;
    a[25][21] = 2.25716014285337;
    a[25][22] = 2.25716014285337;
    a[25][23] = 2.25716014285337;
    a[25][24] = 2.25716014285337;
    a[25][25] = 2.25716014285337;
    a[25][26] = 1.10728610781486;
    a[26][0] = 2.25713144460677;
    a[26][1] = 2.25713144460677;
    a[26][2] = 2.25713144460677;
    a[26][3] = 2.25713144460677;
    a[26][4] = 2.25713144460677;
    a[26][5] = 2.25713144460677;
    a[26][6] = 2.25713144460677;
    a[26][7] = 2.25713144460677;
    a[26][8] = 2.25713144460677;
    a[26][9] = 2.25713144460677;
    a[26][10] = 2.25713144460677;
    a[26][11] = 2.25713144460677;
    a[26][12] = 2.25713144460677;
    a[26][13] = 2.25713144460677;
    a[26][14] = 2.25713144460677;
    a[26][15] = 2.25713144460677;
    a[26][16] = 2.25713144460677;
    a[26][17] = 2.25713144460677;
    a[26][18] = 2.25713144460677;
    a[26][19] = 2.25713144460677;
    a[26][20] = 2.25713144460677;
    a[26][21] = 2.25713144460677;
    a[26][22] = 2.25713144460677;
    a[26][23] = 2.25713144460677;
    a[26][24] = 2.25713144460677;
    a[26][25] = 2.25713144460677;
    a[26][26] = 2.25713144460677;
    a[26][27] = 1.10804634553423;
    a[27][0] = 2.25710571483407;
    a[27][1] = 2.25710571483407;
    a[27][2] = 2.25710571483407;
    a[27][3] = 2.25710571483407;
    a[27][4] = 2.25710571483407;
    a[27][5] = 2.25710571483407;
    a[27][6] = 2.25710571483407;
    a[27][7] = 2.25710571483407;
    a[27][8] = 2.25710571483407;
    a[27][9] = 2.25710571483407;
    a[27][10] = 2.25710571483407;
    a[27][11] = 2.25710571483407;
    a[27][12] = 2.25710571483407;
    a[27][13] = 2.25710571483407;
    a[27][14] = 2.25710571483407;
    a[27][15] = 2.25710571483407;
    a[27][16] = 2.25710571483407;
    a[27][17] = 2.25710571483407;
    a[27][18] = 2.25710571483407;
    a[27][19] = 2.25710571483407;
    a[27][20] = 2.25710571483407;
    a[27][21] = 2.25710571483407;
    a[27][22] = 2.25710571483407;
    a[27][23] = 2.25710571483407;
    a[27][24] = 2.25710571483407;
    a[27][25] = 2.25710571483407;
    a[27][26] = 2.25710571483407;
    a[27][27] = 2.25710571483407;
    a[27][28] = 1.10875368447990;
    a[28][0] = 2.25708255778787;
    a[28][1] = 2.25708255778787;
    a[28][2] = 2.25708255778787;
    a[28][3] = 2.25708255778787;
    a[28][4] = 2.25708255778787;
    a[28][5] = 2.25708255778787;
    a[28][6] = 2.25708255778787;
    a[28][7] = 2.25708255778787;
    a[28][8] = 2.25708255778787;
    a[28][9] = 2.25708255778787;
    a[28][10] = 2.25708255778787;
    a[28][11] = 2.25708255778787;
    a[28][12] = 2.25708255778787;
    a[28][13] = 2.25708255778787;
    a[28][14] = 2.25708255778787;
    a[28][15] = 2.25708255778787;
    a[28][16] = 2.25708255778787;
    a[28][17] = 2.25708255778787;
    a[28][18] = 2.25708255778787;
    a[28][19] = 2.25708255778787;
    a[28][20] = 2.25708255778787;
    a[28][21] = 2.25708255778787;
    a[28][22] = 2.25708255778787;
    a[28][23] = 2.25708255778787;
    a[28][24] = 2.25708255778787;
    a[28][25] = 2.25708255778787;
    a[28][26] = 2.25708255778787;
    a[28][27] = 2.25708255778787;
    a[28][28] = 2.25708255778787;
    a[28][29] = 1.10941346060760;
    a[29][0] = 2.25706164154193;
    a[29][1] = 2.25706164154193;
    a[29][2] = 2.25706164154193;
    a[29][3] = 2.25706164154193;
    a[29][4] = 2.25706164154193;
    a[29][5] = 2.25706164154193;
    a[29][6] = 2.25706164154193;
    a[29][7] = 2.25706164154193;
    a[29][8] = 2.25706164154193;
    a[29][9] = 2.25706164154193;
    a[29][10] = 2.25706164154193;
    a[29][11] = 2.25706164154193;
    a[29][12] = 2.25706164154193;
    a[29][13] = 2.25706164154193;
    a[29][14] = 2.25706164154193;
    a[29][15] = 2.25706164154193;
    a[29][16] = 2.25706164154193;
    a[29][17] = 2.25706164154193;
    a[29][18] = 2.25706164154193;
    a[29][19] = 2.25706164154193;
    a[29][20] = 2.25706164154193;
    a[29][21] = 2.25706164154193;
    a[29][22] = 2.25706164154193;
    a[29][23] = 2.25706164154193;
    a[29][24] = 2.25706164154193;
    a[29][25] = 2.25706164154193;
    a[29][26] = 2.25706164154193;
    a[29][27] = 2.25706164154193;
    a[29][28] = 2.25706164154193;
    a[29][29] = 2.25706164154193;
    a[29][30] = 1.11003031551242;
    a[30][0] = 2.25704268602661;
    a[30][1] = 2.25704268602661;
    a[30][2] = 2.25704268602661;
    a[30][3] = 2.25704268602661;
    a[30][4] = 2.25704268602661;
    a[30][5] = 2.25704268602661;
    a[30][6] = 2.25704268602661;
    a[30][7] = 2.25704268602661;
    a[30][8] = 2.25704268602661;
    a[30][9] = 2.25704268602661;
    a[30][10] = 2.25704268602661;
    a[30][11] = 2.25704268602661;
    a[30][12] = 2.25704268602661;
    a[30][13] = 2.25704268602661;
    a[30][14] = 2.25704268602661;
    a[30][15] = 2.25704268602661;
    a[30][16] = 2.25704268602661;
    a[30][17] = 2.25704268602661;
    a[30][18] = 2.25704268602661;
    a[30][19] = 2.25704268602661;
    a[30][20] = 2.25704268602661;
    a[30][21] = 2.25704268602661;
    a[30][22] = 2.25704268602661;
    a[30][23] = 2.25704268602661;
    a[30][24] = 2.25704268602661;
    a[30][25] = 2.25704268602661;
    a[30][26] = 2.25704268602661;
    a[30][27] = 2.25704268602661;
    a[30][28] = 2.25704268602661;
    a[30][29] = 2.25704268602661;
    a[30][30] = 2.25704268602661;
    a[30][31] = 1.11060830582262;
    a[31][0] = 2.25702545360182;
    a[31][1] = 2.25702545360182;
    a[31][2] = 2.25702545360182;
    a[31][3] = 2.25702545360182;
    a[31][4] = 2.25702545360182;
    a[31][5] = 2.25702545360182;
    a[31][6] = 2.25702545360182;
    a[31][7] = 2.25702545360182;
    a[31][8] = 2.25702545360182;
    a[31][9] = 2.25702545360182;
    a[31][10] = 2.25702545360182;
    a[31][11] = 2.25702545360182;
    a[31][12] = 2.25702545360182;
    a[31][13] = 2.25702545360182;
    a[31][14] = 2.25702545360182;
    a[31][15] = 2.25702545360182;
    a[31][16] = 2.25702545360182;
    a[31][17] = 2.25702545360182;
    a[31][18] = 2.25702545360182;
    a[31][19] = 2.25702545360182;
    a[31][20] = 2.25702545360182;
    a[31][21] = 2.25702545360182;
    a[31][22] = 2.25702545360182;
    a[31][23] = 2.25702545360182;
    a[31][24] = 2.25702545360182;
    a[31][25] = 2.25702545360182;
    a[31][26] = 2.25702545360182;
    a[31][27] = 2.25702545360182;
    a[31][28] = 2.25702545360182;
    a[31][29] = 2.25702545360182;
    a[31][30] = 2.25702545360182;
    a[31][31] = 2.25702545360182;
    a[31][32] = 1.11115099254243;
    a[32][0] = 2.25700974157043;
    a[32][1] = 2.25700974157043;
    a[32][2] = 2.25700974157043;
    a[32][3] = 2.25700974157043;
    a[32][4] = 2.25700974157043;
    a[32][5] = 2.25700974157043;
    a[32][6] = 2.25700974157043;
    a[32][7] = 2.25700974157043;
    a[32][8] = 2.25700974157043;
    a[32][9] = 2.25700974157043;
    a[32][10] = 2.25700974157043;
    a[32][11] = 2.25700974157043;
    a[32][12] = 2.25700974157043;
    a[32][13] = 2.25700974157043;
    a[32][14] = 2.25700974157043;
    a[32][15] = 2.25700974157043;
    a[32][16] = 2.25700974157043;
    a[32][17] = 2.25700974157043;
    a[32][18] = 2.25700974157043;
    a[32][19] = 2.25700974157043;
    a[32][20] = 2.25700974157043;
    a[32][21] = 2.25700974157043;
    a[32][22] = 2.25700974157043;
    a[32][23] = 2.25700974157043;
    a[32][24] = 2.25700974157043;
    a[32][25] = 2.25700974157043;
    a[32][26] = 2.25700974157043;
    a[32][27] = 2.25700974157043;
    a[32][28] = 2.25700974157043;
    a[32][29] = 2.25700974157043;
    a[32][30] = 2.25700974157043;
    a[32][31] = 2.25700974157043;
    a[32][32] = 2.25700974157043;
    a[32][33] = 1.11166151450484;
    a[33][0] = 2.25699537618886;
    a[33][1] = 2.25699537618886;
    a[33][2] = 2.25699537618886;
    a[33][3] = 2.25699537618886;
    a[33][4] = 2.25699537618886;
    a[33][5] = 2.25699537618886;
    a[33][6] = 2.25699537618886;
    a[33][7] = 2.25699537618886;
    a[33][8] = 2.25699537618886;
    a[33][9] = 2.25699537618886;
    a[33][10] = 2.25699537618886;
    a[33][11] = 2.25699537618886;
    a[33][12] = 2.25699537618886;
    a[33][13] = 2.25699537618886;
    a[33][14] = 2.25699537618886;
    a[33][15] = 2.25699537618886;
    a[33][16] = 2.25699537618886;
    a[33][17] = 2.25699537618886;
    a[33][18] = 2.25699537618886;
    a[33][19] = 2.25699537618886;
    a[33][20] = 2.25699537618886;
    a[33][21] = 2.25699537618886;
    a[33][22] = 2.25699537618886;
    a[33][23] = 2.25699537618886;
    a[33][24] = 2.25699537618886;
    a[33][25] = 2.25699537618886;
    a[33][26] = 2.25699537618886;
    a[33][27] = 2.25699537618886;
    a[33][28] = 2.25699537618886;
    a[33][29] = 2.25699537618886;
    a[33][30] = 2.25699537618886;
    a[33][31] = 2.25699537618886;
    a[33][32] = 2.25699537618886;
    a[33][33] = 2.25699537618886;
    a[33][34] = 1.11214264913654;
    a[34][0] = 2.25698220784211;
    a[34][1] = 2.25698220784211;
    a[34][2] = 2.25698220784211;
    a[34][3] = 2.25698220784211;
    a[34][4] = 2.25698220784211;
    a[34][5] = 2.25698220784211;
    a[34][6] = 2.25698220784211;
    a[34][7] = 2.25698220784211;
    a[34][8] = 2.25698220784211;
    a[34][9] = 2.25698220784211;
    a[34][10] = 2.25698220784211;
    a[34][11] = 2.25698220784211;
    a[34][12] = 2.25698220784211;
    a[34][13] = 2.25698220784211;
    a[34][14] = 2.25698220784211;
    a[34][15] = 2.25698220784211;
    a[34][16] = 2.25698220784211;
    a[34][17] = 2.25698220784211;
    a[34][18] = 2.25698220784211;
    a[34][19] = 2.25698220784211;
    a[34][20] = 2.25698220784211;
    a[34][21] = 2.25698220784211;
    a[34][22] = 2.25698220784211;
    a[34][23] = 2.25698220784211;
    a[34][24] = 2.25698220784211;
    a[34][25] = 2.25698220784211;
    a[34][26] = 2.25698220784211;
    a[34][27] = 2.25698220784211;
    a[34][28] = 2.25698220784211;
    a[34][29] = 2.25698220784211;
    a[34][30] = 2.25698220784211;
    a[34][31] = 2.25698220784211;
    a[34][32] = 2.25698220784211;
    a[34][33] = 2.25698220784211;
    a[34][34] = 2.25698220784211;
    a[34][35] = 1.11259686302076;
    a[35][0] = 2.25697010713141;
    a[35][1] = 2.25697010713141;
    a[35][2] = 2.25697010713141;
    a[35][3] = 2.25697010713141;
    a[35][4] = 2.25697010713141;
    a[35][5] = 2.25697010713141;
    a[35][6] = 2.25697010713141;
    a[35][7] = 2.25697010713141;
    a[35][8] = 2.25697010713141;
    a[35][9] = 2.25697010713141;
    a[35][10] = 2.25697010713141;
    a[35][11] = 2.25697010713141;
    a[35][12] = 2.25697010713141;
    a[35][13] = 2.25697010713141;
    a[35][14] = 2.25697010713141;
    a[35][15] = 2.25697010713141;
    a[35][16] = 2.25697010713141;
    a[35][17] = 2.25697010713141;
    a[35][18] = 2.25697010713141;
    a[35][19] = 2.25697010713141;
    a[35][20] = 2.25697010713141;
    a[35][21] = 2.25697010713141;
    a[35][22] = 2.25697010713141;
    a[35][23] = 2.25697010713141;
    a[35][24] = 2.25697010713141;
    a[35][25] = 2.25697010713141;
    a[35][26] = 2.25697010713141;
    a[35][27] = 2.25697010713141;
    a[35][28] = 2.25697010713141;
    a[35][29] = 2.25697010713141;
    a[35][30] = 2.25697010713141;
    a[35][31] = 2.25697010713141;
    a[35][32] = 2.25697010713141;
    a[35][33] = 2.25697010713141;
    a[35][34] = 2.25697010713141;
    a[35][35] = 2.25697010713141;
    a[35][36] = 1.11302635420179;
    a[36][0] = 2.25695896168257;
    a[36][1] = 2.25695896168257;
    a[36][2] = 2.25695896168257;
    a[36][3] = 2.25695896168257;
    a[36][4] = 2.25695896168257;
    a[36][5] = 2.25695896168257;
    a[36][6] = 2.25695896168257;
    a[36][7] = 2.25695896168257;
    a[36][8] = 2.25695896168257;
    a[36][9] = 2.25695896168257;
    a[36][10] = 2.25695896168257;
    a[36][11] = 2.25695896168257;
    a[36][12] = 2.25695896168257;
    a[36][13] = 2.25695896168257;
    a[36][14] = 2.25695896168257;
    a[36][15] = 2.25695896168257;
    a[36][16] = 2.25695896168257;
    a[36][17] = 2.25695896168257;
    a[36][18] = 2.25695896168257;
    a[36][19] = 2.25695896168257;
    a[36][20] = 2.25695896168257;
    a[36][21] = 2.25695896168257;
    a[36][22] = 2.25695896168257;
    a[36][23] = 2.25695896168257;
    a[36][24] = 2.25695896168257;
    a[36][25] = 2.25695896168257;
    a[36][26] = 2.25695896168257;
    a[36][27] = 2.25695896168257;
    a[36][28] = 2.25695896168257;
    a[36][29] = 2.25695896168257;
    a[36][30] = 2.25695896168257;
    a[36][31] = 2.25695896168257;
    a[36][32] = 2.25695896168257;
    a[36][33] = 2.25695896168257;
    a[36][34] = 2.25695896168257;
    a[36][35] = 2.25695896168257;
    a[36][36] = 2.25695896168257;
    a[36][37] = 1.11343308776340;
    a[37][0] = 2.25694867352710;
    a[37][1] = 2.25694867352710;
    a[37][2] = 2.25694867352710;
    a[37][3] = 2.25694867352710;
    a[37][4] = 2.25694867352710;
    a[37][5] = 2.25694867352710;
    a[37][6] = 2.25694867352710;
    a[37][7] = 2.25694867352710;
    a[37][8] = 2.25694867352710;
    a[37][9] = 2.25694867352710;
    a[37][10] = 2.25694867352710;
    a[37][11] = 2.25694867352710;
    a[37][12] = 2.25694867352710;
    a[37][13] = 2.25694867352710;
    a[37][14] = 2.25694867352710;
    a[37][15] = 2.25694867352710;
    a[37][16] = 2.25694867352710;
    a[37][17] = 2.25694867352710;
    a[37][18] = 2.25694867352710;
    a[37][19] = 2.25694867352710;
    a[37][20] = 2.25694867352710;
    a[37][21] = 2.25694867352710;
    a[37][22] = 2.25694867352710;
    a[37][23] = 2.25694867352710;
    a[37][24] = 2.25694867352710;
    a[37][25] = 2.25694867352710;
    a[37][26] = 2.25694867352710;
    a[37][27] = 2.25694867352710;
    a[37][28] = 2.25694867352710;
    a[37][29] = 2.25694867352710;
    a[37][30] = 2.25694867352710;
    a[37][31] = 2.25694867352710;
    a[37][32] = 2.25694867352710;
    a[37][33] = 2.25694867352710;
    a[37][34] = 2.25694867352710;
    a[37][35] = 2.25694867352710;
    a[37][36] = 2.25694867352710;
    a[37][37] = 2.25694867352710;
    a[37][38] = 1.11381882589649;
    a[38][0] = 2.25693915694154;
    a[38][1] = 2.25693915694154;
    a[38][2] = 2.25693915694154;
    a[38][3] = 2.25693915694154;
    a[38][4] = 2.25693915694154;
    a[38][5] = 2.25693915694154;
    a[38][6] = 2.25693915694154;
    a[38][7] = 2.25693915694154;
    a[38][8] = 2.25693915694154;
    a[38][9] = 2.25693915694154;
    a[38][10] = 2.25693915694154;
    a[38][11] = 2.25693915694154;
    a[38][12] = 2.25693915694154;
    a[38][13] = 2.25693915694154;
    a[38][14] = 2.25693915694154;
    a[38][15] = 2.25693915694154;
    a[38][16] = 2.25693915694154;
    a[38][17] = 2.25693915694154;
    a[38][18] = 2.25693915694154;
    a[38][19] = 2.25693915694154;
    a[38][20] = 2.25693915694154;
    a[38][21] = 2.25693915694154;
    a[38][22] = 2.25693915694154;
    a[38][23] = 2.25693915694154;
    a[38][24] = 2.25693915694154;
    a[38][25] = 2.25693915694154;
    a[38][26] = 2.25693915694154;
    a[38][27] = 2.25693915694154;
    a[38][28] = 2.25693915694154;
    a[38][29] = 2.25693915694154;
    a[38][30] = 2.25693915694154;
    a[38][31] = 2.25693915694154;
    a[38][32] = 2.25693915694154;
    a[38][33] = 2.25693915694154;
    a[38][34] = 2.25693915694154;
    a[38][35] = 2.25693915694154;
    a[38][36] = 2.25693915694154;
    a[38][37] = 2.25693915694154;
    a[38][38] = 2.25693915694154;
    a[38][39] = 1.11418515342684;
    a[39][0] = 2.25693033665568;
    a[39][1] = 2.25693033665568;
    a[39][2] = 2.25693033665568;
    a[39][3] = 2.25693033665568;
    a[39][4] = 2.25693033665568;
    a[39][5] = 2.25693033665568;
    a[39][6] = 2.25693033665568;
    a[39][7] = 2.25693033665568;
    a[39][8] = 2.25693033665568;
    a[39][9] = 2.25693033665568;
    a[39][10] = 2.25693033665568;
    a[39][11] = 2.25693033665568;
    a[39][12] = 2.25693033665568;
    a[39][13] = 2.25693033665568;
    a[39][14] = 2.25693033665568;
    a[39][15] = 2.25693033665568;
    a[39][16] = 2.25693033665568;
    a[39][17] = 2.25693033665568;
    a[39][18] = 2.25693033665568;
    a[39][19] = 2.25693033665568;
    a[39][20] = 2.25693033665568;
    a[39][21] = 2.25693033665568;
    a[39][22] = 2.25693033665568;
    a[39][23] = 2.25693033665568;
    a[39][24] = 2.25693033665568;
    a[39][25] = 2.25693033665568;
    a[39][26] = 2.25693033665568;
    a[39][27] = 2.25693033665568;
    a[39][28] = 2.25693033665568;
    a[39][29] = 2.25693033665568;
    a[39][30] = 2.25693033665568;
    a[39][31] = 2.25693033665568;
    a[39][32] = 2.25693033665568;
    a[39][33] = 2.25693033665568;
    a[39][34] = 2.25693033665568;
    a[39][35] = 2.25693033665568;
    a[39][36] = 2.25693033665568;
    a[39][37] = 2.25693033665568;
    a[39][38] = 2.25693033665568;
    a[39][39] = 2.25693033665568;
    a[39][40] = 1.11453349958305;
    a[40][0] = 2.25692214635937;
    a[40][1] = 2.25692214635937;
    a[40][2] = 2.25692214635937;
    a[40][3] = 2.25692214635937;
    a[40][4] = 2.25692214635937;
    a[40][5] = 2.25692214635937;
    a[40][6] = 2.25692214635937;
    a[40][7] = 2.25692214635937;
    a[40][8] = 2.25692214635937;
    a[40][9] = 2.25692214635937;
    a[40][10] = 2.25692214635937;
    a[40][11] = 2.25692214635937;
    a[40][12] = 2.25692214635937;
    a[40][13] = 2.25692214635937;
    a[40][14] = 2.25692214635937;
    a[40][15] = 2.25692214635937;
    a[40][16] = 2.25692214635937;
    a[40][17] = 2.25692214635937;
    a[40][18] = 2.25692214635937;
    a[40][19] = 2.25692214635937;
    a[40][20] = 2.25692214635937;
    a[40][21] = 2.25692214635937;
    a[40][22] = 2.25692214635937;
    a[40][23] = 2.25692214635937;
    a[40][24] = 2.25692214635937;
    a[40][25] = 2.25692214635937;
    a[40][26] = 2.25692214635937;
    a[40][27] = 2.25692214635937;
    a[40][28] = 2.25692214635937;
    a[40][29] = 2.25692214635937;
    a[40][30] = 2.25692214635937;
    a[40][31] = 2.25692214635937;
    a[40][32] = 2.25692214635937;
    a[40][33] = 2.25692214635937;
    a[40][34] = 2.25692214635937;
    a[40][35] = 2.25692214635937;
    a[40][36] = 2.25692214635937;
    a[40][37] = 2.25692214635937;
    a[40][38] = 2.25692214635937;
    a[40][39] = 2.25692214635937;
    a[40][40] = 2.25692214635937;
    a[40][41] = 1.11486515663535;
    a[41][0] = 2.25691452745239;
    a[41][1] = 2.25691452745239;
    a[41][2] = 2.25691452745239;
    a[41][3] = 2.25691452745239;
    a[41][4] = 2.25691452745239;
    a[41][5] = 2.25691452745239;
    a[41][6] = 2.25691452745239;
    a[41][7] = 2.25691452745239;
    a[41][8] = 2.25691452745239;
    a[41][9] = 2.25691452745239;
    a[41][10] = 2.25691452745239;
    a[41][11] = 2.25691452745239;
    a[41][12] = 2.25691452745239;
    a[41][13] = 2.25691452745239;
    a[41][14] = 2.25691452745239;
    a[41][15] = 2.25691452745239;
    a[41][16] = 2.25691452745239;
    a[41][17] = 2.25691452745239;
    a[41][18] = 2.25691452745239;
    a[41][19] = 2.25691452745239;
    a[41][20] = 2.25691452745239;
    a[41][21] = 2.25691452745239;
    a[41][22] = 2.25691452745239;
    a[41][23] = 2.25691452745239;
    a[41][24] = 2.25691452745239;
    a[41][25] = 2.25691452745239;
    a[41][26] = 2.25691452745239;
    a[41][27] = 2.25691452745239;
    a[41][28] = 2.25691452745239;
    a[41][29] = 2.25691452745239;
    a[41][30] = 2.25691452745239;
    a[41][31] = 2.25691452745239;
    a[41][32] = 2.25691452745239;
    a[41][33] = 2.25691452745239;
    a[41][34] = 2.25691452745239;
    a[41][35] = 2.25691452745239;
    a[41][36] = 2.25691452745239;
    a[41][37] = 2.25691452745239;
    a[41][38] = 2.25691452745239;
    a[41][39] = 2.25691452745239;
    a[41][40] = 2.25691452745239;
    a[41][41] = 2.25691452745239;
    a[41][42] = 1.11518129591765;
    a[42][0] = 2.25690742799320;
    a[42][1] = 2.25690742799320;
    a[42][2] = 2.25690742799320;
    a[42][3] = 2.25690742799320;
    a[42][4] = 2.25690742799320;
    a[42][5] = 2.25690742799320;
    a[42][6] = 2.25690742799320;
    a[42][7] = 2.25690742799320;
    a[42][8] = 2.25690742799320;
    a[42][9] = 2.25690742799320;
    a[42][10] = 2.25690742799320;
    a[42][11] = 2.25690742799320;
    a[42][12] = 2.25690742799320;
    a[42][13] = 2.25690742799320;
    a[42][14] = 2.25690742799320;
    a[42][15] = 2.25690742799320;
    a[42][16] = 2.25690742799320;
    a[42][17] = 2.25690742799320;
    a[42][18] = 2.25690742799320;
    a[42][19] = 2.25690742799320;
    a[42][20] = 2.25690742799320;
    a[42][21] = 2.25690742799320;
    a[42][22] = 2.25690742799320;
    a[42][23] = 2.25690742799320;
    a[42][24] = 2.25690742799320;
    a[42][25] = 2.25690742799320;
    a[42][26] = 2.25690742799320;
    a[42][27] = 2.25690742799320;
    a[42][28] = 2.25690742799320;
    a[42][29] = 2.25690742799320;
    a[42][30] = 2.25690742799320;
    a[42][31] = 2.25690742799320;
    a[42][32] = 2.25690742799320;
    a[42][33] = 2.25690742799320;
    a[42][34] = 2.25690742799320;
    a[42][35] = 2.25690742799320;
    a[42][36] = 2.25690742799320;
    a[42][37] = 2.25690742799320;
    a[42][38] = 2.25690742799320;
    a[42][39] = 2.25690742799320;
    a[42][40] = 2.25690742799320;
    a[42][41] = 2.25690742799320;
    a[42][42] = 2.25690742799320;
    a[42][43] = 1.11548298165181;
    a[43][0] = 2.25690080181114;
    a[43][1] = 2.25690080181114;
    a[43][2] = 2.25690080181114;
    a[43][3] = 2.25690080181114;
    a[43][4] = 2.25690080181114;
    a[43][5] = 2.25690080181114;
    a[43][6] = 2.25690080181114;
    a[43][7] = 2.25690080181114;
    a[43][8] = 2.25690080181114;
    a[43][9] = 2.25690080181114;
    a[43][10] = 2.25690080181114;
    a[43][11] = 2.25690080181114;
    a[43][12] = 2.25690080181114;
    a[43][13] = 2.25690080181114;
    a[43][14] = 2.25690080181114;
    a[43][15] = 2.25690080181114;
    a[43][16] = 2.25690080181114;
    a[43][17] = 2.25690080181114;
    a[43][18] = 2.25690080181114;
    a[43][19] = 2.25690080181114;
    a[43][20] = 2.25690080181114;
    a[43][21] = 2.25690080181114;
    a[43][22] = 2.25690080181114;
    a[43][23] = 2.25690080181114;
    a[43][24] = 2.25690080181114;
    a[43][25] = 2.25690080181114;
    a[43][26] = 2.25690080181114;
    a[43][27] = 2.25690080181114;
    a[43][28] = 2.25690080181114;
    a[43][29] = 2.25690080181114;
    a[43][30] = 2.25690080181114;
    a[43][31] = 2.25690080181114;
    a[43][32] = 2.25690080181114;
    a[43][33] = 2.25690080181114;
    a[43][34] = 2.25690080181114;
    a[43][35] = 2.25690080181114;
    a[43][36] = 2.25690080181114;
    a[43][37] = 2.25690080181114;
    a[43][38] = 2.25690080181114;
    a[43][39] = 2.25690080181114;
    a[43][40] = 2.25690080181114;
    a[43][41] = 2.25690080181114;
    a[43][42] = 2.25690080181114;
    a[43][43] = 2.25690080181114;
    a[43][44] = 1.11577118291787;
    a[44][0] = 2.25689460775379;
    a[44][1] = 2.25689460775379;
    a[44][2] = 2.25689460775379;
    a[44][3] = 2.25689460775379;
    a[44][4] = 2.25689460775379;
    a[44][5] = 2.25689460775379;
    a[44][6] = 2.25689460775379;
    a[44][7] = 2.25689460775379;
    a[44][8] = 2.25689460775379;
    a[44][9] = 2.25689460775379;
    a[44][10] = 2.25689460775379;
    a[44][11] = 2.25689460775379;
    a[44][12] = 2.25689460775379;
    a[44][13] = 2.25689460775379;
    a[44][14] = 2.25689460775379;
    a[44][15] = 2.25689460775379;
    a[44][16] = 2.25689460775379;
    a[44][17] = 2.25689460775379;
    a[44][18] = 2.25689460775379;
    a[44][19] = 2.25689460775379;
    a[44][20] = 2.25689460775379;
    a[44][21] = 2.25689460775379;
    a[44][22] = 2.25689460775379;
    a[44][23] = 2.25689460775379;
    a[44][24] = 2.25689460775379;
    a[44][25] = 2.25689460775379;
    a[44][26] = 2.25689460775379;
    a[44][27] = 2.25689460775379;
    a[44][28] = 2.25689460775379;
    a[44][29] = 2.25689460775379;
    a[44][30] = 2.25689460775379;
    a[44][31] = 2.25689460775379;
    a[44][32] = 2.25689460775379;
    a[44][33] = 2.25689460775379;
    a[44][34] = 2.25689460775379;
    a[44][35] = 2.25689460775379;
    a[44][36] = 2.25689460775379;
    a[44][37] = 2.25689460775379;
    a[44][38] = 2.25689460775379;
    a[44][39] = 2.25689460775379;
    a[44][40] = 2.25689460775379;
    a[44][41] = 2.25689460775379;
    a[44][42] = 2.25689460775379;
    a[44][43] = 2.25689460775379;
    a[44][44] = 2.25689460775379;
    a[44][45] = 1.11604678405407;
    a[45][0] = 2.25688880904640;
    a[45][1] = 2.25688880904640;
    a[45][2] = 2.25688880904640;
    a[45][3] = 2.25688880904640;
    a[45][4] = 2.25688880904640;
    a[45][5] = 2.25688880904640;
    a[45][6] = 2.25688880904640;
    a[45][7] = 2.25688880904640;
    a[45][8] = 2.25688880904640;
    a[45][9] = 2.25688880904640;
    a[45][10] = 2.25688880904640;
    a[45][11] = 2.25688880904640;
    a[45][12] = 2.25688880904640;
    a[45][13] = 2.25688880904640;
    a[45][14] = 2.25688880904640;
    a[45][15] = 2.25688880904640;
    a[45][16] = 2.25688880904640;
    a[45][17] = 2.25688880904640;
    a[45][18] = 2.25688880904640;
    a[45][19] = 2.25688880904640;
    a[45][20] = 2.25688880904640;
    a[45][21] = 2.25688880904640;
    a[45][22] = 2.25688880904640;
    a[45][23] = 2.25688880904640;
    a[45][24] = 2.25688880904640;
    a[45][25] = 2.25688880904640;
    a[45][26] = 2.25688880904640;
    a[45][27] = 2.25688880904640;
    a[45][28] = 2.25688880904640;
    a[45][29] = 2.25688880904640;
    a[45][30] = 2.25688880904640;
    a[45][31] = 2.25688880904640;
    a[45][32] = 2.25688880904640;
    a[45][33] = 2.25688880904640;
    a[45][34] = 2.25688880904640;
    a[45][35] = 2.25688880904640;
    a[45][36] = 2.25688880904640;
    a[45][37] = 2.25688880904640;
    a[45][38] = 2.25688880904640;
    a[45][39] = 2.25688880904640;
    a[45][40] = 2.25688880904640;
    a[45][41] = 2.25688880904640;
    a[45][42] = 2.25688880904640;
    a[45][43] = 2.25688880904640;
    a[45][44] = 2.25688880904640;
    a[45][45] = 2.25688880904640;
    a[45][46] = 1.11631059372188;
    a[46][0] = 2.25688337274470;
    a[46][1] = 2.25688337274470;
    a[46][2] = 2.25688337274470;
    a[46][3] = 2.25688337274470;
    a[46][4] = 2.25688337274470;
    a[46][5] = 2.25688337274470;
    a[46][6] = 2.25688337274470;
    a[46][7] = 2.25688337274470;
    a[46][8] = 2.25688337274470;
    a[46][9] = 2.25688337274470;
    a[46][10] = 2.25688337274470;
    a[46][11] = 2.25688337274470;
    a[46][12] = 2.25688337274470;
    a[46][13] = 2.25688337274470;
    a[46][14] = 2.25688337274470;
    a[46][15] = 2.25688337274470;
    a[46][16] = 2.25688337274470;
    a[46][17] = 2.25688337274470;
    a[46][18] = 2.25688337274470;
    a[46][19] = 2.25688337274470;
    a[46][20] = 2.25688337274470;
    a[46][21] = 2.25688337274470;
    a[46][22] = 2.25688337274470;
    a[46][23] = 2.25688337274470;
    a[46][24] = 2.25688337274470;
    a[46][25] = 2.25688337274470;
    a[46][26] = 2.25688337274470;
    a[46][27] = 2.25688337274470;
    a[46][28] = 2.25688337274470;
    a[46][29] = 2.25688337274470;
    a[46][30] = 2.25688337274470;
    a[46][31] = 2.25688337274470;
    a[46][32] = 2.25688337274470;
    a[46][33] = 2.25688337274470;
    a[46][34] = 2.25688337274470;
    a[46][35] = 2.25688337274470;
    a[46][36] = 2.25688337274470;
    a[46][37] = 2.25688337274470;
    a[46][38] = 2.25688337274470;
    a[46][39] = 2.25688337274470;
    a[46][40] = 2.25688337274470;
    a[46][41] = 2.25688337274470;
    a[46][42] = 2.25688337274470;
    a[46][43] = 2.25688337274470;
    a[46][44] = 2.25688337274470;
    a[46][45] = 2.25688337274470;
    a[46][46] = 2.25688337274470;
    a[46][47] = 1.11656335283159;
    a[47][0] = 2.25687826926587;
    a[47][1] = 2.25687826926587;
    a[47][2] = 2.25687826926587;
    a[47][3] = 2.25687826926587;
    a[47][4] = 2.25687826926587;
    a[47][5] = 2.25687826926587;
    a[47][6] = 2.25687826926587;
    a[47][7] = 2.25687826926587;
    a[47][8] = 2.25687826926587;
    a[47][9] = 2.25687826926587;
    a[47][10] = 2.25687826926587;
    a[47][11] = 2.25687826926587;
    a[47][12] = 2.25687826926587;
    a[47][13] = 2.25687826926587;
    a[47][14] = 2.25687826926587;
    a[47][15] = 2.25687826926587;
    a[47][16] = 2.25687826926587;
    a[47][17] = 2.25687826926587;
    a[47][18] = 2.25687826926587;
    a[47][19] = 2.25687826926587;
    a[47][20] = 2.25687826926587;
    a[47][21] = 2.25687826926587;
    a[47][22] = 2.25687826926587;
    a[47][23] = 2.25687826926587;
    a[47][24] = 2.25687826926587;
    a[47][25] = 2.25687826926587;
    a[47][26] = 2.25687826926587;
    a[47][27] = 2.25687826926587;
    a[47][28] = 2.25687826926587;
    a[47][29] = 2.25687826926587;
    a[47][30] = 2.25687826926587;
    a[47][31] = 2.25687826926587;
    a[47][32] = 2.25687826926587;
    a[47][33] = 2.25687826926587;
    a[47][34] = 2.25687826926587;
    a[47][35] = 2.25687826926587;
    a[47][36] = 2.25687826926587;
    a[47][37] = 2.25687826926587;
    a[47][38] = 2.25687826926587;
    a[47][39] = 2.25687826926587;
    a[47][40] = 2.25687826926587;
    a[47][41] = 2.25687826926587;
    a[47][42] = 2.25687826926587;
    a[47][43] = 2.25687826926587;
    a[47][44] = 2.25687826926587;
    a[47][45] = 2.25687826926587;
    a[47][46] = 2.25687826926587;
    a[47][47] = 2.25687826926587;
    a[47][48] = 1.11680574149239;
    a[48][0] = 2.25687347198525;
    a[48][1] = 2.25687347198525;
    a[48][2] = 2.25687347198525;
    a[48][3] = 2.25687347198525;
    a[48][4] = 2.25687347198525;
    a[48][5] = 2.25687347198525;
    a[48][6] = 2.25687347198525;
    a[48][7] = 2.25687347198525;
    a[48][8] = 2.25687347198525;
    a[48][9] = 2.25687347198525;
    a[48][10] = 2.25687347198525;
    a[48][11] = 2.25687347198525;
    a[48][12] = 2.25687347198525;
    a[48][13] = 2.25687347198525;
    a[48][14] = 2.25687347198525;
    a[48][15] = 2.25687347198525;
    a[48][16] = 2.25687347198525;
    a[48][17] = 2.25687347198525;
    a[48][18] = 2.25687347198525;
    a[48][19] = 2.25687347198525;
    a[48][20] = 2.25687347198525;
    a[48][21] = 2.25687347198525;
    a[48][22] = 2.25687347198525;
    a[48][23] = 2.25687347198525;
    a[48][24] = 2.25687347198525;
    a[48][25] = 2.25687347198525;
    a[48][26] = 2.25687347198525;
    a[48][27] = 2.25687347198525;
    a[48][28] = 2.25687347198525;
    a[48][29] = 2.25687347198525;
    a[48][30] = 2.25687347198525;
    a[48][31] = 2.25687347198525;
    a[48][32] = 2.25687347198525;
    a[48][33] = 2.25687347198525;
    a[48][34] = 2.25687347198525;
    a[48][35] = 2.25687347198525;
    a[48][36] = 2.25687347198525;
    a[48][37] = 2.25687347198525;
    a[48][38] = 2.25687347198525;
    a[48][39] = 2.25687347198525;
    a[48][40] = 2.25687347198525;
    a[48][41] = 2.25687347198525;
    a[48][42] = 2.25687347198525;
    a[48][43] = 2.25687347198525;
    a[48][44] = 2.25687347198525;
    a[48][45] = 2.25687347198525;
    a[48][46] = 2.25687347198525;
    a[48][47] = 2.25687347198525;
    a[48][48] = 2.25687347198525;
    a[48][49] = 1.11703838512401;
    a[49][0] = 2.25686895688829;
    a[49][1] = 2.25686895688829;
    a[49][2] = 2.25686895688829;
    a[49][3] = 2.25686895688829;
    a[49][4] = 2.25686895688829;
    a[49][5] = 2.25686895688829;
    a[49][6] = 2.25686895688829;
    a[49][7] = 2.25686895688829;
    a[49][8] = 2.25686895688829;
    a[49][9] = 2.25686895688829;
    a[49][10] = 2.25686895688829;
    a[49][11] = 2.25686895688829;
    a[49][12] = 2.25686895688829;
    a[49][13] = 2.25686895688829;
    a[49][14] = 2.25686895688829;
    a[49][15] = 2.25686895688829;
    a[49][16] = 2.25686895688829;
    a[49][17] = 2.25686895688829;
    a[49][18] = 2.25686895688829;
    a[49][19] = 2.25686895688829;
    a[49][20] = 2.25686895688829;
    a[49][21] = 2.25686895688829;
    a[49][22] = 2.25686895688829;
    a[49][23] = 2.25686895688829;
    a[49][24] = 2.25686895688829;
    a[49][25] = 2.25686895688829;
    a[49][26] = 2.25686895688829;
    a[49][27] = 2.25686895688829;
    a[49][28] = 2.25686895688829;
    a[49][29] = 2.25686895688829;
    a[49][30] = 2.25686895688829;
    a[49][31] = 2.25686895688829;
    a[49][32] = 2.25686895688829;
    a[49][33] = 2.25686895688829;
    a[49][34] = 2.25686895688829;
    a[49][35] = 2.25686895688829;
    a[49][36] = 2.25686895688829;
    a[49][37] = 2.25686895688829;
    a[49][38] = 2.25686895688829;
    a[49][39] = 2.25686895688829;
    a[49][40] = 2.25686895688829;
    a[49][41] = 2.25686895688829;
    a[49][42] = 2.25686895688829;
    a[49][43] = 2.25686895688829;
    a[49][44] = 2.25686895688829;
    a[49][45] = 2.25686895688829;
    a[49][46] = 2.25686895688829;
    a[49][47] = 2.25686895688829;
    a[49][48] = 2.25686895688829;
    a[49][49] = 2.25686895688829;
    a[49][50] = 1.11726185984569;
    a[50][0] = 2.25686470226943;
    a[50][1] = 2.25686470226943;
    a[50][2] = 2.25686470226943;
    a[50][3] = 2.25686470226943;
    a[50][4] = 2.25686470226943;
    a[50][5] = 2.25686470226943;
    a[50][6] = 2.25686470226943;
    a[50][7] = 2.25686470226943;
    a[50][8] = 2.25686470226943;
    a[50][9] = 2.25686470226943;
    a[50][10] = 2.25686470226943;
    a[50][11] = 2.25686470226943;
    a[50][12] = 2.25686470226943;
    a[50][13] = 2.25686470226943;
    a[50][14] = 2.25686470226943;
    a[50][15] = 2.25686470226943;
    a[50][16] = 2.25686470226943;
    a[50][17] = 2.25686470226943;
    a[50][18] = 2.25686470226943;
    a[50][19] = 2.25686470226943;
    a[50][20] = 2.25686470226943;
    a[50][21] = 2.25686470226943;
    a[50][22] = 2.25686470226943;
    a[50][23] = 2.25686470226943;
    a[50][24] = 2.25686470226943;
    a[50][25] = 2.25686470226943;
    a[50][26] = 2.25686470226943;
    a[50][27] = 2.25686470226943;
    a[50][28] = 2.25686470226943;
    a[50][29] = 2.25686470226943;
    a[50][30] = 2.25686470226943;
    a[50][31] = 2.25686470226943;
    a[50][32] = 2.25686470226943;
    a[50][33] = 2.25686470226943;
    a[50][34] = 2.25686470226943;
    a[50][35] = 2.25686470226943;
    a[50][36] = 2.25686470226943;
    a[50][37] = 2.25686470226943;
    a[50][38] = 2.25686470226943;
    a[50][39] = 2.25686470226943;
    a[50][40] = 2.25686470226943;
    a[50][41] = 2.25686470226943;
    a[50][42] = 2.25686470226943;
    a[50][43] = 2.25686470226943;
    a[50][44] = 2.25686470226943;
    a[50][45] = 2.25686470226943;
    a[50][46] = 2.25686470226943;
    a[50][47] = 2.25686470226943;
    a[50][48] = 2.25686470226943;
    a[50][49] = 2.25686470226943;
    a[50][50] = 2.25686470226943;
    a[50][51] = 1.11747669724020;
    a[51][0] = 2.25686068847070;
    a[51][1] = 2.25686068847070;
    a[51][2] = 2.25686068847070;
    a[51][3] = 2.25686068847070;
    a[51][4] = 2.25686068847070;
    a[51][5] = 2.25686068847070;
    a[51][6] = 2.25686068847070;
    a[51][7] = 2.25686068847070;
    a[51][8] = 2.25686068847070;
    a[51][9] = 2.25686068847070;
    a[51][10] = 2.25686068847070;
    a[51][11] = 2.25686068847070;
    a[51][12] = 2.25686068847070;
    a[51][13] = 2.25686068847070;
    a[51][14] = 2.25686068847070;
    a[51][15] = 2.25686068847070;
    a[51][16] = 2.25686068847070;
    a[51][17] = 2.25686068847070;
    a[51][18] = 2.25686068847070;
    a[51][19] = 2.25686068847070;
    a[51][20] = 2.25686068847070;
    a[51][21] = 2.25686068847070;
    a[51][22] = 2.25686068847070;
    a[51][23] = 2.25686068847070;
    a[51][24] = 2.25686068847070;
    a[51][25] = 2.25686068847070;
    a[51][26] = 2.25686068847070;
    a[51][27] = 2.25686068847070;
    a[51][28] = 2.25686068847070;
    a[51][29] = 2.25686068847070;
    a[51][30] = 2.25686068847070;
    a[51][31] = 2.25686068847070;
    a[51][32] = 2.25686068847070;
    a[51][33] = 2.25686068847070;
    a[51][34] = 2.25686068847070;
    a[51][35] = 2.25686068847070;
    a[51][36] = 2.25686068847070;
    a[51][37] = 2.25686068847070;
    a[51][38] = 2.25686068847070;
    a[51][39] = 2.25686068847070;
    a[51][40] = 2.25686068847070;
    a[51][41] = 2.25686068847070;
    a[51][42] = 2.25686068847070;
    a[51][43] = 2.25686068847070;
    a[51][44] = 2.25686068847070;
    a[51][45] = 2.25686068847070;
    a[51][46] = 2.25686068847070;
    a[51][47] = 2.25686068847070;
    a[51][48] = 2.25686068847070;
    a[51][49] = 2.25686068847070;
    a[51][50] = 2.25686068847070;
    a[51][51] = 2.25686068847070;
    a[51][52] = 1.11768338857596;
    a[52][0] = 2.25685689765423;
    a[52][1] = 2.25685689765423;
    a[52][2] = 2.25685689765423;
    a[52][3] = 2.25685689765423;
    a[52][4] = 2.25685689765423;
    a[52][5] = 2.25685689765423;
    a[52][6] = 2.25685689765423;
    a[52][7] = 2.25685689765423;
    a[52][8] = 2.25685689765423;
    a[52][9] = 2.25685689765423;
    a[52][10] = 2.25685689765423;
    a[52][11] = 2.25685689765423;
    a[52][12] = 2.25685689765423;
    a[52][13] = 2.25685689765423;
    a[52][14] = 2.25685689765423;
    a[52][15] = 2.25685689765423;
    a[52][16] = 2.25685689765423;
    a[52][17] = 2.25685689765423;
    a[52][18] = 2.25685689765423;
    a[52][19] = 2.25685689765423;
    a[52][20] = 2.25685689765423;
    a[52][21] = 2.25685689765423;
    a[52][22] = 2.25685689765423;
    a[52][23] = 2.25685689765423;
    a[52][24] = 2.25685689765423;
    a[52][25] = 2.25685689765423;
    a[52][26] = 2.25685689765423;
    a[52][27] = 2.25685689765423;
    a[52][28] = 2.25685689765423;
    a[52][29] = 2.25685689765423;
    a[52][30] = 2.25685689765423;
    a[52][31] = 2.25685689765423;
    a[52][32] = 2.25685689765423;
    a[52][33] = 2.25685689765423;
    a[52][34] = 2.25685689765423;
    a[52][35] = 2.25685689765423;
    a[52][36] = 2.25685689765423;
    a[52][37] = 2.25685689765423;
    a[52][38] = 2.25685689765423;
    a[52][39] = 2.25685689765423;
    a[52][40] = 2.25685689765423;
    a[52][41] = 2.25685689765423;
    a[52][42] = 2.25685689765423;
    a[52][43] = 2.25685689765423;
    a[52][44] = 2.25685689765423;
    a[52][45] = 2.25685689765423;
    a[52][46] = 2.25685689765423;
    a[52][47] = 2.25685689765423;
    a[52][48] = 2.25685689765423;
    a[52][49] = 2.25685689765423;
    a[52][50] = 2.25685689765423;
    a[52][51] = 2.25685689765423;
    a[52][52] = 2.25685689765423;
    a[52][53] = 1.11788238855770;
    a[53][0] = 2.25685331360371;
    a[53][1] = 2.25685331360371;
    a[53][2] = 2.25685331360371;
    a[53][3] = 2.25685331360371;
    a[53][4] = 2.25685331360371;
    a[53][5] = 2.25685331360371;
    a[53][6] = 2.25685331360371;
    a[53][7] = 2.25685331360371;
    a[53][8] = 2.25685331360371;
    a[53][9] = 2.25685331360371;
    a[53][10] = 2.25685331360371;
    a[53][11] = 2.25685331360371;
    a[53][12] = 2.25685331360371;
    a[53][13] = 2.25685331360371;
    a[53][14] = 2.25685331360371;
    a[53][15] = 2.25685331360371;
    a[53][16] = 2.25685331360371;
    a[53][17] = 2.25685331360371;
    a[53][18] = 2.25685331360371;
    a[53][19] = 2.25685331360371;
    a[53][20] = 2.25685331360371;
    a[53][21] = 2.25685331360371;
    a[53][22] = 2.25685331360371;
    a[53][23] = 2.25685331360371;
    a[53][24] = 2.25685331360371;
    a[53][25] = 2.25685331360371;
    a[53][26] = 2.25685331360371;
    a[53][27] = 2.25685331360371;
    a[53][28] = 2.25685331360371;
    a[53][29] = 2.25685331360371;
    a[53][30] = 2.25685331360371;
    a[53][31] = 2.25685331360371;
    a[53][32] = 2.25685331360371;
    a[53][33] = 2.25685331360371;
    a[53][34] = 2.25685331360371;
    a[53][35] = 2.25685331360371;
    a[53][36] = 2.25685331360371;
    a[53][37] = 2.25685331360371;
    a[53][38] = 2.25685331360371;
    a[53][39] = 2.25685331360371;
    a[53][40] = 2.25685331360371;
    a[53][41] = 2.25685331360371;
    a[53][42] = 2.25685331360371;
    a[53][43] = 2.25685331360371;
    a[53][44] = 2.25685331360371;
    a[53][45] = 2.25685331360371;
    a[53][46] = 2.25685331360371;
    a[53][47] = 2.25685331360371;
    a[53][48] = 2.25685331360371;
    a[53][49] = 2.25685331360371;
    a[53][50] = 2.25685331360371;
    a[53][51] = 2.25685331360371;
    a[53][52] = 2.25685331360371;
    a[53][53] = 2.25685331360371;
    a[53][54] = 1.11807411866606;
    a[54][0] = 2.25684992155066;
    a[54][1] = 2.25684992155066;
    a[54][2] = 2.25684992155066;
    a[54][3] = 2.25684992155066;
    a[54][4] = 2.25684992155066;
    a[54][5] = 2.25684992155066;
    a[54][6] = 2.25684992155066;
    a[54][7] = 2.25684992155066;
    a[54][8] = 2.25684992155066;
    a[54][9] = 2.25684992155066;
    a[54][10] = 2.25684992155066;
    a[54][11] = 2.25684992155066;
    a[54][12] = 2.25684992155066;
    a[54][13] = 2.25684992155066;
    a[54][14] = 2.25684992155066;
    a[54][15] = 2.25684992155066;
    a[54][16] = 2.25684992155066;
    a[54][17] = 2.25684992155066;
    a[54][18] = 2.25684992155066;
    a[54][19] = 2.25684992155066;
    a[54][20] = 2.25684992155066;
    a[54][21] = 2.25684992155066;
    a[54][22] = 2.25684992155066;
    a[54][23] = 2.25684992155066;
    a[54][24] = 2.25684992155066;
    a[54][25] = 2.25684992155066;
    a[54][26] = 2.25684992155066;
    a[54][27] = 2.25684992155066;
    a[54][28] = 2.25684992155066;
    a[54][29] = 2.25684992155066;
    a[54][30] = 2.25684992155066;
    a[54][31] = 2.25684992155066;
    a[54][32] = 2.25684992155066;
    a[54][33] = 2.25684992155066;
    a[54][34] = 2.25684992155066;
    a[54][35] = 2.25684992155066;
    a[54][36] = 2.25684992155066;
    a[54][37] = 2.25684992155066;
    a[54][38] = 2.25684992155066;
    a[54][39] = 2.25684992155066;
    a[54][40] = 2.25684992155066;
    a[54][41] = 2.25684992155066;
    a[54][42] = 2.25684992155066;
    a[54][43] = 2.25684992155066;
    a[54][44] = 2.25684992155066;
    a[54][45] = 2.25684992155066;
    a[54][46] = 2.25684992155066;
    a[54][47] = 2.25684992155066;
    a[54][48] = 2.25684992155066;
    a[54][49] = 2.25684992155066;
    a[54][50] = 2.25684992155066;
    a[54][51] = 2.25684992155066;
    a[54][52] = 2.25684992155066;
    a[54][53] = 2.25684992155066;
    a[54][54] = 2.25684992155066;
    a[54][55] = 1.11825897013772;
    a[55][0] = 2.25684670802201;
    a[55][1] = 2.25684670802201;
    a[55][2] = 2.25684670802201;
    a[55][3] = 2.25684670802201;
    a[55][4] = 2.25684670802201;
    a[55][5] = 2.25684670802201;
    a[55][6] = 2.25684670802201;
    a[55][7] = 2.25684670802201;
    a[55][8] = 2.25684670802201;
    a[55][9] = 2.25684670802201;
    a[55][10] = 2.25684670802201;
    a[55][11] = 2.25684670802201;
    a[55][12] = 2.25684670802201;
    a[55][13] = 2.25684670802201;
    a[55][14] = 2.25684670802201;
    a[55][15] = 2.25684670802201;
    a[55][16] = 2.25684670802201;
    a[55][17] = 2.25684670802201;
    a[55][18] = 2.25684670802201;
    a[55][19] = 2.25684670802201;
    a[55][20] = 2.25684670802201;
    a[55][21] = 2.25684670802201;
    a[55][22] = 2.25684670802201;
    a[55][23] = 2.25684670802201;
    a[55][24] = 2.25684670802201;
    a[55][25] = 2.25684670802201;
    a[55][26] = 2.25684670802201;
    a[55][27] = 2.25684670802201;
    a[55][28] = 2.25684670802201;
    a[55][29] = 2.25684670802201;
    a[55][30] = 2.25684670802201;
    a[55][31] = 2.25684670802201;
    a[55][32] = 2.25684670802201;
    a[55][33] = 2.25684670802201;
    a[55][34] = 2.25684670802201;
    a[55][35] = 2.25684670802201;
    a[55][36] = 2.25684670802201;
    a[55][37] = 2.25684670802201;
    a[55][38] = 2.25684670802201;
    a[55][39] = 2.25684670802201;
    a[55][40] = 2.25684670802201;
    a[55][41] = 2.25684670802201;
    a[55][42] = 2.25684670802201;
    a[55][43] = 2.25684670802201;
    a[55][44] = 2.25684670802201;
    a[55][45] = 2.25684670802201;
    a[55][46] = 2.25684670802201;
    a[55][47] = 2.25684670802201;
    a[55][48] = 2.25684670802201;
    a[55][49] = 2.25684670802201;
    a[55][50] = 2.25684670802201;
    a[55][51] = 2.25684670802201;
    a[55][52] = 2.25684670802201;
    a[55][53] = 2.25684670802201;
    a[55][54] = 2.25684670802201;
    a[55][55] = 2.25684670802201;
    a[55][56] = 1.11843730663038;
    a[56][0] = 2.25684366070614;
    a[56][1] = 2.25684366070614;
    a[56][2] = 2.25684366070614;
    a[56][3] = 2.25684366070614;
    a[56][4] = 2.25684366070614;
    a[56][5] = 2.25684366070614;
    a[56][6] = 2.25684366070614;
    a[56][7] = 2.25684366070614;
    a[56][8] = 2.25684366070614;
    a[56][9] = 2.25684366070614;
    a[56][10] = 2.25684366070614;
    a[56][11] = 2.25684366070614;
    a[56][12] = 2.25684366070614;
    a[56][13] = 2.25684366070614;
    a[56][14] = 2.25684366070614;
    a[56][15] = 2.25684366070614;
    a[56][16] = 2.25684366070614;
    a[56][17] = 2.25684366070614;
    a[56][18] = 2.25684366070614;
    a[56][19] = 2.25684366070614;
    a[56][20] = 2.25684366070614;
    a[56][21] = 2.25684366070614;
    a[56][22] = 2.25684366070614;
    a[56][23] = 2.25684366070614;
    a[56][24] = 2.25684366070614;
    a[56][25] = 2.25684366070614;
    a[56][26] = 2.25684366070614;
    a[56][27] = 2.25684366070614;
    a[56][28] = 2.25684366070614;
    a[56][29] = 2.25684366070614;
    a[56][30] = 2.25684366070614;
    a[56][31] = 2.25684366070614;
    a[56][32] = 2.25684366070614;
    a[56][33] = 2.25684366070614;
    a[56][34] = 2.25684366070614;
    a[56][35] = 2.25684366070614;
    a[56][36] = 2.25684366070614;
    a[56][37] = 2.25684366070614;
    a[56][38] = 2.25684366070614;
    a[56][39] = 2.25684366070614;
    a[56][40] = 2.25684366070614;
    a[56][41] = 2.25684366070614;
    a[56][42] = 2.25684366070614;
    a[56][43] = 2.25684366070614;
    a[56][44] = 2.25684366070614;
    a[56][45] = 2.25684366070614;
    a[56][46] = 2.25684366070614;
    a[56][47] = 2.25684366070614;
    a[56][48] = 2.25684366070614;
    a[56][49] = 2.25684366070614;
    a[56][50] = 2.25684366070614;
    a[56][51] = 2.25684366070614;
    a[56][52] = 2.25684366070614;
    a[56][53] = 2.25684366070614;
    a[56][54] = 2.25684366070614;
    a[56][55] = 2.25684366070614;
    a[56][56] = 2.25684366070614;
    a[56][57] = 1.11860946661087;
    a[57][0] = 2.25684076833472;
    a[57][1] = 2.25684076833472;
    a[57][2] = 2.25684076833472;
    a[57][3] = 2.25684076833472;
    a[57][4] = 2.25684076833472;
    a[57][5] = 2.25684076833472;
    a[57][6] = 2.25684076833472;
    a[57][7] = 2.25684076833472;
    a[57][8] = 2.25684076833472;
    a[57][9] = 2.25684076833472;
    a[57][10] = 2.25684076833472;
    a[57][11] = 2.25684076833472;
    a[57][12] = 2.25684076833472;
    a[57][13] = 2.25684076833472;
    a[57][14] = 2.25684076833472;
    a[57][15] = 2.25684076833472;
    a[57][16] = 2.25684076833472;
    a[57][17] = 2.25684076833472;
    a[57][18] = 2.25684076833472;
    a[57][19] = 2.25684076833472;
    a[57][20] = 2.25684076833472;
    a[57][21] = 2.25684076833472;
    a[57][22] = 2.25684076833472;
    a[57][23] = 2.25684076833472;
    a[57][24] = 2.25684076833472;
    a[57][25] = 2.25684076833472;
    a[57][26] = 2.25684076833472;
    a[57][27] = 2.25684076833472;
    a[57][28] = 2.25684076833472;
    a[57][29] = 2.25684076833472;
    a[57][30] = 2.25684076833472;
    a[57][31] = 2.25684076833472;
    a[57][32] = 2.25684076833472;
    a[57][33] = 2.25684076833472;
    a[57][34] = 2.25684076833472;
    a[57][35] = 2.25684076833472;
    a[57][36] = 2.25684076833472;
    a[57][37] = 2.25684076833472;
    a[57][38] = 2.25684076833472;
    a[57][39] = 2.25684076833472;
    a[57][40] = 2.25684076833472;
    a[57][41] = 2.25684076833472;
    a[57][42] = 2.25684076833472;
    a[57][43] = 2.25684076833472;
    a[57][44] = 2.25684076833472;
    a[57][45] = 2.25684076833472;
    a[57][46] = 2.25684076833472;
    a[57][47] = 2.25684076833472;
    a[57][48] = 2.25684076833472;
    a[57][49] = 2.25684076833472;
    a[57][50] = 2.25684076833472;
    a[57][51] = 2.25684076833472;
    a[57][52] = 2.25684076833472;
    a[57][53] = 2.25684076833472;
    a[57][54] = 2.25684076833472;
    a[57][55] = 2.25684076833472;
    a[57][56] = 2.25684076833472;
    a[57][57] = 2.25684076833472;
    a[57][58] = 1.11877576549926;
    a[58][0] = 2.25683802057844;
    a[58][1] = 2.25683802057844;
    a[58][2] = 2.25683802057844;
    a[58][3] = 2.25683802057844;
    a[58][4] = 2.25683802057844;
    a[58][5] = 2.25683802057844;
    a[58][6] = 2.25683802057844;
    a[58][7] = 2.25683802057844;
    a[58][8] = 2.25683802057844;
    a[58][9] = 2.25683802057844;
    a[58][10] = 2.25683802057844;
    a[58][11] = 2.25683802057844;
    a[58][12] = 2.25683802057844;
    a[58][13] = 2.25683802057844;
    a[58][14] = 2.25683802057844;
    a[58][15] = 2.25683802057844;
    a[58][16] = 2.25683802057844;
    a[58][17] = 2.25683802057844;
    a[58][18] = 2.25683802057844;
    a[58][19] = 2.25683802057844;
    a[58][20] = 2.25683802057844;
    a[58][21] = 2.25683802057844;
    a[58][22] = 2.25683802057844;
    a[58][23] = 2.25683802057844;
    a[58][24] = 2.25683802057844;
    a[58][25] = 2.25683802057844;
    a[58][26] = 2.25683802057844;
    a[58][27] = 2.25683802057844;
    a[58][28] = 2.25683802057844;
    a[58][29] = 2.25683802057844;
    a[58][30] = 2.25683802057844;
    a[58][31] = 2.25683802057844;
    a[58][32] = 2.25683802057844;
    a[58][33] = 2.25683802057844;
    a[58][34] = 2.25683802057844;
    a[58][35] = 2.25683802057844;
    a[58][36] = 2.25683802057844;
    a[58][37] = 2.25683802057844;
    a[58][38] = 2.25683802057844;
    a[58][39] = 2.25683802057844;
    a[58][40] = 2.25683802057844;
    a[58][41] = 2.25683802057844;
    a[58][42] = 2.25683802057844;
    a[58][43] = 2.25683802057844;
    a[58][44] = 2.25683802057844;
    a[58][45] = 2.25683802057844;
    a[58][46] = 2.25683802057844;
    a[58][47] = 2.25683802057844;
    a[58][48] = 2.25683802057844;
    a[58][49] = 2.25683802057844;
    a[58][50] = 2.25683802057844;
    a[58][51] = 2.25683802057844;
    a[58][52] = 2.25683802057844;
    a[58][53] = 2.25683802057844;
    a[58][54] = 2.25683802057844;
    a[58][55] = 2.25683802057844;
    a[58][56] = 2.25683802057844;
    a[58][57] = 2.25683802057844;
    a[58][58] = 2.25683802057844;
    a[58][59] = 1.11893649759772;
    a[59][0] = 2.25683540795462;
    a[59][1] = 2.25683540795462;
    a[59][2] = 2.25683540795462;
    a[59][3] = 2.25683540795462;
    a[59][4] = 2.25683540795462;
    a[59][5] = 2.25683540795462;
    a[59][6] = 2.25683540795462;
    a[59][7] = 2.25683540795462;
    a[59][8] = 2.25683540795462;
    a[59][9] = 2.25683540795462;
    a[59][10] = 2.25683540795462;
    a[59][11] = 2.25683540795462;
    a[59][12] = 2.25683540795462;
    a[59][13] = 2.25683540795462;
    a[59][14] = 2.25683540795462;
    a[59][15] = 2.25683540795462;
    a[59][16] = 2.25683540795462;
    a[59][17] = 2.25683540795462;
    a[59][18] = 2.25683540795462;
    a[59][19] = 2.25683540795462;
    a[59][20] = 2.25683540795462;
    a[59][21] = 2.25683540795462;
    a[59][22] = 2.25683540795462;
    a[59][23] = 2.25683540795462;
    a[59][24] = 2.25683540795462;
    a[59][25] = 2.25683540795462;
    a[59][26] = 2.25683540795462;
    a[59][27] = 2.25683540795462;
    a[59][28] = 2.25683540795462;
    a[59][29] = 2.25683540795462;
    a[59][30] = 2.25683540795462;
    a[59][31] = 2.25683540795462;
    a[59][32] = 2.25683540795462;
    a[59][33] = 2.25683540795462;
    a[59][34] = 2.25683540795462;
    a[59][35] = 2.25683540795462;
    a[59][36] = 2.25683540795462;
    a[59][37] = 2.25683540795462;
    a[59][38] = 2.25683540795462;
    a[59][39] = 2.25683540795462;
    a[59][40] = 2.25683540795462;
    a[59][41] = 2.25683540795462;
    a[59][42] = 2.25683540795462;
    a[59][43] = 2.25683540795462;
    a[59][44] = 2.25683540795462;
    a[59][45] = 2.25683540795462;
    a[59][46] = 2.25683540795462;
    a[59][47] = 2.25683540795462;
    a[59][48] = 2.25683540795462;
    a[59][49] = 2.25683540795462;
    a[59][50] = 2.25683540795462;
    a[59][51] = 2.25683540795462;
    a[59][52] = 2.25683540795462;
    a[59][53] = 2.25683540795462;
    a[59][54] = 2.25683540795462;
    a[59][55] = 2.25683540795462;
    a[59][56] = 2.25683540795462;
    a[59][57] = 2.25683540795462;
    a[59][58] = 2.25683540795462;
    a[59][59] = 2.25683540795462;
    a[59][60] = 1.11909193782874;
    a[60][0] = 2.25683292174526;
    a[60][1] = 2.25683292174526;
    a[60][2] = 2.25683292174526;
    a[60][3] = 2.25683292174526;
    a[60][4] = 2.25683292174526;
    a[60][5] = 2.25683292174526;
    a[60][6] = 2.25683292174526;
    a[60][7] = 2.25683292174526;
    a[60][8] = 2.25683292174526;
    a[60][9] = 2.25683292174526;
    a[60][10] = 2.25683292174526;
    a[60][11] = 2.25683292174526;
    a[60][12] = 2.25683292174526;
    a[60][13] = 2.25683292174526;
    a[60][14] = 2.25683292174526;
    a[60][15] = 2.25683292174526;
    a[60][16] = 2.25683292174526;
    a[60][17] = 2.25683292174526;
    a[60][18] = 2.25683292174526;
    a[60][19] = 2.25683292174526;
    a[60][20] = 2.25683292174526;
    a[60][21] = 2.25683292174526;
    a[60][22] = 2.25683292174526;
    a[60][23] = 2.25683292174526;
    a[60][24] = 2.25683292174526;
    a[60][25] = 2.25683292174526;
    a[60][26] = 2.25683292174526;
    a[60][27] = 2.25683292174526;
    a[60][28] = 2.25683292174526;
    a[60][29] = 2.25683292174526;
    a[60][30] = 2.25683292174526;
    a[60][31] = 2.25683292174526;
    a[60][32] = 2.25683292174526;
    a[60][33] = 2.25683292174526;
    a[60][34] = 2.25683292174526;
    a[60][35] = 2.25683292174526;
    a[60][36] = 2.25683292174526;
    a[60][37] = 2.25683292174526;
    a[60][38] = 2.25683292174526;
    a[60][39] = 2.25683292174526;
    a[60][40] = 2.25683292174526;
    a[60][41] = 2.25683292174526;
    a[60][42] = 2.25683292174526;
    a[60][43] = 2.25683292174526;
    a[60][44] = 2.25683292174526;
    a[60][45] = 2.25683292174526;
    a[60][46] = 2.25683292174526;
    a[60][47] = 2.25683292174526;
    a[60][48] = 2.25683292174526;
    a[60][49] = 2.25683292174526;
    a[60][50] = 2.25683292174526;
    a[60][51] = 2.25683292174526;
    a[60][52] = 2.25683292174526;
    a[60][53] = 2.25683292174526;
    a[60][54] = 2.25683292174526;
    a[60][55] = 2.25683292174526;
    a[60][56] = 2.25683292174526;
    a[60][57] = 2.25683292174526;
    a[60][58] = 2.25683292174526;
    a[60][59] = 2.25683292174526;
    a[60][60] = 2.25683292174526;
    a[60][61] = 1.11924234330456;

    if (q > maxq) {
	printf("ej: q=%d greater than maxq=%d\n", q, maxq);
	exit(1);
    }
    ej = 0.0;
    if (deriv == 0) {
	for (i = 0; i <= q + 1; i = i + 1) {
	    ej = ej + a[q][i] * cos((2.0 * (double) i + 1.0) * x);
	}
    } else if (deriv == 1) {
	for (i = 0; i <= q + 1; i = i + 1) {
	    ej = ej - a[q][i] * sin((2.0 * (double) i + 1.0) * x) * (2.0 * (double) i + 1.0);
	}
    } else {
	printf("ej: Unrecognized value of deriv=%d\n", deriv);
    }

    return (ej);
}

void deriv(double *f_dx, double *f, const double *const x, int a, int num)
{
    double dx_inv = 1.0 / (x[1] - x[0]);

    const double d1 = 0.6666666666666666666666;
    const double d2 = -0.083333333333333333333;

    const double d00 = -1.4117647058823529411764705882353;
    const double d01 = 1.7352941176470588235294117647059;
    const double d02 = -0.23529411764705882352941176470588;
    const double d03 = -0.088235294117647058823529411764706;

    const double d10 = -0.50000000000000000000000000000000;
    const double d12 = 0.50000000000000000000000000000000;

    const double d20 = 0.093023255813953488372093023255814;
    const double d21 = -0.68604651162790697674418604651163;
    const double d23 = 0.68604651162790697674418604651163;
    const double d24 = -0.093023255813953488372093023255814;

    const double d30 = 0.030612244897959183673469387755102;
    const double d32 = -0.60204081632653061224489795918367;
    const double d34 = 0.65306122448979591836734693877551;
    const double d35 = -0.081632653061224489795918367346939;

    if (a > 1 && a < num - 2) {
	f_dx[a] = (-d2 * f[a - 2] - d1 * f[a - 1] + d1 * f[a + 1] + d2 * f[a + 2]) * dx_inv;
    } else if (a == 0) {
	f_dx[a] = (d00 * f[0] + d01 * f[1] + d02 * f[2] + d03 * f[3]) * dx_inv;
    } else if (a == 1) {
	f_dx[a] = (d10 * f[0] + d12 * f[2]) * dx_inv;
    } else if (a == 2) {
	f_dx[a] = (d20 * f[0] + d21 * f[1] + d23 * f[3] + d24 * f[4]) * dx_inv;
    } else if (a == 3) {
	f_dx[a] = (d30 * f[0] + d32 * f[2] + d35 * f[5] + d34 * f[4]) * dx_inv;
    } else if (a == num - 1) {
	f_dx[a] = (d00 * f[num - 1] + d01 * f[num - 2] + d02 * f[num - 3] + d03 * f[num - 4]) * dx_inv;
    } else if (a == num - 2) {
	f_dx[a] = (d10 * f[num - 1] + d12 * f[num - 3]) * dx_inv;
    } else if (a == num - 3) {
	f_dx[a] = (d20 * f[num - 1] + d21 * f[num - 2] + d23 * f[num - 4] + d24 * f[num - 5]) * dx_inv;
    } else if (a == num - 4) {
	f_dx[a] = (d30 * f[num - 1] + d32 * f[num - 3] + d35 * f[num - 6] + d34 * f[num - 5]) * dx_inv;
    }
    // printf("deriv: a=%d num=%d dx_inv=%f f_dx=%f f=%f \n",a,num,dx_inv,f_dx[a],f[a]);

}

/*-------------------------------------------------------------------------
 *
 *  calcdeltanoRBnotrescaled   --- no red black ordering
 *
 *-------------------------------------------------------------------------*/
void calcdeltanoRBnotrescaled(double *delta, double *work, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    // const double THRESHOLD=0.000000001;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50000;
    double dx, res, rhs, xh, cosxh, deltax, jacobian, phih, pih, deltadiff, tmp;
    double totalres;
    dx = x[1] - x[0];
    // coordinate time is proper time at outer boundary:
    delta[n - 1] = 0.0;

    deltadiff = 100000.0;
    numiter = 0;
    while (deltadiff > THRESHOLD && numiter < MAXITERS) {
	for (i = 0; i < n; i++) {
	    work[i] = delta[i];
	}
	deltadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	//    separate Red and Black points:
	//       (NB: May not need to do separate Red from Black when using OpenCL)
	// for (i = 1; i < n-1; i++) {
	for (i = n - 1; i > 0; i--) {
	    xh = 0.5 * (x[i - 1] + x[i]);
	    if (i > 1 && i < n - 2) {
		phih = interpcubic(phi[i - 1], phi[i], phi[i + 1], phi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i - 1], pi[i], pi[i + 1], pi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i < n - 3) {
		phih = interpcubic(phi[i], phi[i + 1], phi[i + 2], phi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		pih = interpcubic(pi[i], pi[i + 1], pi[i + 2], pi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
	    } else if (i > 2) {
		phih = interpcubic(phi[i - 2], phi[i - 1], phi[i], phi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		pih = interpcubic(pi[i - 2], pi[i - 1], pi[i], pi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
	    } else {
		phih = 0.5 * (phi[i] + phi[i - 1]);
		pih = 0.5 * (pi[i] + pi[i - 1]);
	    }
	    deltax = (delta[i] - delta[i - 1]) / dx;
	    cosxh = cos(xh);
	    rhs = -sin(xh) * cosxh * (phih * phih + pih * pih);
	    res = deltax - rhs;
	    // jacobian: d (res) / d (delta[i])
	    jacobian = -1.0 / dx;
	    delta[i - 1] = delta[i - 1] - res / jacobian;
	    // delta[i]   = max(0.0,delta[i]);
	    deltadiff = deltadiff + pow(res / jacobian, 2);

	    // tmp = delta[i-1];
	    // delta[i-1]   = delta[i] - dx * rhs;
	    // deltadiff  = deltadiff + pow(tmp-delta[i-1],2);

	    totalres = totalres + res * res;
	}
	// Quadratic fit at origin so that delta is regular:
	// tmp = delta[0] - (4.0/3.0)*delta[1]+(1.0/3.0)*delta[2];
	// delta[0] = delta[0] - tmp;
	// deltadiff  = deltadiff + pow(tmp,2);
	//
	deltadiff = sqrt(deltadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	printf("calcdeltanoRBnotrescaled: %d ||delta_new-delta_old||_2 = %e ||res||=%e \n", numiter, deltadiff, totalres);
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcdeltanoRBnotrescaledRK4pq    ---uses the pqsource field which has already been interpolated
 *
 *-------------------------------------------------------------------------*/
void calcdeltanoRBnotrescaledRK4pq(double *delta, const double *sourcefctint, const double *sourcefctcop, const double *const x, int n)
{
    int i, numiter;
    // const double THRESHOLD=0.000000001;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50001;
    double dx, res, rhs, xh, cosxh, deltax, jacobian, phih, pih, deltadiff, tmp;
    double cosxj, sinxj, cosxjh, sinxjh, cosxjp1, sinxjp1, Sj, Sjh, Sjp1;
    double ft, k1, ftphh_yphk1, ftphh_yphk2, k3, ftph_yk3, k4, olddelta, metricadiff;
    double totalres, h, k2;
    dx = x[1] - x[0];
    // coordinate time is proper time at outer boundary:
    delta[n - 1] = 0.0;

    deltadiff = 100000.0;
    numiter = 0;
    while (deltadiff > THRESHOLD && numiter < MAXITERS) {
	deltadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	for (i = n - 2; i >= 0; i--) {
	    xh = 0.5 * (x[i] + x[i + 1]);
	    h = -dx;
	    cosxj = cos(x[i]);
	    sinxj = sin(x[i]);
	    cosxjh = cos(xh);
	    sinxjh = sin(xh);
	    cosxjp1 = cos(x[i + 1]);
	    sinxjp1 = sin(x[i + 1]);
	    // Sj     = phi[i]  *phi[i]   + pi[i]*pi[i];
	    // Sjh    = phih    *phih     + pih*pih;
	    // Sjp1   = phi[i+1]*phi[i+1] + pi[i+1]*pi[i+1];
	    Sj = sourcefctcop[i];
	    Sjh = sourcefctint[i];
	    Sjp1 = sourcefctcop[i + 1];
	    // f(t,y):
	    ft = -sinxjp1 * cosxjp1 * Sjp1;
	    k1 = -dx * ft;
	    // f(t+h/2, y+k1/2):
	    ftphh_yphk1 = -sinxjh * cosxjh * Sjh;
	    k2 = -dx * ftphh_yphk1;
	    //printf("ak1hat=%e ftphh_yphk1=%e \n",ak1hat,ftphh_yphk1);
	    // f(t+h/2, y+k2/2):
	    ftphh_yphk2 = -sinxjh * cosxjh * Sjh;
	    k3 = -dx * ftphh_yphk2;
	    // f(t+h, y+k3):
	    ftph_yk3 = -sinxj * cosxj * Sj;
	    k4 = -dx * ftph_yk3;
	    //
	    //
	    olddelta = delta[i];
	    delta[i] = delta[i + 1] + (1.0 / 6.0) * (k1 + 2.0 * k2 + 2.0 * k3 + k4);
	    metricadiff = metricadiff + pow(olddelta - delta[i], 2);
	    totalres = totalres + res * res;
	    totalres = metricadiff;
	    // printf("i=%d: delta: old:%f new:%f\n",i,olddelta,delta[i]);
	}
	// Quadratic fit at origin so that delta is regular:
	// tmp = delta[0] - (4.0/3.0)*delta[1]+(1.0/3.0)*delta[2];
	// delta[0] = delta[0] - tmp;
	// deltadiff  = deltadiff + pow(tmp,2);
	//
	deltadiff = sqrt(deltadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	// printf("calcdeltanoRBnotrescaledRK4: %d ||delta_new-delta_old||_2 = %e ||res||=%e \n",numiter,deltadiff,totalres);
	// printf("calcdeltanoRBnotrescaledRK4:: ||delta||_2   = %f \n",L2Norm(delta,n));
	// printf("calcdeltanoRBnotrescaledRK4:: ||qq||_2      = %f \n",L2Norm(phi,n));
	// printf("calcdeltanoRBnotrescaledRK4:: ||pp||_2      = %f \n",L2Norm(pi,n));
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcdeltanoRBnotrescaledRK4
 *
 *-------------------------------------------------------------------------*/
void calcdeltanoRBnotrescaledRK4(double *delta, double *work, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    // const double THRESHOLD=0.000000001;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 50001;
    double dx, res, rhs, xh, cosxh, deltax, jacobian, phih, pih, deltadiff, tmp;
    double cosxj, sinxj, cosxjh, sinxjh, cosxjp1, sinxjp1, Sj, Sjh, Sjp1;
    double ft, k1, ftphh_yphk1, ftphh_yphk2, k3, ftph_yk3, k4, olddelta, metricadiff;
    double totalres, h, k2;
    dx = x[1] - x[0];
    // coordinate time is proper time at outer boundary:
    delta[n - 1] = 0.0;

    deltadiff = 100000.0;
    numiter = 0;
    while (deltadiff > THRESHOLD && numiter < MAXITERS) {
	// for (i = 0; i < n; i++) {
	// work[i] = delta[i];
	// }
	deltadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	for (i = n - 2; i >= 0; i--) {
	    xh = 0.5 * (x[i] + x[i + 1]);
	    h = -dx;
	    if (i > 0 && i < n - 2) {
		phih = interpcubic(phi[i - 1], phi[i], phi[i + 1], phi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i - 1], pi[i], pi[i + 1], pi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i == 0) {
		// use symmetry properties
		phih = interpcubic(-phi[i + 1], phi[i], phi[i + 1], phi[i + 2], xh, -x[i + 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i + 1], pi[i], pi[i + 1], pi[i + 2], xh, -x[i + 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i < n - 3) {
		phih = interpcubic(phi[i], phi[i + 1], phi[i + 2], phi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		pih = interpcubic(pi[i], pi[i + 1], pi[i + 2], pi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
	    } else if (i > 2) {
		phih = interpcubic(phi[i - 2], phi[i - 1], phi[i], phi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		pih = interpcubic(pi[i - 2], pi[i - 1], pi[i], pi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
	    }
	    cosxj = cos(x[i]);
	    sinxj = sin(x[i]);
	    cosxjh = cos(xh);
	    sinxjh = sin(xh);
	    cosxjp1 = cos(x[i + 1]);
	    sinxjp1 = sin(x[i + 1]);
	    Sj = phi[i] * phi[i] + pi[i] * pi[i];
	    Sjh = phih * phih + pih * pih;
	    Sjp1 = phi[i + 1] * phi[i + 1] + pi[i + 1] * pi[i + 1];
	    // f(t,y):
	    ft = -sinxjp1 * cosxjp1 * Sjp1;
	    k1 = -dx * ft;
	    // f(t+h/2, y+k1/2):
	    ftphh_yphk1 = -sinxjh * cosxjh * Sjh;
	    k2 = -dx * ftphh_yphk1;
	    //printf("ak1hat=%e ftphh_yphk1=%e \n",ak1hat,ftphh_yphk1);
	    // f(t+h/2, y+k2/2):
	    ftphh_yphk2 = -sinxjh * cosxjh * Sjh;
	    k3 = -dx * ftphh_yphk2;
	    // f(t+h, y+k3):
	    ftph_yk3 = -sinxj * cosxj * Sj;
	    k4 = -dx * ftph_yk3;
	    //
	    //
	    olddelta = delta[i];
	    delta[i] = delta[i + 1] + (1.0 / 6.0) * (k1 + 2.0 * k2 + 2.0 * k3 + k4);
	    metricadiff = metricadiff + pow(olddelta - delta[i], 2);
	    totalres = totalres + res * res;
	    totalres = metricadiff;
	    // printf("i=%d: delta: old:%f new:%f\n",i,olddelta,delta[i]);
	}
	// Quadratic fit at origin so that delta is regular:
	// tmp = delta[0] - (4.0/3.0)*delta[1]+(1.0/3.0)*delta[2];
	// delta[0] = delta[0] - tmp;
	// deltadiff  = deltadiff + pow(tmp,2);
	//
	deltadiff = sqrt(deltadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	// printf("calcdeltanoRBnotrescaledRK4: %d ||delta_new-delta_old||_2 = %e ||res||=%e \n",numiter,deltadiff,totalres);
	// printf("calcdeltanoRBnotrescaledRK4:: ||delta||_2   = %f \n",L2Norm(delta,n));
	// printf("calcdeltanoRBnotrescaledRK4:: ||qq||_2      = %f \n",L2Norm(phi,n));
	// printf("calcdeltanoRBnotrescaledRK4:: ||pp||_2      = %f \n",L2Norm(pi,n));
    }

}

/*-------------------------------------------------------------------------
 *
 *  calcmetricanoRB ---no Red-Black ordering, but tight stencil
 *
 *-------------------------------------------------------------------------*/
void calcmetricanoRBnotrescaled(double *metrica, double *work, const double *const phi, const double *const pi, const double *const x, int n)
{
    int i, numiter;
    const double THRESHOLD = 0.0000000000000001;
    const int MAXITERS = 1;
    const int MINITERS = 1;
    double dx, res, rhs, sinx, cosx, metricax, jacobian, phih, pih, metricadiff, tmp;
    double J, K, metricah, totalres, source;
    double xh, sinxj, sinxjh, sinxjp1, cosxj, cosxjh, cosxjp1, ajhat, Sj, Sjh, Sjp1, aj;
    double ft, k1, ftphh_yphk1, ak1hat, k2, ak2hat, ftph_yk3, k4, ftphh_yphk2;
    double k3, ak3hat, ajp1hat, oldmetrica;
    dx = x[1] - x[0];
    // enforce a regular origin:
    metrica[0] = 1.0;

    metricadiff = 100000.0;
    numiter = 0;
    while ((metricadiff > THRESHOLD || numiter < MINITERS) && numiter < MAXITERS) {
	// for (i = 0; i < n; i++) {
	// work[i] = metrica[i];
	// }
	metricadiff = 0.0;
	totalres = 0.0;
	numiter++;
	// loop over interior points
	for (i = 0; i < n - 1; i++) {
	    xh = 0.5 * (x[i] + x[i + 1]);
	    if (i > 0 && i < n - 2) {
		phih = interpcubic(phi[i - 1], phi[i], phi[i + 1], phi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i - 1], pi[i], pi[i + 1], pi[i + 2], xh, x[i - 1], x[i], x[i + 1], x[i + 2]);
		//metricah = interpcubic(work[i-1],work[i],work[i+1],work[i+2],xh,x[i-1],x[i],x[i+1],x[i+2]);
		//metricax = (1.0*work[i-2]-27.0*work[i-1]+27.0*work[i]-1.0*work[i+1])/(24.0*dx);
	    } else if (i == 0) {
		// use symmetry properties
		phih = interpcubic(-phi[i + 1], phi[i], phi[i + 1], phi[i + 2], xh, -x[i + 1], x[i], x[i + 1], x[i + 2]);
		pih = interpcubic(pi[i + 1], pi[i], pi[i + 1], pi[i + 2], xh, -x[i + 1], x[i], x[i + 1], x[i + 2]);
	    } else if (i < n - 3) {
		phih = interpcubic(phi[i], phi[i + 1], phi[i + 2], phi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		pih = interpcubic(pi[i], pi[i + 1], pi[i + 2], pi[i + 3], xh, x[i], x[i + 1], x[i + 2], x[i + 3]);
		//metricah = interpcubic(work[i],work[i+1],work[i+2],work[i+3],xh,x[i],x[i+1],x[i+2],x[i+3]);
		// use metrica[0]=1:
		//metricax = (1.0*1.0      -27.0*work[i-1]+27.0*work[i]-1.0*work[i+1])/(24.0*dx);
	    } else if (i > 2) {
		phih = interpcubic(phi[i - 2], phi[i - 1], phi[i], phi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		pih = interpcubic(pi[i - 2], pi[i - 1], pi[i], pi[i + 1], xh, x[i - 2], x[i - 1], x[i], x[i + 1]);
		//metricah = interpcubic(work[i-2],work[i-1],work[i],work[i+1],xh,x[i-2],x[i-1],x[i],x[i+1]);
		// use metrica[N-1]=1:
		//metricax = (1.0*work[i-2]-27.0*work[i-1]+27.0*work[i]-1.0*1.0      )/(24.0*dx);
	    }
	    aj = metrica[i];
	    cosxj = cos(x[i]);
	    sinxj = sin(x[i]);
	    cosxjh = cos(xh);
	    sinxjh = sin(xh);
	    cosxjp1 = cos(x[i + 1]);
	    sinxjp1 = sin(x[i + 1]);
	    Sj = phi[i] * phi[i] + pi[i] * pi[i];
	    Sjh = phih * phih + pih * pih;
	    Sjp1 = phi[i + 1] * phi[i + 1] + pi[i + 1] * pi[i + 1];
	    // f(t,y):
	    ajhat = 1.0 - aj;
	    ft = -(1.0 + 2.0 * sinxj * sinxj) / sinxj / cosxj * ajhat + sinxj * cosxj * (1.0 - ajhat) * Sj;
	    if (x[i] < 0.0000000001)
		ft = 0.0;
	    //printf("ajhat=%e ft=%e \n",ajhat,ft);
	    k1 = dx * ft;
	    // f(t+h/2, y+k1/2):
	    ak1hat = ajhat + 0.5 * k1;
	    ak1hat = min(1.0, ak1hat);
	    ak1hat = max(0.0, ak1hat);
	    ftphh_yphk1 = -(1.0 + 2.0 * sinxjh * sinxjh) / sinxjh / cosxjh * ak1hat + sinxjh * cosxjh * (1.0 - ak1hat) * Sjh;
	    k2 = dx * ftphh_yphk1;
	    //printf("ak1hat=%e ftphh_yphk1=%e \n",ak1hat,ftphh_yphk1);
	    // f(t+h/2, y+k2/2):
	    ak2hat = ajhat + 0.5 * k2;
	    ak2hat = min(1.0, ak2hat);
	    ak2hat = max(0.0, ak2hat);
	    ftphh_yphk2 = -(1.0 + 2.0 * sinxjh * sinxjh) / sinxjh / cosxjh * ak2hat + sinxjh * cosxjh * (1.0 - ak2hat) * Sjh;
	    k3 = dx * ftphh_yphk2;
	    //printf("ak2hat=%e ftphh_yphk2=%e k3=%e\n",ak2hat,ftphh_yphk2,k3);
	    // f(t+h, y+k3):
	    ak3hat = ajhat + 1.0 * k3;
	    ak3hat = min(1.0, ak3hat);
	    ak3hat = max(0.0, ak3hat);
	    ftph_yk3 = -(1.0 + 2.0 * sinxjp1 * sinxjp1) / sinxjp1 / cosxjp1 * ak3hat + sinxjp1 * cosxjp1 * (1.0 - ak3hat) * Sjp1;
	    k4 = dx * ftph_yk3;
	    if (abs(cosxjp1) < 0.0000000001)
		ftph_yk3 = 0.0;
	    //
	    //
	    ajp1hat = ajhat + (1.0 / 6.0) * (k1 + 2.0 * k2 + 2.0 * k3 + k4);
	    oldmetrica = metrica[i + 1];
	    metrica[i + 1] = 1.0 - ajp1hat;
	    metrica[i + 1] = min(1.0, metrica[i + 1]);
	    metrica[i + 1] = max(0.0, metrica[i + 1]);
	    metricadiff = metricadiff + pow(oldmetrica - metrica[i + 1], 2);
	    totalres = totalres + res * res;
	    //printf("calcmetricanoRB: i=%d %e/%e/%e metrica[i+1] = %e \n",i,ak1hat,ak2hat,ak3hat,metrica[i+1]);
	    // printf("calcmetricanoRB: S=%e/%e/%e  ft=%e/%e/%e \n",Sj,Sjh,Sjp1,ft,ftphh_yphk1,ftphh_yphk2,ftph_yk3);
	    // printf("calcmetricanoRB: cos=%e/%e/%e sin=%e/%e/%e \n",cosxj,cosxjh,cosxjp1,sinxj,sinxjh,sinxjp1);
	}
	// At outer boundary, A->1:
	tmp = metrica[n - 1] - 1.0;
	metrica[n - 1] = 1.0;
	metricadiff = metricadiff + pow(tmp, 2);
	//
	metricadiff = sqrt(metricadiff / (n - 1));
	totalres = sqrt(totalres / (n - 1));
	// printf("calcmetricanoRB: %d ||metrica_new-metrica_old||_2 = %e |res|=%e\n",numiter,metricadiff,totalres);
	// printf("calcmetricanoRB::: ||metrica||_2 = %f \n",L2Norm(metrica,n));
	// printf("calcmetricanoRB::: ||qq||_2      = %f \n",L2Norm(phi,n));
	// printf("calcmetricanoRB::: ||pp||_2      = %f \n",L2Norm(pi,n));
    }

}
