#define Epsilon 00.010		/* Amplitude of BR initial data */
#define Sigma   0.0625		/* Width of BR initiial pulse (0.0625) */
#define N  4096			/* Radial grid points */
#define Fr  12800		/* Dump frequency */
#define T   100.00040001	/* Stop timer */
#define CFL 0.30		/* CFL condition dt/dx */
#define INITDATA 2		/* If ==0 then BR'11 Gaussian initial data */
 			        /*    ==1 equal energy in modes 0 and 1    */
			        /*    ==2 then approximately QP            */
			        /*    ==3 Stephen's explicit QP            */
#define ALPHA     1.0		/* slope of QP ID */
#define KDOMINANT 2.5		/* dominant mode of QP */
#define PUREADS 0		/* If ==0 then metric is dynamic           */
			        /*    ==1 then metric is fixed A=1 Delta=0 */
			        /*    ==2 then delta  is fixed at initial  */
#define MINABHTHRESH 0.01	/* when min|A| reaches this value, declare BH formation */

       /* do not change:    */
#define MOPI_DELTAT 1.000000000 /* time period over which to compute max mopi */
#define INTAFREQ 9999999	/* how often to solve for metrica */
#define DECOMPOSEFREQ 1		/* how often to decompose data (in terms of FR */
				/*    ==1 means everytime SDF is output */
#define EPSDIS 0.090		/* amount of dissipation */
#define M 1			/* Number of fields */
#define Xmax 1.570796326794895	/* Outer radial boundary */
#define Xmin 0.0		/* Inner radial boundary...origin */
#define Pi 3.14159265358979	/* constant...do not change */

       /* not used anymore: */
#define BCWIDTH 1		/* how many points is the boundary applied to (>=1) */
#define BCTYPE  0		/* 0==>fixed boundaries */
			        /* 1==>periodic BCs     */
#define RKORDER 3		/* 2 for second order; 3 for third order, 4 for fourth, etc */

#define idx(b,a) (N*(b)+(a))
