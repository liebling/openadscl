runme: *.c *.h *.cl
	g++  -DSDF -O2 -I/opt/AMDAPPSDK-3.0-0-Beta/include -L/opt/AMDAPPSDK-3.0-0-Beta/lib/x86_64 *.c -o runme -lOpenCL -lm -L/usr/local/lib -lbbhutil

runmenosdf: *.c
	g++  -O2 -I/opt/AMDAPPSDK-3.0-0-Beta/include -L/opt/AMDAPPSDK-3.0-0-Beta/lib/x86_64 *.c -o runme -lOpenCL -lm -L/usr/local/lib 

runmemac: *.c
	g++ -DMAC -O3 -framework OpenCL *.c -o runme

irunme: *.c
	grep clGetDeviceIDs body.c
	icpc -O2 -I/opt/intel/opencl-sdk/include     -L/opt/intel/opencl-sdk/lib            *.c -o irunme -lOpenCL -lm -L/usr/local/lib -lbbhutil

timings: runmenosdf
	/usr/bin/time runme

clean:
	/bin/rm *.o runme

