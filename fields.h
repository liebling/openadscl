/* fields */

/* Note: #define VAR_DECLS 1 before including 
this file to DECLARE and INITIALIZE global 
variables.  Include this file without defining 
VAR_DECLS to extern these variables.
*/

//Make sure this is loaded only once per file
#ifndef VAR_DEFS
#define VAR_DEFS 1

/*----------------------------------------------
Setup variable declaration macros.
----------------------------------------------*/
#ifndef VAR_DECLS
# define _DECL extern
#else
# define _DECL
#endif

/* fields begin below */

// Just auxilliary fields for knowing what's going on:
//     (they're not needed for a proper evolution)
_DECL double dmdr[M * N];
_DECL double momc[M * N];
_DECL double hamc[M * N];
// The Main Evolution fields:
_DECL double qq[M * N];
_DECL double pp[M * N];
_DECL double ph[M * N];
_DECL double metrica[M * N];
// Not evolved, but spatially integrated (and necessary):
_DECL double delta[M * N];
// Derivatives of the fields
_DECL double qq_dx[M * N];
_DECL double pp_dx[M * N];
_DECL double ph_dx[M * N];
_DECL double metrica_dx[M * N];
_DECL double delta_dx[M * N];
// Auxiliary fields for RK:
//     (Notes:    
//             1. Should be able to find a lower storage scheme w/ which to implement RK4
//             2. We should never need these fields on the host, only on the device,
//                and so we should, at the least, be able to avoid allocating them here
//            )
_DECL double qq_k1[M * N];
_DECL double pp_k1[M * N];
_DECL double ph_k1[M * N];
_DECL double metrica_k1[M * N];
//
_DECL double qq_k2[M * N];
_DECL double pp_k2[M * N];
_DECL double ph_k2[M * N];
_DECL double metrica_k2[M * N];
//
_DECL double qq_k3[M * N];
_DECL double pp_k3[M * N];
_DECL double ph_k3[M * N];
_DECL double metrica_k3[M * N];
//
_DECL double qq_k4[M * N];
_DECL double pp_k4[M * N];
_DECL double ph_k4[M * N];
_DECL double metrica_k4[M * N];
// Fields on the advanced time step:
_DECL double qq_np1[M * N];
_DECL double pp_np1[M * N];
_DECL double ph_np1[M * N];
_DECL double metrica_np1[M * N];
_DECL double delta_np1[M * N];
// Auxiliary fields to store the time-derivative for each evolved field:
_DECL double rhs_qq[M * N];
_DECL double rhs_pp[M * N];
_DECL double rhs_ph[M * N];
_DECL double rhs_metrica[M * N];
_DECL double aux_pi[M * N];
_DECL double aux_pi_dx[M * N];
_DECL double pqsourceint[M * N];
_DECL double pqsourcecop[M * N];
// Auxiliary fields to store the dissipation     for each evolved field:
_DECL double diss_qq[M * N];
_DECL double diss_pp[M * N];
_DECL double diss_ph[M * N];
_DECL double diss_metrica[M * N];
_DECL double diss_aux_pi[M * N];

#endif
