#pragma OPENCL EXTENSION cl_khr_fp64: enable
//#pragma OPENCL EXTENSION cl_amd_fp64: enable
#include "parm.h"

double kernel_interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4);

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute derivatives of fields   ----------- */
/* -------------------------------------------------- */
/* ---NB: translated from massads ------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_derivs1(__global double *qq_dx, __global double *pp_dx, __global double *ph_dx, __global double *metrica_dx, __global double *delta_dx,
	       __global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dx_in,
	       __global double *aux_pi, __global double *aux_pi_dx)
{
    int b;
    int a;

    double dx_inv = 1.0 / (*dx_in);

    const double d1 = 0.6666666666666666666666;
    const double d2 = -0.083333333333333333333;

    const double d00 = -1.4117647058823529411764705882353;
    const double d01 = 1.7352941176470588235294117647059;
    const double d02 = -0.23529411764705882352941176470588;
    const double d03 = -0.088235294117647058823529411764706;

    const double d10 = -0.50000000000000000000000000000000;
    const double d12 = 0.50000000000000000000000000000000;

    const double d20 = 0.093023255813953488372093023255814;
    const double d21 = -0.68604651162790697674418604651163;
    const double d23 = 0.68604651162790697674418604651163;
    const double d24 = -0.093023255813953488372093023255814;

    const double d30 = 0.030612244897959183673469387755102;
    const double d32 = -0.60204081632653061224489795918367;
    const double d34 = 0.65306122448979591836734693877551;
    const double d35 = -0.081632653061224489795918367346939;

    b = get_global_id(0);
    a = get_global_id(1);

    if (a > 1 && a < N - 2) {
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] + d2 * pp[idx(b, a + 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] + d2 * ph[idx(b, a + 2)]) * dx_inv;
	metrica_dx[idx(b, a)] =
	    (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] + d2 * metrica[idx(b, a + 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] + d2 * delta[idx(b, a + 2)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] + d1 * aux_pi[idx(b, a + 1)] + d2 * aux_pi[idx(b, a + 2)]) * dx_inv;
    } else if (a == 1) {
	// use symmetry properties:
	// Odd
	qq_dx[idx(b, a)] = (+d2 * qq[idx(b, a)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	// Even:
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] + d2 * pp[idx(b, a + 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] + d2 * ph[idx(b, a + 2)]) * dx_inv;
	metrica_dx[idx(b, a)] = (-d2 * metrica[idx(b, a)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] + d2 * metrica[idx(b, a + 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] + d2 * delta[idx(b, a + 2)]) * dx_inv;
	// Use quadratic fit for aux_pi[0]:
	aux_pi_dx[idx(b, a)] =
	    (-d2 * aux_pi[idx(b, a)] - d1 * (1.5 * aux_pi[idx(b, 1)] - 0.6 * aux_pi[idx(b, 2)] + 0.1 * aux_pi[idx(b, 3)]) + d1 * aux_pi[idx(b, a + 1)] +
	     d2 * aux_pi[idx(b, a + 2)]) * dx_inv;
    } else if (a == 0) {
	// use symmetry properties:
	// Odd
	qq_dx[idx(b, a)] = (+d2 * qq[idx(b, a + 2)] + d1 * qq[idx(b, a + 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	// Even:
	pp_dx[idx(b, a)] = 0.0;
	ph_dx[idx(b, a)] = 0.0;
	metrica_dx[idx(b, a)] = 0.0;
	delta_dx[idx(b, a)] = 0.0;
	aux_pi_dx[idx(b, a)] = 0.0;
    } else if (a == N - 2) {
	// use symmetry properties:
	// Even:
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a)]) * dx_inv;
	// Odd
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] - d2 * pp[idx(b, a)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] - d2 * ph[idx(b, a)]) * dx_inv;
	metrica_dx[idx(b, a)] = (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] - d2 * metrica[idx(b, a)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] - d2 * delta[idx(b, a)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] + d1 * aux_pi[idx(b, a + 1)] - d2 * aux_pi[idx(b, a)]) * dx_inv;
    } else if (a == N - 1) {
	// use symmetry properties:
	// Even:
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a - 1)] + d2 * qq[idx(b, a - 2)]) * dx_inv;
	// Odd
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] - d1 * pp[idx(b, a - 1)] - d2 * pp[idx(b, a - 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] - d1 * ph[idx(b, a - 1)] - d2 * ph[idx(b, a - 2)]) * dx_inv;
	metrica_dx[idx(b, a)] =
	    (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] - d1 * metrica[idx(b, a - 1)] - d2 * metrica[idx(b, a - 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] - d1 * delta[idx(b, a - 1)] - d2 * delta[idx(b, a - 2)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] - d1 * aux_pi[idx(b, a - 1)] - d2 * aux_pi[idx(b, a - 2)]) * dx_inv;
    }


}				// end kernel_derivs1

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute derivatives of fields   ----------- */
/* -------------------------------------------------- */
/* ---NB: Same as derivs1 except for arguments   ---- */
/* ---    Instead, uses advanced time for fields ---- */
/* ---NB: translated from massads ------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_derivs2(__global double *qq_dx, __global double *pp_dx, __global double *ph_dx, __global double *metrica_dx, __global double *delta_dx,
	       __global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dx_in,
	       __global double *aux_pi, __global double *aux_pi_dx)
{
    int b;
    int a;

    double dx_inv = 1.0 / (*dx_in);

    const double d1 = 0.6666666666666666666666;
    const double d2 = -0.083333333333333333333;

    const double d00 = -1.4117647058823529411764705882353;
    const double d01 = 1.7352941176470588235294117647059;
    const double d02 = -0.23529411764705882352941176470588;
    const double d03 = -0.088235294117647058823529411764706;

    const double d10 = -0.50000000000000000000000000000000;
    const double d12 = 0.50000000000000000000000000000000;

    const double d20 = 0.093023255813953488372093023255814;
    const double d21 = -0.68604651162790697674418604651163;
    const double d23 = 0.68604651162790697674418604651163;
    const double d24 = -0.093023255813953488372093023255814;

    const double d30 = 0.030612244897959183673469387755102;
    const double d32 = -0.60204081632653061224489795918367;
    const double d34 = 0.65306122448979591836734693877551;
    const double d35 = -0.081632653061224489795918367346939;

    b = get_global_id(0);
    a = get_global_id(1);

    if (a > 1 && a < N - 2) {
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] + d2 * pp[idx(b, a + 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] + d2 * ph[idx(b, a + 2)]) * dx_inv;
	metrica_dx[idx(b, a)] =
	    (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] + d2 * metrica[idx(b, a + 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] + d2 * delta[idx(b, a + 2)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] + d1 * aux_pi[idx(b, a + 1)] + d2 * aux_pi[idx(b, a + 2)]) * dx_inv;
    } else if (a == 1) {
	// use symmetry properties:
	// Odd
	qq_dx[idx(b, a)] = (+d2 * qq[idx(b, a)] + 0.0 + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	// Even:
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] + d2 * pp[idx(b, a + 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] + d2 * ph[idx(b, a + 2)]) * dx_inv;
	metrica_dx[idx(b, a)] = (-d2 * metrica[idx(b, a)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] + d2 * metrica[idx(b, a + 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] + d2 * delta[idx(b, a + 2)]) * dx_inv;
	// Use quadratic fit for aux_pi[0]:
	aux_pi_dx[idx(b, a)] =
	    (-d2 * aux_pi[idx(b, a)] - d1 * (1.5 * aux_pi[idx(b, 1)] - 0.6 * aux_pi[idx(b, 2)] + 0.1 * aux_pi[idx(b, 3)]) + d1 * aux_pi[idx(b, a + 1)] +
	     d2 * aux_pi[idx(b, a + 2)]) * dx_inv;
    } else if (a == 0) {
	// use symmetry properties:
	// Odd
	qq_dx[idx(b, a)] = (+d2 * qq[idx(b, a + 2)] + d1 * qq[idx(b, a + 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a + 2)]) * dx_inv;
	// Even:
	pp_dx[idx(b, a)] = 0.0;
	ph_dx[idx(b, a)] = 0.0;
	metrica_dx[idx(b, a)] = 0.0;
	delta_dx[idx(b, a)] = 0.0;
	aux_pi_dx[idx(b, a)] = 0.0;
    } else if (a == N - 2) {
	// use symmetry properties:
	// Even:
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a + 1)] + d2 * qq[idx(b, a)]) * dx_inv;
	// Odd
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] + d1 * pp[idx(b, a + 1)] - d2 * pp[idx(b, a)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] + d1 * ph[idx(b, a + 1)] - d2 * ph[idx(b, a)]) * dx_inv;
	metrica_dx[idx(b, a)] = (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] + d1 * metrica[idx(b, a + 1)] - d2 * metrica[idx(b, a)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] + d1 * delta[idx(b, a + 1)] - d2 * delta[idx(b, a)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] + d1 * aux_pi[idx(b, a + 1)] - d2 * aux_pi[idx(b, a)]) * dx_inv;
    } else if (a == N - 1) {
	// use symmetry properties:
	// Even:
	qq_dx[idx(b, a)] = (-d2 * qq[idx(b, a - 2)] - d1 * qq[idx(b, a - 1)] + d1 * qq[idx(b, a - 1)] + d2 * qq[idx(b, a - 2)]) * dx_inv;
	// Odd
	pp_dx[idx(b, a)] = (-d2 * pp[idx(b, a - 2)] - d1 * pp[idx(b, a - 1)] - d1 * pp[idx(b, a - 1)] - d2 * pp[idx(b, a - 2)]) * dx_inv;
	ph_dx[idx(b, a)] = (-d2 * ph[idx(b, a - 2)] - d1 * ph[idx(b, a - 1)] - d1 * ph[idx(b, a - 1)] - d2 * ph[idx(b, a - 2)]) * dx_inv;
	metrica_dx[idx(b, a)] =
	    (-d2 * metrica[idx(b, a - 2)] - d1 * metrica[idx(b, a - 1)] - d1 * metrica[idx(b, a - 1)] - d2 * metrica[idx(b, a - 2)]) * dx_inv;
	delta_dx[idx(b, a)] = (-d2 * delta[idx(b, a - 2)] - d1 * delta[idx(b, a - 1)] - d1 * delta[idx(b, a - 1)] - d2 * delta[idx(b, a - 2)]) * dx_inv;
	aux_pi_dx[idx(b, a)] = (-d2 * aux_pi[idx(b, a - 2)] - d1 * aux_pi[idx(b, a - 1)] - d1 * aux_pi[idx(b, a - 1)] - d2 * aux_pi[idx(b, a - 2)]) * dx_inv;
    }

}				// end kernel_derivs2


/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute dissipation for each evolved field- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------h^5 partial^6 phi  ------------------------ */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_calcdiss(__global double *diss_qq, __global double *diss_pp, __global double *diss_ph, __global double *diss_metrica, __global double *diss_aux_pi,
		__global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *aux_pi, __global double *dx_in)
{
    int b;
    int a;

    double dx_inv = 1.0 / (*dx_in);


    b = get_global_id(0);
    a = get_global_id(1);

    int myi = idx(b, a);

    if (a >= 3 && a < N - 3) {
	diss_qq[myi] = qq[myi + 3] + qq[myi - 3] - 6.0 * (qq[myi + 2] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	diss_pp[myi] = pp[myi + 3] + pp[myi - 3] - 6.0 * (pp[myi + 2] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi - 3] - 6.0 * (ph[myi + 2] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi - 3] - 6.0 * (metrica[myi + 2] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi - 3] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 2) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi - 1] - 6.0 * (qq[myi + 2] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi - 1] - 6.0 * (pp[myi + 2] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi - 1] - 6.0 * (ph[myi + 2] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi - 1] - 6.0 * (metrica[myi + 2] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi - 1] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 1) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi + 1] - 6.0 * (qq[myi + 2] - qq[myi]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi + 1] - 6.0 * (pp[myi + 2] + pp[myi]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi + 1] - 6.0 * (ph[myi + 2] + ph[myi]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi + 1] - 6.0 * (metrica[myi + 2] + metrica[myi]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) - 20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi + 1] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 0) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi + 3] - 6.0 * (qq[myi + 2] - qq[myi + 2]) + 15.0 * (qq[myi + 1] - qq[myi + 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi + 3] - 6.0 * (pp[myi + 2] + pp[myi + 2]) + 15.0 * (pp[myi + 1] + pp[myi + 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi + 3] - 6.0 * (ph[myi + 2] + ph[myi + 2]) + 15.0 * (ph[myi + 1] + ph[myi + 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi + 3] - 6.0 * (metrica[myi + 2] + metrica[myi + 2]) + 15.0 * (metrica[myi + 1] + metrica[myi + 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi + 3] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi + 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi + 1]) - 20.0 * aux_pi[myi];
	// these cause problems and so I've turned them off:
    } else if (a == N - 1 && 0) {
	// Even:
	diss_qq[myi] = qq[myi - 3] + qq[myi - 3] - 6.0 * (qq[myi - 2] + qq[myi - 2]) + 15.0 * (qq[myi - 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// Odd
	diss_pp[myi] = -pp[myi - 3] + pp[myi - 3] + 6.0 * (pp[myi - 2] + pp[myi - 2]) - 15.0 * (pp[myi - 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = -ph[myi - 3] + ph[myi - 3] + 6.0 * (ph[myi - 2] + ph[myi - 2]) - 15.0 * (ph[myi - 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    -metrica[myi - 3] + metrica[myi - 3] + 6.0 * (metrica[myi - 2] + metrica[myi - 2]) - 15.0 * (metrica[myi - 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    -aux_pi[myi - 3] + aux_pi[myi - 3] + 6.0 * (aux_pi[myi - 2] + aux_pi[myi - 2]) - 15.0 * (aux_pi[myi - 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == N - 2 && 0) {
	// Even:
	diss_qq[myi] = qq[myi - 1] + qq[myi - 3] - 6.0 * (qq[myi] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// Odd
	diss_pp[myi] = -pp[myi - 1] + pp[myi - 3] + 6.0 * (pp[myi] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = -ph[myi - 1] + ph[myi - 3] + 6.0 * (ph[myi] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    -metrica[myi - 1] + metrica[myi - 3] + 6.0 * (metrica[myi] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) - 20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    -aux_pi[myi - 1] + aux_pi[myi - 3] + 6.0 * (aux_pi[myi] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == N - 3 && 0) {
	// Even:
	diss_qq[myi] = qq[myi + 1] + qq[myi - 3] - 6.0 * (qq[myi + 2] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// Odd
	diss_pp[myi] = -pp[myi + 1] + pp[myi - 3] - 6.0 * (pp[myi + 2] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = -ph[myi + 1] + ph[myi - 3] - 6.0 * (ph[myi + 2] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    -metrica[myi + 1] + metrica[myi - 3] - 6.0 * (metrica[myi + 2] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    -aux_pi[myi + 1] + aux_pi[myi - 3] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else {
	diss_qq[myi] = 0.0;
	diss_pp[myi] = 0.0;
	diss_ph[myi] = 0.0;
	diss_metrica[myi] = 0.0;
	diss_aux_pi[myi] = 0.0;
    }

    diss_qq[myi] = diss_qq[myi] * dx_inv;
    diss_pp[myi] = diss_pp[myi] * dx_inv;
    diss_ph[myi] = diss_ph[myi] * dx_inv;
    diss_metrica[myi] = diss_metrica[myi] * dx_inv;
    diss_aux_pi[myi] = diss_aux_pi[myi] * dx_inv;

}				// end kernel_calcdiss

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute dissipation for each evolved field- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------h^5 partial^6 phi  ------------------------ */
/* -------------------------------------------------- */
/* ---NB: only difference w/r/t calcdiss is that----- */
/* ---    this routine takes the _n data.       ----- */
/* -------------------------------------------------- */
__kernel void
kernel_calcdissn(__global double *diss_qq, __global double *diss_pp, __global double *diss_ph, __global double *diss_metrica, __global double *diss_aux_pi,
		 __global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *aux_pi, __global double *dx_in)
{
    int b;
    int a;

    double dx_inv = 1.0 / (*dx_in);


    b = get_global_id(0);
    a = get_global_id(1);

    int myi = idx(b, a);

    if (a >= 3 && a < N - 3) {
	diss_qq[myi] = qq[myi + 3] + qq[myi - 3] - 6.0 * (qq[myi + 2] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	diss_pp[myi] = pp[myi + 3] + pp[myi - 3] - 6.0 * (pp[myi + 2] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi - 3] - 6.0 * (ph[myi + 2] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi - 3] - 6.0 * (metrica[myi + 2] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi - 3] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 2) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi - 1] - 6.0 * (qq[myi + 2] + qq[myi - 2]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi - 1] - 6.0 * (pp[myi + 2] + pp[myi - 2]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi - 1] - 6.0 * (ph[myi + 2] + ph[myi - 2]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi - 1] - 6.0 * (metrica[myi + 2] + metrica[myi - 2]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi - 1] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi - 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 1) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi + 1] - 6.0 * (qq[myi + 2] - qq[myi]) + 15.0 * (qq[myi + 1] + qq[myi - 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi + 1] - 6.0 * (pp[myi + 2] + pp[myi]) + 15.0 * (pp[myi + 1] + pp[myi - 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi + 1] - 6.0 * (ph[myi + 2] + ph[myi]) + 15.0 * (ph[myi + 1] + ph[myi - 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi + 1] - 6.0 * (metrica[myi + 2] + metrica[myi]) + 15.0 * (metrica[myi + 1] + metrica[myi - 1]) - 20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi + 1] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi - 1]) - 20.0 * aux_pi[myi];
    } else if (a == 0) {
	// odd: 
	diss_qq[myi] = qq[myi + 3] - qq[myi + 3] - 6.0 * (qq[myi + 2] - qq[myi + 2]) + 15.0 * (qq[myi + 1] - qq[myi + 1]) - 20.0 * qq[myi];
	// even:
	diss_pp[myi] = pp[myi + 3] + pp[myi + 3] - 6.0 * (pp[myi + 2] + pp[myi + 2]) + 15.0 * (pp[myi + 1] + pp[myi + 1]) - 20.0 * pp[myi];
	diss_ph[myi] = ph[myi + 3] + ph[myi + 3] - 6.0 * (ph[myi + 2] + ph[myi + 2]) + 15.0 * (ph[myi + 1] + ph[myi + 1]) - 20.0 * ph[myi];
	diss_metrica[myi] =
	    metrica[myi + 3] + metrica[myi + 3] - 6.0 * (metrica[myi + 2] + metrica[myi + 2]) + 15.0 * (metrica[myi + 1] + metrica[myi + 1]) -
	    20.0 * metrica[myi];
	diss_aux_pi[myi] =
	    aux_pi[myi + 3] + aux_pi[myi + 3] - 6.0 * (aux_pi[myi + 2] + aux_pi[myi + 2]) + 15.0 * (aux_pi[myi + 1] + aux_pi[myi + 1]) - 20.0 * aux_pi[myi];
    } else if (a == N - 2) {
	diss_qq[myi] = qq[myi - 3] - 6.0 * qq[myi - 2] + 12.0 * qq[myi - 1] - 10.0 * qq[myi] + 3.0 * qq[myi + 1];
	diss_pp[myi] = pp[myi - 3] - 6.0 * pp[myi - 2] + 12.0 * pp[myi - 1] - 10.0 * pp[myi] + 3.0 * pp[myi + 1];
	diss_ph[myi] = ph[myi - 3] - 6.0 * ph[myi - 2] + 12.0 * ph[myi - 1] - 10.0 * ph[myi] + 3.0 * ph[myi + 1];
	diss_metrica[myi] = metrica[myi - 3] - 6.0 * metrica[myi - 2] + 12.0 * metrica[myi - 1] - 10.0 * metrica[myi] + 3.0 * metrica[myi + 1];
	diss_aux_pi[myi] = aux_pi[myi - 3] - 6.0 * aux_pi[myi - 2] + 12.0 * aux_pi[myi - 1] - 10.0 * aux_pi[myi] + 3.0 * aux_pi[myi + 1];
    } else if (a == N - 3) {
	diss_qq[myi] = qq[myi - 3] - 6.0 * qq[myi - 2] + 15.0 * qq[myi - 1] - 19.0 * qq[myi] + 12.0 * qq[myi + 1] - 3.0 * qq[myi + 2];;
	diss_pp[myi] = pp[myi - 3] - 6.0 * pp[myi - 2] + 15.0 * pp[myi - 1] - 19.0 * pp[myi] + 12.0 * pp[myi + 1] - 3.0 * pp[myi + 2];;
	diss_ph[myi] = ph[myi - 3] - 6.0 * ph[myi - 2] + 15.0 * ph[myi - 1] - 19.0 * ph[myi] + 12.0 * ph[myi + 1] - 3.0 * ph[myi + 2];;
	diss_metrica[myi] =
	    metrica[myi - 3] - 6.0 * metrica[myi - 2] + 15.0 * metrica[myi - 1] - 19.0 * metrica[myi] + 12.0 * metrica[myi + 1] - 3.0 * metrica[myi + 2];;
	diss_aux_pi[myi] =
	    aux_pi[myi - 3] - 6.0 * aux_pi[myi - 2] + 15.0 * aux_pi[myi - 1] - 19.0 * aux_pi[myi] + 12.0 * aux_pi[myi + 1] - 3.0 * aux_pi[myi + 2];;
    } else if (a == 1 || a == N - 1) {
	diss_qq[myi] = 0.0;
	diss_pp[myi] = 0.0;
	diss_ph[myi] = 0.0;
	diss_metrica[myi] = 0.0;
	diss_aux_pi[myi] = 0.0;
    }

    diss_qq[myi] = diss_qq[myi] * dx_inv;
    diss_pp[myi] = diss_pp[myi] * dx_inv;
    diss_ph[myi] = diss_ph[myi] * dx_inv;
    diss_metrica[myi] = diss_metrica[myi] * dx_inv;
    diss_aux_pi[myi] = diss_aux_pi[myi] * dx_inv;

}				// end kernel_calcdissn


/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute the RHS for the Runge-Kutta-------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_rhs1(__global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *diss_qq,
	    __global double *diss_pp, __global double *diss_ph, __global double *diss_metrica, __global double *qq_dx, __global double *pp_dx,
	    __global double *ph_dx, __global double *metrica_dx, __global double *delta_dx, __global double *qq, __global double *pp, __global double *ph,
	    __global double *metrica, __global double *delta, __global double *dx_in, __global double *x_c, __global double *aux_pi_dx)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double aexpmdelta, x, adx;
    double expml, dexpml;
    double myh, myf;

    x = x_c[a];
    expml = exp(-delta[idx(b, a)]);
    dexpml = -delta_dx[idx(b, a)] * expml;
    aexpmdelta = metrica[idx(b, a)] * expml;
    adx = metrica_dx[idx(b, a)] * expml + metrica[idx(b, a)] * dexpml;

    if (PUREADS == 1) {
	aexpmdelta = 1.0;
	adx = 0.0;
    }
    // Following Eq 9  of http://arxiv.org/pdf/1308.1235.pdf
    rhs_qq[idx(b, a)] = adx * pp[idx(b, a)] + aexpmdelta * pp_dx[idx(b, a)];

    // Try "mixing" the two righthandsides:
    myf = pow(0.5 + 0.5 * tanh((x - 0.4) / 0.1), 2);
    myh = pow(0.5 + 0.5 * tanh((0.5 * Pi - 0.4 - x) / 0.1), 2);
    myf = exp(-pow(x - 0.25 * Pi, 4) / 0.01);
    myh = 1.0;
    if ((x - 0.785398) * (x - 0.785398) < 0.0001) {
	rhs_pp[idx(b, a)] = ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
			     + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x));
    } else {
	rhs_pp[idx(b, a)] = myh * myf * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
					 + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x))
	    + (1.0 - myh * myf) * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)]) * (1.0 + 2.0 / cos(2.0 * x))
				   - tan(2.0 * x) * aux_pi_dx[idx(b, a)]);
    }


    if (PUREADS == 1) {
	rhs_metrica[idx(b, a)] = 0.0;
    } else {
	// rhs_metrica[idx (b, a)] = -2.0*sin(x)*pow(cos(x),4)*metrica[idx(b,a)]*aexpmdelta*pp[idx(b,a)]*qq[idx(b,a)];
	rhs_metrica[idx(b, a)] = -2.0 * sin(x) * cos(x) * metrica[idx(b, a)] * aexpmdelta * pp[idx(b, a)] * qq[idx(b, a)];
    }

	rhs_qq[idx(b, a)] = rhs_qq[idx(b, a)] + EPSDIS * diss_qq[idx(b, a)];
	rhs_pp[idx(b, a)] = rhs_pp[idx(b, a)] + EPSDIS * diss_pp[idx(b, a)];
	rhs_ph[idx(b, a)] = rhs_ph[idx(b, a)] + EPSDIS * diss_ph[idx(b, a)];
	rhs_metrica[idx(b, a)] = rhs_metrica[idx(b, a)] + EPSDIS * diss_metrica[idx(b, a)];

    if (a == N - 1) {
	rhs_qq[idx(b, a)] = 0.0;
	rhs_metrica[idx(b, a)] = 0.0;
    }
}				// END: kernel_rhs1

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute the RHS for the Runge-Kutta-------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_rhs2(__global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *diss_qq,
	    __global double *diss_pp, __global double *diss_ph, __global double *diss_metrica, __global double *qq_dx, __global double *pp_dx,
	    __global double *ph_dx, __global double *metrica_dx, __global double *delta_dx, __global double *qq, __global double *pp, __global double *ph,
	    __global double *metrica, __global double *delta, __global double *dx_in, __global double *x_c, __global double *aux_pi_dx)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double aexpmdelta, x, adx;
    double expml, dexpml;
    double myf, myh;

    x = x_c[a];
    expml = exp(-delta[idx(b, a)]);
    dexpml = -delta_dx[idx(b, a)] * expml;
    aexpmdelta = metrica[idx(b, a)] * expml;
    adx = metrica_dx[idx(b, a)] * expml + metrica[idx(b, a)] * dexpml;

    if (PUREADS == 1) {
	aexpmdelta = 1.0;
	adx = 0.0;
    }
    // Following Eq 9  of http://arxiv.org/pdf/1308.1235.pdf
    rhs_qq[idx(b, a)] = adx * pp[idx(b, a)] + aexpmdelta * pp_dx[idx(b, a)];

    // Try "mixing" the two righthandsides:
    myf = pow(0.5 + 0.5 * tanh((x - 0.4) / 0.1), 2);
    myh = pow(0.5 + 0.5 * tanh((0.5 * Pi - 0.4 - x) / 0.1), 2);
    myf = exp(-pow(x - 0.25 * Pi, 4) / 0.01);
    myh = 1.0;
    if ((x - 0.785398) * (x - 0.785398) < 0.0001) {
	rhs_pp[idx(b, a)] = ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
			     + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x));
    } else {
	rhs_pp[idx(b, a)] = myh * myf * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
					 + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x))
	    + (1.0 - myh * myf) * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)]) * (1.0 + 2.0 / cos(2.0 * x))
				   - tan(2.0 * x) * aux_pi_dx[idx(b, a)]);
    }


    if (PUREADS == 1) {
	rhs_metrica[idx(b, a)] = 0.0;
    } else {
	rhs_metrica[idx(b, a)] = -2.0 * sin(x) * cos(x) * metrica[idx(b, a)] * aexpmdelta * pp[idx(b, a)] * qq[idx(b, a)];
    }

    rhs_qq[idx(b, a)] = rhs_qq[idx(b, a)] + EPSDIS * diss_qq[idx(b, a)];
    rhs_pp[idx(b, a)] = rhs_pp[idx(b, a)] + EPSDIS * diss_pp[idx(b, a)];
    rhs_ph[idx(b, a)] = rhs_ph[idx(b, a)] + EPSDIS * diss_ph[idx(b, a)];
    rhs_metrica[idx(b, a)] = rhs_metrica[idx(b, a)] + EPSDIS * diss_metrica[idx(b, a)];

    if (a == N - 1) {
	rhs_qq[idx(b, a)] = 0.0;
	rhs_metrica[idx(b, a)] = 0.0;
    }
}				// END: kernel_rhs2

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  compute the RHS for the Runge-Kutta-------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_rhs3(__global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *diss_qq,
	    __global double *diss_pp, __global double *diss_ph, __global double *diss_metrica, __global double *qq_dx, __global double *pp_dx,
	    __global double *ph_dx, __global double *metrica_dx, __global double *delta_dx, __global double *qq, __global double *pp, __global double *ph,
	    __global double *metrica, __global double *delta, __global double *dx_in, __global double *x_c, __global double *aux_pi_dx)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double aexpmdelta, x, adx;
    double expml, dexpml;
    double myf, myh;

    x = x_c[a];
    expml = exp(-delta[idx(b, a)]);
    dexpml = -delta_dx[idx(b, a)] * expml;
    aexpmdelta = metrica[idx(b, a)] * expml;
    adx = metrica_dx[idx(b, a)] * expml + metrica[idx(b, a)] * dexpml;

    if (PUREADS == 1) {
	aexpmdelta = 1.0;
	adx = 0.0;
    }
    // Following Eq 9  of http://arxiv.org/pdf/1308.1235.pdf
    rhs_qq[idx(b, a)] = adx * pp[idx(b, a)] + aexpmdelta * pp_dx[idx(b, a)];

    // Try "mixing" the two righthandsides:
    myf = pow(0.5 + 0.5 * tanh((x - 0.4) / 0.1), 2);
    myh = pow(0.5 + 0.5 * tanh((0.5 * Pi - 0.4 - x) / 0.1), 2);
    myf = exp(-pow(x - 0.25 * Pi, 4) / 0.01);
    myh = 1.0;
    if ((x - 0.785398) * (x - 0.785398) < 0.0001) {
	rhs_pp[idx(b, a)] = ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
			     + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x));
    } else {
	rhs_pp[idx(b, a)] = myh * myf * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)])
					 + 2.0 * aexpmdelta * qq[idx(b, a)] / sin(x) / cos(x))
	    + (1.0 - myh * myf) * ((adx * qq[idx(b, a)] + aexpmdelta * qq_dx[idx(b, a)]) * (1.0 + 2.0 / cos(2.0 * x))
				   - tan(2.0 * x) * aux_pi_dx[idx(b, a)]);
    }


    if (PUREADS == 1) {
	rhs_metrica[idx(b, a)] = 0.0;
    } else {
	rhs_metrica[idx(b, a)] = -2.0 * sin(x) * cos(x) * metrica[idx(b, a)] * aexpmdelta * pp[idx(b, a)] * qq[idx(b, a)];
    }

    rhs_qq[idx(b, a)] = rhs_qq[idx(b, a)] + EPSDIS * diss_qq[idx(b, a)];
    rhs_pp[idx(b, a)] = rhs_pp[idx(b, a)] + EPSDIS * diss_pp[idx(b, a)];
    rhs_ph[idx(b, a)] = rhs_ph[idx(b, a)] + EPSDIS * diss_ph[idx(b, a)];
    rhs_metrica[idx(b, a)] = rhs_metrica[idx(b, a)] + EPSDIS * diss_metrica[idx(b, a)];

    if (a == N - 1) {
	rhs_qq[idx(b, a)] = 0.0;
	rhs_metrica[idx(b, a)] = 0.0;
    }
}				// END: kernel_rhs3

/* -------------------------------------------------- */

__kernel void kernel_compauxpi(__global double *qq, __global double *metrica, __global double *delta, __global double *aux_pi, __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    if (a > 0 && a < N - 1) {
	aux_pi[idx(b, a)] = metrica[idx(b, a)] * exp(-delta[idx(b, a)]) * qq[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }
}

//  Takes _np1 fields as arguments and so only difference in body.c:
__kernel void kernel_compauxpi2(__global double *qq, __global double *metrica, __global double *delta, __global double *aux_pi, __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    if (a > 0 && a < N - 1) {
	aux_pi[idx(b, a)] = metrica[idx(b, a)] * exp(-delta[idx(b, a)]) * qq[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }
}

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  update the fields as first step of RK ----- */
/* -------------------------------------------------- */
/* ------------ X_np1 = X_n + dt*RHS ---------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_update1(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *delta_np1,
	       __global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *qq,
	       __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dt_in, global double *qq_k1,
	       __global double *pp_k1, __global double *ph_k1, __global double *metrica_k1, __global double *aux_pi, __global double *diss_aux_pi,
	       __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dt = *dt_in;

    if (RKORDER == 3) {
	qq_np1[idx(b, a)] = qq[idx(b, a)] + 1.0 * dt * rhs_qq[idx(b, a)];
	pp_np1[idx(b, a)] = pp[idx(b, a)] + 1.0 * dt * rhs_pp[idx(b, a)];
	metrica_np1[idx(b, a)] = metrica[idx(b, a)] + 1.0 * dt * rhs_metrica[idx(b, a)];
    } else if (RKORDER == 2) {
	// RK2 w/ midpoint method
	qq_np1[idx(b, a)] = qq[idx(b, a)] + 0.5 * dt * rhs_qq[idx(b, a)];
	pp_np1[idx(b, a)] = pp[idx(b, a)] + 0.5 * dt * rhs_pp[idx(b, a)];
	metrica_np1[idx(b, a)] = metrica[idx(b, a)] + 0.5 * dt * rhs_metrica[idx(b, a)];
    } else if (RKORDER == 4) {
	// RK4
	qq_np1[idx(b, a)] = qq[idx(b, a)] + 0.5 * dt * rhs_qq[idx(b, a)];
	pp_np1[idx(b, a)] = pp[idx(b, a)] + 0.5 * dt * rhs_pp[idx(b, a)];
	metrica_np1[idx(b, a)] = metrica[idx(b, a)] + 0.5 * dt * rhs_metrica[idx(b, a)];
	// Set the RK k1 variables:
	qq_k1[idx(b, a)] = dt * rhs_qq[idx(b, a)];
	pp_k1[idx(b, a)] = dt * rhs_pp[idx(b, a)];
	metrica_k1[idx(b, a)] = dt * rhs_metrica[idx(b, a)];
    }

    if (metrica_np1[idx(b, a)] > 1.0)
	metrica_np1[idx(b, a)] = 1.0;

    // Set the aux_pi field:
    if (a == 0) {
	aux_pi[idx(b, a)] = 0.0;
    } else if (a == N - 1) {
	aux_pi[idx(b, a)] = 0.0;
    } else {
	aux_pi[idx(b, a)] = metrica_np1[idx(b, a)] * exp(-delta_np1[idx(b, a)]) * qq_np1[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }

    aux_pi[idx(b, a)] = aux_pi[idx(b, a)] - (1.0 / 16.0) * EPSDIS * dt * diss_aux_pi[idx(b, a)];

}				// END: kernel_update1


/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  update the fields as second step of RK ---- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_update2(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *delta_np1,
	       __global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *qq,
	       __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dt_in, global double *qq_k2,
	       __global double *pp_k2, __global double *ph_k2, __global double *metrica_k2, __global double *aux_pi, __global double *diss_aux_pi,
	       __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dt = *dt_in;

    if (RKORDER == 3) {
	qq_np1[idx(b, a)] = 0.75 * qq[idx(b, a)] + 0.25 * dt * rhs_qq[idx(b, a)] + 0.25 * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = 0.75 * pp[idx(b, a)] + 0.25 * dt * rhs_pp[idx(b, a)] + 0.25 * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = 0.75 * metrica[idx(b, a)] + 0.25 * dt * rhs_metrica[idx(b, a)] + 0.25 * metrica_np1[idx(b, a)];
    } else if (RKORDER == 2) {
	// RK2 w/ midpoint method
	qq_np1[idx(b, a)] = 1.0 * qq[idx(b, a)] + 1.0 * dt * rhs_qq[idx(b, a)] + 0.0 * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = 1.0 * pp[idx(b, a)] + 1.0 * dt * rhs_pp[idx(b, a)] + 0.0 * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = 1.0 * metrica[idx(b, a)] + 1.0 * dt * rhs_metrica[idx(b, a)] + 0.0 * metrica_np1[idx(b, a)];
    } else if (RKORDER == 4) {
	// RK4
	qq_np1[idx(b, a)] = 1.0 * qq[idx(b, a)] + 0.5 * dt * rhs_qq[idx(b, a)] + 0.0 * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = 1.0 * pp[idx(b, a)] + 0.5 * dt * rhs_pp[idx(b, a)] + 0.0 * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = 1.0 * metrica[idx(b, a)] + 0.5 * dt * rhs_metrica[idx(b, a)] + 0.0 * metrica_np1[idx(b, a)];
	// Set the RK k2 variables:
	qq_k2[idx(b, a)] = dt * rhs_qq[idx(b, a)];
	pp_k2[idx(b, a)] = dt * rhs_pp[idx(b, a)];
	metrica_k2[idx(b, a)] = dt * rhs_metrica[idx(b, a)];
    }
    // Set the aux_pi field:
    if (a == 0) {
	aux_pi[idx(b, a)] = 0.0;
    } else if (a == N - 1) {
	aux_pi[idx(b, a)] = 0.0;
    } else {
	aux_pi[idx(b, a)] = metrica_np1[idx(b, a)] * exp(-delta_np1[idx(b, a)]) * qq_np1[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }
    aux_pi[idx(b, a)] = aux_pi[idx(b, a)] - (1.0 / 16.0) * EPSDIS * dt * diss_aux_pi[idx(b, a)];

    if (metrica_np1[idx(b, a)] > 1.0)
	metrica_np1[idx(b, a)] = 1.0;
}

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  update the fields as third  step of RK ---- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_update3(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *delta_np1,
	       __global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *qq,
	       __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dt_in, global double *qq_k3,
	       __global double *pp_k3, __global double *ph_k3, __global double *metrica_k3, __global double *aux_pi, __global double *diss_aux_pi,
	       __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dt = *dt_in;
    const double othird = 1.0 / 3.0;
    const double tthird = 2.0 * othird;

    if (RKORDER == 3) {
	qq_np1[idx(b, a)] = othird * qq[idx(b, a)] + tthird * dt * rhs_qq[idx(b, a)] + tthird * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = othird * pp[idx(b, a)] + tthird * dt * rhs_pp[idx(b, a)] + tthird * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = othird * metrica[idx(b, a)] + tthird * dt * rhs_metrica[idx(b, a)] + tthird * metrica_np1[idx(b, a)];
    } else if (RKORDER == 2) {
	// nothing needed to do for RK2
    } else if (RKORDER == 4) {
	qq_np1[idx(b, a)] = 1.0 * qq[idx(b, a)] + 1.0 * dt * rhs_qq[idx(b, a)] + 0 * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = 1.0 * pp[idx(b, a)] + 1.0 * dt * rhs_pp[idx(b, a)] + 0 * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = 1.0 * metrica[idx(b, a)] + 1.0 * dt * rhs_metrica[idx(b, a)] + 0 * metrica_np1[idx(b, a)];
	// Set the RK k3 variables:
	qq_k3[idx(b, a)] = dt * rhs_qq[idx(b, a)];
	pp_k3[idx(b, a)] = dt * rhs_pp[idx(b, a)];
	metrica_k3[idx(b, a)] = dt * rhs_metrica[idx(b, a)];
    }
    // Set the aux_pi field:
    if (a == 0) {
	aux_pi[idx(b, a)] = 0.0;
    } else if (a == N - 1) {
	aux_pi[idx(b, a)] = 0.0;
    } else {
	aux_pi[idx(b, a)] = metrica_np1[idx(b, a)] * exp(-delta_np1[idx(b, a)]) * qq_np1[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }
    aux_pi[idx(b, a)] = aux_pi[idx(b, a)] - (1.0 / 16.0) * EPSDIS * dt * diss_aux_pi[idx(b, a)];

    if (metrica_np1[idx(b, a)] > 1.0)
	metrica_np1[idx(b, a)] = 1.0;
}

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  update the fields as fourth step of RK ---- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_update4(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *delta_np1,
	       __global double *rhs_qq, __global double *rhs_pp, __global double *rhs_ph, __global double *rhs_metrica, __global double *qq,
	       __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *dt_in, global double *qq_k4,
	       __global double *pp_k4, __global double *ph_k4, __global double *metrica_k4, __global double *aux_pi, __global double *diss_aux_pi,
	       __global double *x_c)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dt = *dt_in;
    const double othird = 1.0 / 3.0;
    const double tthird = 2.0 * othird;

    if (RKORDER == 3) {
	// nothing needed to do for RK3
    } else if (RKORDER == 2) {
	// nothing needed to do for RK2
    } else if (RKORDER == 4) {
	qq_np1[idx(b, a)] = 1.0 * qq[idx(b, a)] + 1.0 * dt * rhs_qq[idx(b, a)] + 0 * qq_np1[idx(b, a)];
	pp_np1[idx(b, a)] = 1.0 * pp[idx(b, a)] + 1.0 * dt * rhs_pp[idx(b, a)] + 0 * pp_np1[idx(b, a)];
	metrica_np1[idx(b, a)] = 1.0 * metrica[idx(b, a)] + 1.0 * dt * rhs_metrica[idx(b, a)] + 0 * metrica_np1[idx(b, a)];
    }
    // Set the aux_pi field:
    if (a == 0) {
	aux_pi[idx(b, a)] = 0.0;
    } else if (a == N - 1) {
	aux_pi[idx(b, a)] = 0.0;
    } else {
	aux_pi[idx(b, a)] = metrica_np1[idx(b, a)] * exp(-delta_np1[idx(b, a)]) * qq_np1[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }

    // Set the RK k4 variables:
    qq_k4[idx(b, a)] = dt * rhs_qq[idx(b, a)];
    pp_k4[idx(b, a)] = dt * rhs_pp[idx(b, a)];
    metrica_k4[idx(b, a)] = dt * rhs_metrica[idx(b, a)];

    if (metrica_np1[idx(b, a)] > 1.0)
	metrica_np1[idx(b, a)] = 1.0;
}



/* -------------------------------------------------- */

__kernel void
kernel_relax2(__global double *qq_h, __global double *pp_h, __global double *ph_h, __global double *metrica_h, __global double *delta_h, __global double *qq,
	      __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *x_c, __global double *rhs,
	      __global double *jj, __global double *sourceint, __global double *sourcecop)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dx = (x_c[idx(b, a)] - x_c[idx(b, a - 1)]);
    double x, jacobian, res, deltadiff, deltax, tmp;
    double xh, phih, pih, cosxh, sinxh, myrhs, dx_inv;

    xh = 0.5 * (x_c[a + 1] + x_c[a]);

    if (a > 1 && a < N - 2) {
	phih = kernel_interpcubic(qq_h[a - 1], qq_h[a], qq_h[a + 1], qq_h[a + 2], xh, x_c[a - 1], x_c[a], x_c[a + 1], x_c[a + 2]);
	pih = kernel_interpcubic(pp_h[a - 1], pp_h[a], pp_h[a + 1], pp_h[a + 2], xh, x_c[a - 1], x_c[a], x_c[a + 1], x_c[a + 2]);
    } else if (a == 0) {
	// use symmetry properties
	phih = kernel_interpcubic(-qq_h[a + 1], qq_h[a], qq_h[a + 1], qq_h[a + 2], xh, -x_c[a + 1], x_c[a], x_c[a + 1], x_c[a + 2]);
	pih = kernel_interpcubic(pp_h[a + 1], pp_h[a], pp_h[a + 1], pp_h[a + 2], xh, -x_c[a + 1], x_c[a], x_c[a + 1], x_c[a + 2]);
    } else if (a < N - 3) {
	phih = kernel_interpcubic(qq_h[a], qq_h[a + 1], qq_h[a + 2], qq_h[a + 3], xh, x_c[a], x_c[a + 1], x_c[a + 2], x_c[a + 3]);
	pih = kernel_interpcubic(pp_h[a], pp_h[a + 1], pp_h[a + 2], pp_h[a + 3], xh, x_c[a], x_c[a + 1], x_c[a + 2], x_c[a + 3]);
    } else if (a > 2) {
	phih = kernel_interpcubic(qq_h[a - 2], qq_h[a - 1], qq_h[a], qq_h[a + 1], xh, x_c[a - 2], x_c[a - 1], x_c[a], x_c[a + 1]);
	pih = kernel_interpcubic(pp_h[a - 2], pp_h[a - 1], pp_h[a], pp_h[a + 1], xh, x_c[a - 2], x_c[a - 1], x_c[a], x_c[a + 1]);
    }
    // Just a direct "copy":
    sourcecop[a] = qq_h[a] * qq_h[a] + pp_h[a] * pp_h[a];
    // "Interpolate":
    sourceint[a] = phih * phih + pih * pih;

}

/* -------------------------------------------------- */

__kernel void
kernel_boundary(__global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *delta, __global double *aux_pi,
		__global double *qq_buff)
{
    int b;

    b = get_global_id(0);
    double tmp1, tmp2, tmp3;

    /*    working from: http://arxiv.org/abs/1210.0890
       qq -> Phi
       pp -> Pi
       Boundary conditions in Eq. 2.14 and 2.15
       qq(x=0)    = 0
       pp(x=0)    = free (quadratic fit)
       qq(x=Pi/2) = 0
       pp(x=Pi/2) = 0
     */

    // Origin:
    qq[idx(b, 0)] = 0.0;

    // Quadratic fit:
    pp[idx(b, 0)] = 1.5 * pp[idx(b, 1)] - 0.6 * pp[idx(b, 2)] + 0.1 * pp[idx(b, 3)];
    aux_pi[idx(b, 0)] = 1.5 * aux_pi[idx(b, 1)] - 0.6 * aux_pi[idx(b, 2)] + 0.1 * aux_pi[idx(b, 3)];

    metrica[idx(b, 0)] = 1.0;

    // Outer boundary (x=Pi/2):
    qq[idx(b, N - 1)] = 0.0;
    pp[idx(b, N - 1)] = 0.0;
    //
    //  even fourth order:
    qq[idx(b, N - 2)] = (6.0 * qq[idx(b, N - 3)] - qq[idx(b, N - 4)]) / 15.0;
    pp[idx(b, N - 2)] = 0.8 * pp[idx(b, N - 3)] - 0.2 * pp[idx(b, N - 4)];
    aux_pi[idx(b, N - 2)] = 0.8 * aux_pi[idx(b, N - 3)] - 0.2 * aux_pi[idx(b, N - 4)];


    qq_buff[b] = pp[idx(b, 0)];

}

/* -------------------------------------------------- */
/* -------------------------------------------------- */

__kernel void
kernel_compaux(__global double *dmdr, __global double *hamc, __global double *momc, __global double *qq, __global double *pp, __global double *ph,
	       __global double *metrica, __global double *delta, __global double *metrica_h, __global double *x, __global double *dt_in)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    // The following line seems strange, but copying from above:
    double dt = *dt_in;
    double dx = x[idx(b, a + 1)] - x[idx(b, a)];
    // For rescaled fields:
    double source = pow(qq[idx(b, a)], 2) + pow(pp[idx(b, a)], 2);
    // compute value of metrica here from the half grid:
    double metrica_hc = 0.5 * (metrica_h[idx(b, a + 1)] + metrica_h[idx(b, a)]);
    // estimate time derivative here (only first order accurate since not centered):
    double metrica_t = (metrica[idx(b, a)] - metrica_h[idx(b, a)]) / dt;
    double metrica_x = 0.5 * (metrica[idx(b, a + 1)] - metrica[idx(b, a - 1)]) / dx;

    if (a > 0) {
	// Using original fields:
	dmdr[idx(b, a)] = pow(tan(x[idx(b, a)]), 2) * metrica[idx(b, a)] * (source);
	hamc[idx(b, a)] = sin(x[idx(b, a)]) * cos(x[idx(b, a)]) * metrica_x - (1.0 + 2.0 * pow(sin(x[idx(b, a)]), 2)) * (1.0 - metrica[idx(b, a)])
	    + pow(sin(x[idx(b, a)]) * cos(x[idx(b, a)]), 2) * metrica[idx(b, a)] * source;
    } else {
	dmdr[idx(b, a)] = 0.0;
	hamc[idx(b, a)] = 0.0;
    }

    // Original fields:
    momc[idx(b, a)] =
	metrica_t + 2.0 * sin(x[idx(b, a)]) * cos(x[idx(b, a)]) * pow(metrica[idx(b, a)], 2) * exp(-delta[idx(b, a)]) * qq[idx(b, a)] * pp[idx(b, a)];

}

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  RK final                              ----- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_rkfinal(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *qq,
	       __global double *pp, __global double *ph, __global double *metrica, __global double *qq_k1, __global double *pp_k1, __global double *ph_k1,
	       __global double *metrica_k1, __global double *qq_k2, __global double *pp_k2, __global double *ph_k2, __global double *metrica_k2,
	       __global double *qq_k3, __global double *pp_k3, __global double *ph_k3, __global double *metrica_k3, __global double *qq_k4,
	       __global double *pp_k4, __global double *ph_k4, __global double *metrica_k4, __global double *aux_pi, __global double *delta_np1,
	       __global double *x_c, __global double *diss_aux_pi, __global double *dt_in)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double dt = *dt_in;

    qq_np1[idx(b, a)] = qq[idx(b, a)] + (1.0 / 6.0) * (qq_k1[idx(b, a)] + 2.0 * qq_k2[idx(b, a)] + 2.0 * qq_k3[idx(b, a)] + qq_k4[idx(b, a)]);
    pp_np1[idx(b, a)] = pp[idx(b, a)] + (1.0 / 6.0) * (pp_k1[idx(b, a)] + 2.0 * pp_k2[idx(b, a)] + 2.0 * pp_k3[idx(b, a)] + pp_k4[idx(b, a)]);
    ph_np1[idx(b, a)] = ph[idx(b, a)] + (1.0 / 6.0) * (ph_k1[idx(b, a)] + 2.0 * ph_k2[idx(b, a)] + 2.0 * ph_k3[idx(b, a)] + ph_k4[idx(b, a)]);
    metrica_np1[idx(b, a)] =
	metrica[idx(b, a)] + (1.0 / 6.0) * (metrica_k1[idx(b, a)] + 2.0 * metrica_k2[idx(b, a)] + 2.0 * metrica_k3[idx(b, a)] + metrica_k4[idx(b, a)]);

    // Set the aux_pi field:
    if (a == 0) {
	// quadratic fit
	//  This is not correct, but we shouldn't need this value as long as we take derivative correctly
	aux_pi[idx(b, a)] = 0.0;
    } else if (a == N - 1) {
	aux_pi[idx(b, a)] = 0.0;
    } else {
	aux_pi[idx(b, a)] = metrica_np1[idx(b, a)] * exp(-delta_np1[idx(b, a)]) * qq_np1[idx(b, a)] / sin(x_c[idx(b, a)]) / cos(x_c[idx(b, a)]);
    }
    aux_pi[idx(b, a)] = aux_pi[idx(b, a)] - (1.0 / 16.0) * EPSDIS * dt * diss_aux_pi[idx(b, a)];


}

/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -----  swap _n and _np1 data                 ----- */
/* -------------------------------------------------- */
/* ------ really should just swap pointers but ------ */
/* ------ not sure how to do this w/ host/device (SLL)*/
/* -------------------------------------------------- */
/* -------------------------------------------------- */
/* -------------------------------------------------- */
__kernel void
kernel_swapnn1(__global double *qq_np1, __global double *pp_np1, __global double *ph_np1, __global double *metrica_np1, __global double *delta_np1,
	       __global double *qq, __global double *pp, __global double *ph, __global double *metrica, __global double *delta)
{
    int b;
    int a;

    b = get_global_id(0);
    a = get_global_id(1);

    double temp;

    temp = qq_np1[idx(b, a)];
    qq_np1[idx(b, a)] = qq[idx(b, a)];
    qq[idx(b, a)] = temp;
    //
    temp = pp_np1[idx(b, a)];
    pp_np1[idx(b, a)] = pp[idx(b, a)];
    pp[idx(b, a)] = temp;
    //
    temp = ph_np1[idx(b, a)];
    ph_np1[idx(b, a)] = ph[idx(b, a)];
    ph[idx(b, a)] = temp;
    //
    temp = metrica_np1[idx(b, a)];
    metrica_np1[idx(b, a)] = metrica[idx(b, a)];
    metrica[idx(b, a)] = temp;
    //
    temp = delta_np1[idx(b, a)];
    // Just copy the np1 data into the _n data since it's always the best:
    delta[idx(b, a)] = temp;
    //

}

/*-------------------------------------------------------------------------
 *
 *  kernel_interpcubic
 *
 *-------------------------------------------------------------------------*/
double kernel_interpcubic(double y1, double y2, double y3, double y4, double x, double x1, double x2, double x3, double x4)
{
    double interpcubic, xx1, xx2, xx3, xx4;
    xx1 = x - x1;
    xx2 = x - x2;
    xx3 = x - x3;
    xx4 = x - x4;
    interpcubic = xx2 * xx3 * xx4 * y1 / ((x1 - x2) * (x1 - x3) * (x1 - x4))
	+ xx1 * xx3 * xx4 * y2 / ((x2 - x1) * (x2 - x3) * (x2 - x4))
	+ xx1 * xx2 * xx4 * y3 / ((x3 - x1) * (x3 - x2) * (x3 - x4))
	+ xx1 * xx2 * xx3 * y4 / ((x4 - x1) * (x4 - x2) * (x4 - x3));

    return (interpcubic);
}
